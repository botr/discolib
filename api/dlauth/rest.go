// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dlauth

import (
	"encoding/json"
	"net/url"
	"time"

	"pkg.botr.me/discolib/api/dlresource"
	"pkg.botr.me/discolib/api/dlrest"
	"pkg.botr.me/discolib/api/dltype"
	"pkg.botr.me/discolib/internal/errstr"
)

// TODO: implement /oauth2/token/revoke in accordance to https://tools.ietf.org/html/rfc7009

// AuthorizationCodeGrant is what most developers will recognize as "standard OAuth2" and involves
// retrieving an access code and exchanging it for a user's access token.
// It allows the authorization server to act as an intermediary between the client and the resource owner,
// so the resource owner's credentials are never shared directly with the client.
//
// NOTE: although Discord itself not enforce usage of state parameter, it is made mandatory for security reasons.
//
// After successful authorization user will be redirected to URL specified with RedirectURL and followind query parameters:
//  - code  - access code to exchange for access token
//  - state - value that should be checked to match with State from authorization request
type AuthorizationCodeGrant struct {
	ClientID    dltype.Snowflake `url:"client_id"   valid:"required"` // your app's client id
	State       string           `url:"state"       valid:"required"` // string unique to user's request (to protect from CSRF)
	Prompt      Prompt           `url:"prompt"      valid:"required"` // controls how the authorization flow handles existing authorizations
	Scope       []Scope          `url:"scope,space" valid:"required"` // list of scopes (will be separated by url encoded spaces)
	RedirectURI string           `url:"redirect_uri,omitempty"`       // will be url-encoded

	redirect AuthorizationCodeRedirect `url:"-" form:"-"`
}

type AuthorizationCodeRedirect struct {
	Code  string `url:"code"`
	State string `url:"state"`
}

func (req *AuthorizationCodeGrant) Path() string { return "/oauth2/authorize" }

func (req *AuthorizationCodeGrant) Redirect() *AuthorizationCodeRedirect { return &req.redirect }

func (req *AuthorizationCodeGrant) EncodeURL(enc SchemaEncoder, v url.Values) error {
	if err := enc.Encode(req, v); err != nil {
		return err
	}

	v.Add("response_type", ResponseCode) // not optional and differs for different grant flows
	return nil
}

// ImplicitGrant is a simplified flow optimized for in-browser clients.
// Instead of issuing the client an authorization code to be exchanged for an access token,
// the client is directly issued an access token.
//
// On redirect, your redirect URI will contain additional URI fragments:
// access_token, token_type, expires_in, scope, and state (if specified).
//
// There are tradeoffs in using the implicit grant flow. It is both quicker and easier to implement,
// but rather than exchanging a code and getting a token returned in a secure HTTP body,
// the access token is returned in the URI fragment, which makes it possibly exposed to unauthorized parties.
// You also are not returned a refresh token, so the user must explicitly re-authorize once their token expires.
type ImplicitGrant struct {
	ClientID dltype.Snowflake `url:"client_id"   valid:"required"` // your app's client id
	Scope    []Scope          `url:"scope,space" valid:"required"` // list of scopes (will be separated by url encoded spaces)
	State    string           `url:"state"       valid:"required"` // string unique to user's request (to protect from CSRF)

	redirect ImplicitGrantRedirect `url:"-" form:"-"`
}

type ImplicitGrantRedirect struct {
	Token

	ExpiresIn int     `url:"expires_in"` // how long, in seconds, until the returned access token expires
	Scope     []Scope `url:"-"`          // requested scopes
	State     string  `url:"state"`
}

func (red *ImplicitGrantRedirect) DecodeURL(dec SchemaDecoder, v url.Values) error {
	type Aux ImplicitGrantRedirect
	aux := struct {
		*Aux
		Scope string `json:"scope"` // space-separated string
	}{Aux: (*Aux)(red)}

	if err := dec.Decode(aux, v); err != nil {
		return errstr.Error("can't unmarshal response").Wrap(err)
	}

	red.Scope = decodeScopes(aux.Scope)
	return nil
}

func (req *ImplicitGrant) Path() string { return "/oauth2/authorize" }

func (req *ImplicitGrant) Redirect() *ImplicitGrantRedirect { return &req.redirect }

func (req *ImplicitGrant) EncodeURL(enc SchemaEncoder, v url.Values) error {
	if err := enc.Encode(req, v); err != nil {
		return err
	}

	v.Add("response_type", ResponseToken) // not optional and differs for different grant flows
	return nil
}

// BotAuthorization can be used to request bot scopes.
// When if scopes outside of bot and applications.commands are provided, authorization will prompt a continuation into
// a complete authorization code grant flow and add the ability to request the user's access token.
// If you request any scopes outside of bot, response_type is again mandatory.
// User will be automatically redirected to the first uri in your application's registered list unless redirect_uri is specified.
//
// When receiving the access code on redirect, there will be additional querystring parameters of guild_id and permissions.
// The guild_id parameter should only be used as a hint as to the relationship between your bot and a guild.
// To be sure of the relationship between your bot and the guild, consider requiring the Oauth2 code grant in your bot's settings.
// Enabling it requires anyone adding your bot to a server to go through a full OAuth2 authorization code grant flow.
type BotAuthorization struct {
	ClientID dltype.Snowflake `url:"client_id"   valid:"required"` // your app's client id
	Scope    []Scope          `url:"scope,space" valid:"required"` // bot scope not implicit and must be put manually

	RedirectURI string `url:"redirect_uri,omitempty"` // will be url-encoded
	State       string `url:"state,omitempty"`        // string unique to user's request (to protect from CSRF), required when scope contains something except bot and applications.commands
	Prompt      Prompt `url:"prompt,omitempty"`       // controls how the authorization flow handles existing authorizations

	Permissions        dlresource.Permission `url:"permissions"`
	GuildID            dltype.Snowflake      `url:"guild_id,omitempty" valid:"required_with=DisableGuildSelect"` // pre-fills the dropdown picker with a guild for the user
	DisableGuildSelect bool                  `url:"disable_guild_select,omitempty"`                              // disallows the user from changing the guild dropdown

	redirect BotAuthorizationRedirect `url:"-" form:"-"`
}

type BotAuthorizationRedirect struct {
	Code        string                `url:"code"`
	State       string                `url:"state"`
	GuildID     dltype.Snowflake      `url:"guild_id"`
	Permissions dlresource.Permission `url:"permissions"`
}

func (req *BotAuthorization) Path() string { return "/oauth2/authorize" }

func (req *BotAuthorization) Redirect() *BotAuthorizationRedirect { return &req.redirect }

func (req *BotAuthorization) EncodeURL(enc SchemaEncoder, v url.Values) error {
	if err := enc.Encode(req, v); err != nil {
		return err
	}

	for _, s := range req.Scope {
		switch s {
		case ScopeApplicationsCommands, ScopeBot:
			continue
		}

		if req.State == "" {
			return errstr.Error("empty state")
		}

		v.Add("response_type", ResponseCode) // not optional and differs for different grant flows
		break
	}
	return nil
}

// WebhookAuthorization is a specialized version of an authorization code implementation.
type WebhookAuthorization struct {
	ClientID    dltype.Snowflake `url:"client_id" valid:"required"` // your app's client id
	State       string           `url:"state"     valid:"required"` // string unique to user's request (to protect from CSRF)
	RedirectURI string           `url:"redirect_uri,omitempty"`     // will be url-encoded

	redirect AuthorizationCodeRedirect `url:"-" form:"-"`
}

func (req *WebhookAuthorization) Path() string { return "/oauth2/authorize" }

func (req *WebhookAuthorization) Redirect() *AuthorizationCodeRedirect { return &req.redirect }

func (req *WebhookAuthorization) EncodeURL(enc SchemaEncoder, v url.Values) error {
	if err := enc.Encode(req, v); err != nil {
		return err
	}

	v.Add("scope", string(ScopeWebhookIncoming))
	v.Add("response_type", ResponseCode) // not optional and differs for different grant flows
	return nil
}

// AuthorizationToken exchanges access code to user's access token by making POST request
// NOTE: In accordance with the relevant RFCs, the token and token revocation URLs will only accept a content type of application/x-www-form-urlencoded.
// JSON content is not permitted and will return an error.
//
// Code and RefreshToken are mutually exclusive.
// When grant type is authorization_code Code must be used and for refresh_token grant type RefreshToken must be used.
type AuthorizationToken struct {
	ClientID     dltype.Snowflake `url:"-" form:"client_id"               valid:"required"`                                 // your application's client id
	ClientSecret string           `url:"-" form:"client_secret"           valid:"required"`                                 // your application's client secret
	Scope        []Scope          `url:"-" form:"scope,space"             valid:"required"`                                 // the scopes requested in your authorization url, will be separated by space
	GrantType    GrantType        `url:"-" form:"grant_type"              valid:"required"`                                 //
	Code         string           `url:"-" form:"code,omitempty"          valid:"required_if=GrantType authorization_code"` // the code from the querystring
	RefreshToken string           `url:"-" form:"refresh_token,omitempty" valid:"required_if=GrantType refresh_token"`      // the user's refresh token
	RedirectURI  string           `url:"-" form:"redirect_uri,omitempty"`

	response AccessTokenResponse `url:"-" form:"-"`
}

type AccessTokenResponse struct {
	Token

	RefreshToken string  `json:"refresh_token,omitempty"` // allows you to refresh access token
	ExpiresIn    int     `json:"expires_in"`              // how long, in seconds, until the returned access token expires
	Scope        []Scope `json:"-"`                       // requested scopes

	Guild   *dlresource.Guild   `json:"guild,omitempty"`   // only returned for bots
	Webhook *dlresource.Webhook `json:"webhook,omitempty"` // only returned for webhooks
}

func (resp *AccessTokenResponse) UnmarshalJSON(data []byte) error {
	type Aux AccessTokenResponse
	aux := struct {
		*Aux
		Scope string `json:"scope"` // space-separated string
	}{Aux: (*Aux)(resp)}

	if err := json.Unmarshal(data, &aux); err != nil {
		return errstr.Error("can't unmarshal response").Wrap(err)
	}

	resp.Scope = decodeScopes(aux.Scope)
	return nil
}

func (req *AuthorizationToken) Path() string { return "/oauth2/token" }

func (req *AuthorizationToken) Response() *AccessTokenResponse { return &req.response }

func (req *AuthorizationToken) Prepare() dlrest.Request {
	return dlrest.Request{Method: "POST", ContentType: "application/x-www-form-urlencoded", Response: &req.response}
}

// ClientCredentialsGrant is a quick and easy way for bot developers to get their own bearer tokens for testing purposes.
// By making a POST request to the token URL with a grant type of client_credentials,
// using Basic authentication with your client id as the username and your client secret as the password,
// you will be returned an access token for the bot owner.
// Therefore, always be super-extra-very-we-are-not-kidding-like-really-be-secure-make-sure-your-info-is-not-in-your-source-code careful with your client_id and client_secret.
// We don't take kindly to imposters around these parts.
//
// You can specify scopes with the scope parameter, which is a list of OAuth2 scopes separated by spaces.
//
// TODO: client id and secred passed to auth header
type ClientCredentialsGrant struct {
	// ClientID     string  `url:"client_id"     valid:"required"` // your application's client id
	// ClientSecret string  `url:"client_secret" valid:"required"` // your application's client secret

	Scope []Scope `url:"-" form:"scope,space" valid:"required"` // the scopes requested in your authorization url, will be separated by space

	response AccessTokenResponse `url:"-" form:"-"`
}

func (req *ClientCredentialsGrant) Path() string { return "/oauth2/token" }

func (req *ClientCredentialsGrant) Response() *AccessTokenResponse { return &req.response }

func (req *ClientCredentialsGrant) Prepare() dlrest.Request {
	return dlrest.Request{Method: "POST", ContentType: "application/x-www-form-urlencoded", Response: &req.response}
}

func (req *ClientCredentialsGrant) EncodeForm(enc SchemaEncoder, v url.Values) error {
	if err := enc.Encode(req, v); err != nil {
		return err
	}

	v.Add("grant_type", string(GrantClientCredentials)) // not optional and differs for different grant flows
	return nil
}

// GetCurrentApplicationInformation Returns the bot's OAuth2 application object without flags
type GetCurrentApplicationInformation struct {
	response dlresource.Application `url:"-" form:"-"`
}

func (req *GetCurrentApplicationInformation) Path() string { return "/oauth2/applications/@me" }

func (req *GetCurrentApplicationInformation) Response() *dlresource.Application { return &req.response }

func (req *GetCurrentApplicationInformation) Prepare() dlrest.Request {
	return dlrest.Request{Method: "GET", Response: &req.response}
}

// GetCurrentAuthorizationInformation returns info about the current authorization. Requires authentication with a bearer token.
type GetCurrentAuthorizationInformation struct {
	response AuthorizationInformationResponse `url:"-" form:"-"`
}

type AuthorizationInformationResponse struct {
	Application dlresource.Application `json:"application"`    // the current application
	Scopes      []Scope                `json:"scopes"`         // the scopes the user has authorized the application for
	Expires     time.Time              `json:"expires"`        // when the access token expires
	User        *dlresource.User       `json:"user,omitempty"` // the user who has authorized, if the user has authorized with the identify scope
}

func (req *AuthorizationInformationResponse) Path() string { return "/oauth2/@me" }

func (req *GetCurrentAuthorizationInformation) Response() *AuthorizationInformationResponse {
	return &req.response
}

func (req *GetCurrentAuthorizationInformation) Prepare() dlrest.Request {
	return dlrest.Request{Method: "GET", Response: &req.response}
}
