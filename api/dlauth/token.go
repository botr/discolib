// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dlauth

import "encoding/base64"

type TokenType = string

const (
	TokenBot    TokenType = "Bot"
	TokenBearer TokenType = "Bearer"
)

type Token struct {
	Type  TokenType `json:"token_type"   url:"token_type"`
	Value string    `json:"access_token" url:"access_token"`
}

func (t Token) HeaderValue() string {
	return string(t.Type) + " " + t.Value
}

type BasicToken struct {
	Username string
	Password string
}

func (t BasicToken) HeaderValue() string {
	return "Basic " + base64.StdEncoding.EncodeToString([]byte(
		t.Username+":"+t.Password,
	))
}
