// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dlauth

import (
	"strings"
	"unsafe"

	"pkg.botr.me/discolib/internal/urlencode"
)

// Scope is a type for the OAuth2 scopes that Discord supports.
// Scopes that are behind a whitelist cannot be requested unless your application is on said whitelist,
// and may cause undocumented/error behavior in the OAuth2 flow if you request them from a user.
//
// NOTE: whitelisted scopes and group DM not supported
type Scope string

const (
	ScopeBot                        Scope = "bot"                          // for oauth2 bots, this puts the bot in the user's selected guild by default
	ScopeConnections                Scope = "connections"                  // allows /users/@me/connections to return linked third-party accounts
	ScopeEmail                      Scope = "email"                        // enables /users/@me to return an email
	ScopeIdentify                   Scope = "identify"                     // allows /users/@me without email
	ScopeGuilds                     Scope = "guilds"                       // allows /users/@me/guilds to return basic information about all of a user's guilds
	ScopeGuildsJoin                 Scope = "guilds.join"                  // allows /guilds/{guild.id}/members/{user.id} to be used for joining users to a guild
	ScopeWebhookIncoming            Scope = "webhook.incoming"             // this generates a webhook that is returned in the oauth token response for authorization code grants
	ScopeApplicationsCommands       Scope = "applications.commands"        // allows your app to use Slash Commands in a guild
	ScopeApplicationsCommandsUpdate Scope = "applications.commands.update" // allows your app to update its Slash Commands via this bearer token - client credentials grant only
)

type ResponseType string

const (
	ResponseCode  = "code"
	ResponseToken = "token"
)

// Prompt controls how the authorization flow handles existing authorizations
type Prompt string

const (
	PromptNone    Prompt = "none"    // it will skip the authorization screen and redirect them back to your redirect URI without requesting their authorization
	PromptConsent Prompt = "consent" // it will request them to reapprove their authorization
)

type GrantType string

const (
	GrantAuthorizationCode GrantType = "authorization_code"
	GrantRefreshToken      GrantType = "refresh_token"
	GrantClientCredentials GrantType = "client_credentials"
)

type SchemaEncoder = urlencode.SchemaEncoder
type SchemaDecoder = urlencode.SchemaDecoder

// DecodeScopes does opposite of encodeScopes
func decodeScopes(scope string) []Scope {
	ss := strings.Split(scope, " ")
	return *(*[]Scope)(unsafe.Pointer(&ss))
}
