// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dlinteraction

import (
	"context"

	"pkg.botr.me/discolib/api/dlresource"
	"pkg.botr.me/discolib/internal/errstr"
)

// Dispatcher interface ensures that Handler can forward requests and get a meaningful response
// even if receiver didn't implement some of the methods
type Dispatcher interface {
	// Ping received on URL change
	Ping(context.Context) error

	// Dispatcher must implement this method in order to response on interaction requests
	ApplicationCommand(context.Context, *dlresource.Interaction) (*dlresource.InteractionResponse, error)

	unimplemented() // make Dispatcher forward-compatible
}

// Handler is a wrapper for dispatching interactions based on their type
type Handler struct {
	dispatcher Dispatcher
}

func NewHandler(d Dispatcher) Handler { return Handler{d} }

func (h *Handler) Handle(ctx context.Context, it *dlresource.Interaction) (*dlresource.InteractionResponse, error) {
	var ir *dlresource.InteractionResponse
	var err error

	switch it.Type {
	case dlresource.InteractionPing:
		if err = h.dispatcher.Ping(ctx); err != nil {
			break
		}

		ir = &dlresource.InteractionResponse{Type: dlresource.InteractionCallbackPong}

	case dlresource.InteractionApplicationCommand:
		ir, err = h.dispatcher.ApplicationCommand(ctx, it)
		if err != nil {
			break
		}

		switch ir.Type {
		case dlresource.InteractionCallbackChannelMessageWithSource, dlresource.InteractionCallbackDeferredChannelMessageWithSource:
		default:
			err = errstr.Error("wrong response type").Describe(ir.Type)
		}

	default:
		err = errstr.Error("unknown interaction type").Describe(it.Type)
	}

	return ir, err
}
