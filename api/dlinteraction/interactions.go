// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dlinteraction

import (
	"context"

	"pkg.botr.me/discolib/api/dlresource"
	"pkg.botr.me/discolib/internal/errstr"
)

const ErrNotImplemented errstr.Error = "method not implemented for type"

// UnimplementedDispatcher should be inherited by interaction handlers
type UnimplementedDispatcher struct{}

func (*UnimplementedDispatcher) Ping(context.Context) error {
	return ErrNotImplemented.Describe(dlresource.InteractionPing)
}

func (*UnimplementedDispatcher) ApplicationCommand(context.Context, *dlresource.Interaction) (*dlresource.InteractionResponse, error) {
	return nil, ErrNotImplemented.Describe(dlresource.InteractionApplicationCommand)
}

func (*UnimplementedDispatcher) unimplemented()

// compile-type interface checking
var _ Dispatcher = (*UnimplementedDispatcher)(nil)
