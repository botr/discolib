// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dlresource

import "pkg.botr.me/discolib/api/dltype"

type PartialApplication struct {
	ID    dltype.Snowflake `json:"id"`    // the id of the app
	Flags int              `json:"flags"` // the application's public flags
}

type Application struct {
	PartialApplication

	Name                string           `json:"name"`                     // the name of the app
	Icon                *string          `json:"icon"`                     // the icon hash of the app
	Description         string           `json:"description"`              // the description of the app
	RPCOrigins          []string         `json:"rpc_origins,omitempty"`    // an array of rpc origin urls, if rpc is enabled
	BotPublic           bool             `json:"bot_public"`               // when false only app owner can join the app's bot to guilds
	BotRequireCodeGrant bool             `json:"bot_require_code_grant"`   // when true the app's bot will only join upon completion of the full oauth2 code grant flow
	Owner               User             `json:"owner"`                    // partial user object containing info on the owner of the application
	Summary             string           `json:"summary"`                  // if this application is a game sold on Discord, this field will be the summary field for the store page of its primary sku
	VerifyKey           string           `json:"verify_key"`               // the base64 encoded key for the GameSDK's GetTicket
	Team                *Team            `json:"team"`                     // if the application belongs to a team, this will be a list of the members of that team
	GuildID             dltype.Snowflake `json:"guild_id,omitempty"`       // if this application is a game sold on Discord, this field will be the guild to which it has been linked
	PrimarySkuID        dltype.Snowflake `json:"primary_sku_id,omitempty"` // if this application is a game sold on Discord, this field will be the id of the "Game SKU" that is created, if exists
	Slug                string           `json:"slug,omitempty"`           // if this application is a game sold on Discord, this field will be the URL slug that links to the store page
	CoverImage          string           `json:"cover_image,omitempty"`    // if this application is a game sold on Discord, this field will be the hash of the image on store embeds
}
