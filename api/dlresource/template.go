// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dlresource

import (
	"time"

	"pkg.botr.me/discolib/api/dltype"
)

// Template Represents a code that when used, creates a guild based on a snapshot of an existing one.
type Template struct {
	Code                  string           `json:"code"`                    // the template code (unique ID)
	Name                  string           `json:"name"`                    // template name
	Description           *string          `json:"description"`             // the description for the template
	UsageCount            int              `json:"usage_count"`             // number of times this template has been used
	CreatorID             dltype.Snowflake `json:"creator_id"`              // the ID of the user who created the template
	Creator               User             `json:"creator"`                 // the user who created the template
	CreatedAt             time.Time        `json:"created_at"`              // when this template was created  // TODO: checkout units
	UpdatedAt             time.Time        `json:"updated_at"`              // when this template was last synced to the source guild  // TODO: checkout units
	SourceGuildID         dltype.Snowflake `json:"source_guild_id"`         // the ID of the guild this template is based on
	SerializedSourceGuild Guild            `json:"serialized_source_guild"` // the guild snapshot this template contains
	IsDirty               *bool            `json:"is_dirty"`                // whether the template has unsynced changes
}
