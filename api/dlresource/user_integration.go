// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dlresource

import (
	"time"

	"pkg.botr.me/discolib/api/dltype"
)

type IntegrationExpireBehavior int

const (
	IntegrationExpireRemoveRole IntegrationExpireBehavior = 0
	IntegrationExpireKick       IntegrationExpireBehavior = 1
)

// IntegrationType twitch, youtube, discord
type IntegrationType string

const (
	IntegrationTypeTwitch  IntegrationType = "twitch"
	IntegrationTypeYoutube IntegrationType = "youtube"
	IntegrationTypeDiscord IntegrationType = "discord"
)

type Integration struct {
	ID              dltype.Snowflake           `json:"id"`                            // integration id
	Name            string                     `json:"name"`                          // integration name
	Type            IntegrationType            `json:"type"`                          // integration type
	Enabled         bool                       `json:"enabled"`                       // is this integration enabled
	Syncing         bool                       `json:"syncing,omitempty"`             // is this integration syncing
	RoleID          dltype.Snowflake           `json:"role_id,omitempty"`             // id that this integration uses for "subscribers"
	EnableEmoticons bool                       `json:"enable_emoticons,omitempty"`    // whether emoticons should be synced for this integration (twitch only currently)
	ExpireBehavior  *IntegrationExpireBehavior `json:"expire_behavior,omitempty"`     // the behavior of expiring subscribers
	ExpireGraceDays int                        `json:"expire_grace_period,omitempty"` // the grace period (in days) before expiring subscribers
	User            *User                      `json:"user,omitempty"`                // user for this integration
	Account         *IntegrationAccount        `json:"account,omitempty"`             // integration account information
	SyncedAt        *time.Time                 `json:"synced_at,omitempty"`           // when this integration was last synced
	SubscriberCount int                        `json:"subscriber_count,omitempty"`    // how many subscribers this integration has
	Revoked         bool                       `json:"revoked,omitempty"`             // has this integration been revoked
	Application     *IntegrationApplication    `json:"application,omitempty"`         // The bot/OAuth2 application for discord integrations
}
type IntegrationAccount struct {
	ID   string `json:"id"`   // id of the account
	Name string `json:"name"` // name of the account
}

type IntegrationApplication struct {
	ID          dltype.Snowflake `json:"id"`            // the id of the app
	Name        string           `json:"name"`          // the name of the app
	Icon        *string          `json:"icon"`          // the [icon hash](#DOCS_REFERENCE/image-formatting) of the app
	Description string           `json:"description"`   // the description of the app
	Summary     string           `json:"summary"`       // the description of the app
	Bot         *User            `json:"bot,omitempty"` // the bot associated with this application
}

type Ban struct {
	Reason *string `json:"reason"` // the reason for the ban
	User   User    `json:"user"`   // the banned user
}
