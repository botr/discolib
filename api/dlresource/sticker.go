package dlresource

import "pkg.botr.me/discolib/api/dltype"

type StickerType int

const (
	StickerStandard StickerType = 1 // an official sticker in a pack, part of Nitro or in a removed purchasable pack
	StickerGuild    StickerType = 2 // a sticker uploaded to a Boosted guild for the guild's members
)

type StickerFormatType int

const (
	StickerFormatPNG    StickerFormatType = 1
	StickerFormatAPNG   StickerFormatType = 2
	StickerFormatLottie StickerFormatType = 3
)

type Sticker struct {
	ID          dltype.Snowflake  `json:"id"`                   // id of the sticker
	PackID      dltype.Snowflake  `json:"pack_id,omitempty"`    // for standard stickers, id of the pack the sticker is from
	Name        string            `json:"name"`                 // name of the sticker
	Description *string           `json:"description"`          // description of the sticker
	Tags        string            `json:"tags"`                 // autocomplete/suggestion tags for the sticker (max 200 characters)
	Type        StickerType       `json:"type"`                 // type of sticker
	FormatType  StickerFormatType `json:"format_type"`          // type of sticker format
	Available   bool              `json:"available,omitempty"`  // whether this guild sticker can be used, may be false due to loss of Server Boosts
	GuildID     dltype.Snowflake  `json:"guild_id,omitempty"`   // id of the guild that owns this sticker
	User        User              `json:"user,omitempty"`       // the user that uploaded the guild sticker
	SortValue   int               `json:"sort_value,omitempty"` // the standard sticker's sort order within its pack
}

type StickerPack struct {
	ID             dltype.Snowflake `json:"id"`                         // id of the sticker pack
	Stickers       []Sticker        `json:"stickers"`                   // the stickers in the pack
	Name           string           `json:"name"`                       // name of the sticker pack
	SkuId          dltype.Snowflake `json:"sku_id"`                     // id of the pack's SKU
	CoverStickerId dltype.Snowflake `json:"cover_sticker_id,omitempty"` // id of a sticker in the pack which is shown as the pack's icon
	Description    string           `json:"description"`                // description of the sticker pack
	BannerAssetId  dltype.Snowflake `json:"banner_asset_id"`            // id of the sticker pack's banner image
}

// StickerItem is the smallest amount of data required to render a sticker
type StickerItem struct {
	ID         dltype.Snowflake  `json:"id"`          // id of the sticker
	Name       string            `json:"name"`        // name of the sticker
	FormatType StickerFormatType `json:"format_type"` // type of sticker format
}
