// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dlresource

import (
	"time"

	"golang.org/x/text/language"

	"pkg.botr.me/discolib/api/dltype"
)

type MessageNotificationLevel int

const (
	MessageNotificationAllMessages  MessageNotificationLevel = 0
	MessageNotificationOnlyMentions MessageNotificationLevel = 1
)

type ExplicitContentFilterLevel int

const (
	ExplicitContentFilterDisabled            ExplicitContentFilterLevel = 0
	ExplicitContentFilterMembersWithoutRoles ExplicitContentFilterLevel = 1
	ExplicitContentFilterAllMembers          ExplicitContentFilterLevel = 2
)

type MFALevel int

const (
	MFANone     MFALevel = 0
	MFAElevated MFALevel = 1
)

type VerificationLevel int

const (
	VerificationNone     VerificationLevel = 0 // unrestricted
	VerificationLow      VerificationLevel = 1 // must have verified email on account
	VerificationMedium   VerificationLevel = 2 // must be registered on Discord for longer than 5 minutes
	VerificationHigh     VerificationLevel = 3 // must be a member of the server for longer than 10 minutes
	VerificationVeryHigh VerificationLevel = 4 // must have a verified phone number
)

type SystemChannelFlag int

const (
	SystemChannelSuppressJoinNotifications    SystemChannelFlag = 1 << 0 //  Suppress member join notifications
	SystemChannelSuppressPremiumSubscriptions SystemChannelFlag = 1 << 1 //  Suppress server boost notifications
)

func (i SystemChannelFlag) Is(other SystemChannelFlag) bool { return i&other == other }
func (i SystemChannelFlag) Or(other SystemChannelFlag) bool { return i&other != 0 }

type GuildFeature string

const (
	GuildFeatureInviteSplash         GuildFeature = "INVITE_SPLASH"          // guild has access to set an invite splash background
	GuildFeatureVipRegions           GuildFeature = "VIP_REGIONS"            // guild has access to set 384kbps bitrate in voice (previously VIP voice servers)
	GuildFeatureVanityURL            GuildFeature = "VANITY_URL"             // guild has access to set a vanity URL
	GuildFeatureVerified             GuildFeature = "VERIFIED"               // guild is verified
	GuildFeaturePartnered            GuildFeature = "PARTNERED"              // guild is partnered
	GuildFeatureCommunity            GuildFeature = "COMMUNITY"              // guild can enable welcome screen and discovery, and receives community updates
	GuildFeatureCommerce             GuildFeature = "COMMERCE"               // guild has access to use commerce features (i.e. create store channels)
	GuildFeatureNews                 GuildFeature = "NEWS"                   // guild has access to create news channels
	GuildFeatureDiscoverable         GuildFeature = "DISCOVERABLE"           // guild is lurkable and able to be discovered in the directory
	GuildFeatureFeaturable           GuildFeature = "FEATURABLE"             // guild is able to be featured in the directory
	GuildFeatureAnimatedIcon         GuildFeature = "ANIMATED_ICON"          // guild has access to set an animated guild icon
	GuildFeatureBanner               GuildFeature = "BANNER"                 // guild has access to set a guild banner image
	GuildFeatureWelcomeScreenEnabled GuildFeature = "WELCOME_SCREEN_ENABLED" // guild has enabled the welcome screen
)

type GuildNSFWLevel int

const (
	GuildNSFWDefault       GuildNSFWLevel = 0
	GuildNSFWExplicit      GuildNSFWLevel = 1
	GuildNSFWSafe          GuildNSFWLevel = 2
	GuildNSFWAgeRestricted GuildNSFWLevel = 3
)

// UnavailableGuild object Represents an Offline Guild, or a Guild whose information has not been provided
// through Guild Create events during the Gateway connect.
type UnavailableGuild struct {
	ID          dltype.Snowflake `json:"id"`                    // guild id
	Unavailable bool             `json:"unavailable,omitempty"` // true if this guild is unavailable due to an outage
}

type GuildPreview struct {
	UnavailableGuild
	// unavailable field not present here

	Name                     string         `json:"name"`                                 // guild name (2-100 characters, excluding trailing and leading whitespace)
	Icon                     *string        `json:"icon"`                                 // [icon hash](#DOCS_REFERENCE/image-formatting)
	Splash                   *string        `json:"splash"`                               // [splash hash](#DOCS_REFERENCE/image-formatting)
	DiscoverySplash          *string        `json:"discovery_splash"`                     // [discovery splash hash](#DOCS_REFERENCE/image-formatting); only present for guilds with the "DISCOVERABLE" feature
	Emojis                   []Emoji        `json:"emojis"`                               // custom guild emojis
	Features                 []GuildFeature `json:"features"`                             // enabled guild features
	ApproximateMemberCount   int            `json:"approximate_member_count,omitempty"`   // approximate number of members in this guild, returned from the `GET /guild/<id>` endpoint when `with_counts` is `true`
	ApproximatePresenceCount int            `json:"approximate_presence_count,omitempty"` // approximate number of non-offline members in this guild, returned from the `GET /guild/<id>` endpoint when `with_counts` is `true`
	Description              *string        `json:"description"`                          // the description for the guild, if the guild is discoverable
}

// Guild in Discord represent an isolated collection of users and channels,
// and are often referred to as "servers" in the UI.
type Guild struct {
	GuildPreview

	IconHash                    *string                    `json:"icon_hash,omitempty"`                  // icon hash, returned when in the template object
	Owner                       bool                       `json:"owner,omitempty"`                      // true if the user is the owner of the guild
	OwnerID                     dltype.Snowflake           `json:"owner_id"`                             // id of owner
	Permissions                 string                     `json:"permissions,omitempty"`                // total permissions for the user in the guild (excludes overrides)
	Region                      dltype.Region              `json:"region"`                               // voice region id for the guild
	AFKChannelID                *dltype.Snowflake          `json:"afk_channel_id"`                       // id of afk channel
	AFKSeconds                  int                        `json:"afk_timeout"`                          // afk timeout in seconds
	WidgetEnabled               bool                       `json:"widget_enabled,omitempty"`             // true if the server widget is enabled
	WidgetChannelID             dltype.Snowflake           `json:"widget_channel_id,omitempty"`          // the channel id that the widget will generate an invite to, or `null` if set to no invite
	VerificationLevel           VerificationLevel          `json:"verification_level"`                   // verification level required for the guild
	DefaultMessageNotifications MessageNotificationLevel   `json:"default_message_notifications"`        // default message notifications level
	ExplicitContentFilter       ExplicitContentFilterLevel `json:"explicit_content_filter"`              // explicit content filter level
	Roles                       []Role                     `json:"roles"`                                // roles in the guild
	MFALevel                    MFALevel                   `json:"mfa_level"`                            // required MFA level for the guild
	ApplicationID               *dltype.Snowflake          `json:"application_id"`                       // application id of the guild creator if it is bot-created
	SystemChannelID             *dltype.Snowflake          `json:"system_channel_id"`                    // the id of the channel where guild notices such as welcome messages and boost events are posted
	SystemChannelFlags          SystemChannelFlag          `json:"system_channel_flags"`                 // system channel flags
	RulesChannelID              *dltype.Snowflake          `json:"rules_channel_id"`                     // the id of the channel where Community guilds can display rules and/or guidelines
	JoinedAt                    *time.Time                 `json:"joined_at,omitempty"`                  // when this guild was joined at
	Large                       bool                       `json:"large,omitempty"`                      // true if this is considered a large guild
	MemberCount                 int                        `json:"member_count,omitempty"`               // total number of members in this guild
	VoiceStates                 []VoiceState               `json:"voice_states,omitempty"`               // states of members currently in voice channels; lacks the `guild_id` key
	Members                     []GuildMember              `json:"members,omitempty"`                    // users in the guild
	Channels                    []Channel                  `json:"channels,omitempty"`                   // channels in the guild
	Threads                     []Channel                  `json:"threads,omitempty"`                    // all active threads in the guild that current user has permission to view |
	Presences                   []Presence                 `json:"presences,omitempty"`                  // presences of the members in the guild, will only include non-offline members if the size is greater than `large threshold`
	MaxPresences                int                        `json:"max_presences,omitempty"`              // the maximum number of presences for the guild (the default value, currently 25000, is in effect when `null` is returned)
	MaxMembers                  int                        `json:"max_members,omitempty"`                // the maximum number of members for the guild
	VanityURLCode               *string                    `json:"vanity_url_code"`                      // the vanity url code for the guild
	Banner                      *string                    `json:"banner"`                               // banner hash
	PremiumTier                 int                        `json:"premium_tier"`                         // premium tier (Server Boost level). 1-3 if present, otherwise 0
	PremiumSubscriptionCount    int                        `json:"premium_subscription_count,omitempty"` // the number of boosts this guild currently has
	PreferredLocale             language.Tag               `json:"preferred_locale"`                     // the preferred locale of a Community guild; used in server discovery and notices from Discord; defaults to "en-US"
	PublicUpdatesChannelID      *dltype.Snowflake          `json:"public_updates_channel_id"`            // the id of the channel where admins and moderators of Community guilds receive notices from Discord
	MaxVideoChannelUsers        int                        `json:"max_video_channel_users,omitempty"`    // the maximum amount of users in a video channel
	WelcomeScreen               *WelcomeScreen             `json:"welcome_screen,omitempty"`             // the welcome screen of a Community guild, shown to new members, returned in an Invite's guild object
	NSFWLevel                   GuildNSFWLevel             `json:"nsfw_level"`                           // guild NSFW level
	StageInstances              []StageInstance            `json:"stage_instances,omitempty"`            // array of stage instance objects	Stage instances in the guild
}

type GuildWidget struct {
	Enabled   bool             `json:"enabled"`    // whether the widget is enabled
	ChannelID dltype.Snowflake `json:"channel_id"` // the widget channel id
}

type GuildMember struct {
	User         *User              `json:"user,omitempty"`          // the user this guild member represents
	Nick         string             `json:"nick,omitempty"`          // this users guild nickname
	Roles        []dltype.Snowflake `json:"roles"`                   // array of role object ids
	JoinedAt     time.Time          `json:"joined_at"`               // when the user joined the guild
	PremiumSince time.Time          `json:"premium_since,omitempty"` // when the user started boosting the guild
	Deaf         bool               `json:"deaf"`                    // whether the user is deafened in voice channels
	Mute         bool               `json:"mute"`                    // whether the user is muted in voice channels
	Pending      *bool              `json:"pending,omitempty"`       // whether the user has not yet passed the guild's Membership Screening requirements
	Permissions  *Permission        `json:"pemissions,omitempty"`    // total permissions of the member in the channel, including overrides, returned when in the interaction object
}

type WelcomeScreen struct {
	Description     string                 `json:"description"`                    // the server description shown in the welcome screen
	WelcomeChannels []WelcomeScreenChannel `json:"welcome_channels" valid:"max=5"` // the channels shown in the welcome screen, up to 5
}

type WelcomeScreenChannel struct {
	ChannelId   dltype.Snowflake `json:"channel_id"`  // the channel's id
	Description string           `json:"description"` // the description shown for the channel
	EmojiID     dltype.Snowflake `json:"emoji_id"`    // the emoji id, if the emoji is custom
	EmojiName   string           `json:"emoji_name"`  // the emoji name if custom, the unicode character if standard, or null if no emoji is set
}
