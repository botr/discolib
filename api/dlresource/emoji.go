// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dlresource

import "pkg.botr.me/discolib/api/dltype"

type Emoji struct {
	ID            *dltype.Snowflake  `json:"id"`                       // emoji id
	Name          *string            `json:"name"`                     // emoji name (can be null only in reaction emoji objects)
	Roles         []dltype.Snowflake `json:"roles,omitempty"`          // roles this emoji is whitelisted to
	User          *User              `json:"user,omitempty"`           // user that created this emoji
	RequireColons bool               `json:"require_colons,omitempty"` // whether this emoji must be wrapped in colons
	Managed       bool               `json:"managed,omitempty"`        // whether this emoji is managed
	Animated      bool               `json:"animated,omitempty"`       // whether this emoji is animated
	Available     bool               `json:"available,omitempty"`      // whether this emoji can be used, may be false due to loss of Server Boosts
}
