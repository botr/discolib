package dlresource

import "pkg.botr.me/discolib/api/dltype"

type PrivacyLevel int

const (
	PrivacyPublic    PrivacyLevel = 1 // The Stage instance is visible publicly, such as on Stage discovery
	PrivacyGuildOnly PrivacyLevel = 2 // The Stage instance is visible to only guild members
)

type StageInstance struct {
	ID                   dltype.Snowflake `json:"id"`                    // The id of this Stage instance
	GuildID              dltype.Snowflake `json:"guild_id"`              // The guild id of the associated Stage channel
	ChannelID            dltype.Snowflake `json:"channel_id"`            // The id of the associated Stage channel
	Topic                string           `json:"topic"`                 // The topic of the Stage instance (1-120 characters)
	PrivacyLevel         PrivacyLevel     `json:"privacy_level"`         // The privacy level of the Stage instance
	DiscoverableDisabled bool             `json:"discoverable_disabled"` // Whether or not Stage discovery is disabled
}
