// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dlresource

import "pkg.botr.me/discolib/api/dltype"

type Team struct {
	Icon        *string          `json:"icon"`          // a hash of the image of the team's icon
	ID          dltype.Snowflake `json:"id"`            // the unique id of the team
	Members     []TeamMember     `json:"members"`       // the members of the team
	OwnerUserID dltype.Snowflake `json:"owner_user_id"` // the user id of the current team owner
}

type TeamMember struct {
	MembershipState int              `json:"membership_state"` // the user's membership state on the team
	Permissions     []string         `json:"permissions"`      // will always be ["*"]
	TeamID          dltype.Snowflake `json:"team_id"`          // the id of the parent team of which they are a member
	User            User             `json:"user"`             // the avatar, discriminator, id, and username of the user
}

type MembershipState int

const (
	MembershipStateInvited  MembershipState = 1
	MembershipStateAccepted MembershipState = 2
)
