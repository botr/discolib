// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dlresource

import (
	"bytes"
	"strconv"

	"pkg.botr.me/discolib/api/dltype"
)

type Permission uint64

const (
	PermissionNone                    Permission = 0       // Request no permissions
	PermissionCreateInstantInvite     Permission = 1 << 0  // Allows creation of instant invites
	PermissionKickMembers             Permission = 1 << 1  // Allows kicking members
	PermissionBanMembers              Permission = 1 << 2  // Allows banning members
	PermissionAdministrator           Permission = 1 << 3  // Allows all permissions and bypasses channel permission overwrites
	PermissionManageChannels          Permission = 1 << 4  // Allows management and editing of channels
	PermissionManageGuild             Permission = 1 << 5  // Allows management and editing of the guild
	PermissionAddReactions            Permission = 1 << 6  // Allows for the addition of reactions to messages
	PermissionViewAuditLog            Permission = 1 << 7  // Allows for viewing of audit logs
	PermissionPrioritySpeaker         Permission = 1 << 8  // Allows for using priority speaker in a voice channel
	PermissionStream                  Permission = 1 << 9  // Allows the user to go live
	PermissionViewChannel             Permission = 1 << 10 // Allows guild members to view a channel, which includes reading messages in text channels
	PermissionSendMessages            Permission = 1 << 11 // Allows for sending messages in a channel
	PermissionSendTTSMessages         Permission = 1 << 12 // Allows for sending of `/tts` messages
	PermissionManageMessages          Permission = 1 << 13 // Allows for deletion of other users messages
	PermissionEmbedLinks              Permission = 1 << 14 // Links sent by users with this permission will be auto-embedded
	PermissionAttachFiles             Permission = 1 << 15 // Allows for uploading images and files
	PermissionReadMessageHistory      Permission = 1 << 16 // Allows for reading of message history
	PermissionMentionEveryone         Permission = 1 << 17 // Allows for using the `@everyone` tag to notify all users in a channel, and the `@here` tag to notify all online users in a channel
	PermissionUseExternalEmojis       Permission = 1 << 18 // Allows the usage of custom emojis from other servers
	PermissionViewGuildInsights       Permission = 1 << 19 // Allows for viewing guild insights
	PermissionConnect                 Permission = 1 << 20 // Allows for joining of a voice channel
	PermissionSpeak                   Permission = 1 << 21 // Allows for speaking in a voice channel
	PermissionMuteMembers             Permission = 1 << 22 // Allows for muting members in a voice channel
	PermissionDeafenMembers           Permission = 1 << 23 // Allows for deafening of members in a voice channel
	PermissionMoveMembers             Permission = 1 << 24 // Allows for moving of members between voice channels
	PermissionUseVAD                  Permission = 1 << 25 // Allows for using voice-activity-detection in a voice channel
	PermissionChangeNickname          Permission = 1 << 26 // Allows for modification of own nickname
	PermissionManageNicknames         Permission = 1 << 27 // Allows for modification of other users nicknames
	PermissionManageRoles             Permission = 1 << 28 // Allows management and editing of roles
	PermissionManageWebhooks          Permission = 1 << 29 // Allows management and editing of webhooks
	PermissionManageEmojisAndStickers Permission = 1 << 30 // Allows management and editing of emojis
	PermissionUseApplicationCommands  Permission = 1 << 31 // Allows members to use slash commands in text channels
	PermissionRequestToSpeak          Permission = 1 << 32 // Allows for requesting to speak in stage channels
	PermissionManageThreads           Permission = 1 << 34 // Allows for deleting and archiving threads, and viewing all private threads
	PermissionCreatePublicThreads     Permission = 1 << 35 // Allows for creating and participating in threads
	PermissionCreatePrivateThreads    Permission = 1 << 36 // Allows for creating and participating in private threads
	PermissionUseExternalStickers     Permission = 1 << 37 // Allows the usage of custom stickers from other servers
	PermissionSendMessagesInThreads   Permission = 1 << 38 // Allows for sending messages in threads
	PermissionStartEmbeddedActivities Permission = 1 << 39 // Allows for launching activities (applications with the EMBEDDED flag) in a voice channel
)

func (p Permission) Is(other Permission) bool { return p&other == other }
func (p Permission) Or(other Permission) bool { return p&other != 0 }

func (p Permission) String() string {
	return strconv.FormatInt(int64(p), 10)
}

func (p *Permission) UnmarshalJSON(data []byte) error {
	data = bytes.Trim(data, `"`)
	n, err := strconv.ParseUint(string(data), 10, 64)
	if err == nil {
		*p = Permission(n)
	}
	return err
}

func (p *Permission) MarshalJSON() (b []byte, err error) {
	return []byte(`"` + p.String() + `"`), nil
}

func (p *Permission) FromString(s string) (err error) {
	i, err := strconv.ParseUint(s, 10, 64)
	if err != nil {
		return err
	}

	*p = Permission(i)
	return nil
}

// Role represents a set of permissions attached to a group of users.
// Roles have unique names, colors, and can be "pinned" to the side bar, causing their members to be listed separately.
// Roles are unique per guild, and can have separate permission profiles for the global context (guild) and channel context.
// The @everyone role has the same ID as the guild it belongs to.
//
// Roles without colors (color == 0) do not count towards the final computed color in the user list.
type Role struct {
	ID          dltype.Snowflake `json:"id"`                 // role id
	Name        string           `json:"name"`               // role name
	Color       dltype.Color     `json:"color"`              // integer representation of hexadecimal color code
	Hoist       bool             `json:"hoist"`              // if this role is pinned in the user listing
	Position    int              `json:"position,omitempty"` // position of this role
	Permissions Permission       `json:"permissions"`        // permission bit set
	Managed     bool             `json:"managed"`            // whether this role is managed by an integration
	Mentionable bool             `json:"mentionable"`        // whether this role is mentionable
	Tags        *RoleTags        `json:"tags,omitempty"`     // the tags this role has
}

type RoleTags struct {
	BotID         dltype.Snowflake `json:"bot_id,omitempty"`         // the id of the bot this role belongs to
	IntegrationID dltype.Snowflake `json:"integration_id,omitempty"` // the id of the integration this role belongs to

	// PremiumSubscriber null      `json:"premium_subscriber,omitempty"` // whether this is the guild's premium subscriber role
}
