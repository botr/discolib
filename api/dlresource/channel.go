// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dlresource

import (
	"time"

	"pkg.botr.me/discolib/api/dltype"
)

type ChannelType int

const (
	ChannelGuildText       ChannelType = 0  // a text channel within a server
	ChannelDM              ChannelType = 1  // a direct message between users
	ChannelGuildVoice      ChannelType = 2  // a voice channel within a server
	ChannelGroupDM         ChannelType = 3  // a direct message between multiple users
	ChannelGuildCategory   ChannelType = 4  // an organizational category that contains up to 50 channels
	ChannelGuildNews       ChannelType = 5  // a channel that users can follow and crosspost into their own server
	ChannelGuildStore      ChannelType = 6  // a channel in which game developers can sell their game on Discord
	ChannelNewsThread      ChannelType = 10 // a temporary sub-channel within a GUILD_NEWS channel
	ChannelPublicThread    ChannelType = 11 // a temporary sub-channel within a GUILD_TEXT channel
	ChannelPrivateThread   ChannelType = 12 // a temporary sub-channel within a GUILD_TEXT channel that is only viewable by those invited and those with the MANAGE_THREADS permission
	ChannelGuildStageVoice ChannelType = 13 // a voice channel for hosting events with an audience
)

type VideoQuality int

const (
	VideoQualityAuto VideoQuality = 1 // Discord chooses the quality for optimal performance
	VideoQualityFull VideoQuality = 2 // 720p
)

// Channel Object Represents a guild or DM channel within Discord.
type Channel struct {
	ID                         dltype.Snowflake `json:"id"`                                              // the id of this channel
	Type                       ChannelType      `json:"type"`                                            // the type of channel
	GuildID                    dltype.Snowflake `json:"guild_id,omitempty"`                              // the id of the guild
	Position                   int              `json:"position,omitempty"`                              // sorting position of the channel
	PermissionOverwrites       []Overwrite      `json:"permission_overwrites,omitempty"`                 // explicit permission overwrites for members and roles
	Name                       string           `json:"name,omitempty"  valid:"omitempty,min=2,max=100"` // the name of the channel (2-100 characters)
	Topic                      string           `json:"topic,omitempty" valid:"max=1024"`                // the channel topic (0-1024 characters)
	NSFW                       bool             `json:"nsfw,omitempty"`                                  // whether the channel is nsfw
	LastMessageID              dltype.Snowflake `json:"last_message_id,omitempty"`                       // the id of the last message sent in this channel (may not point to an existing or valid message)
	Bitrate                    int              `json:"bitrate,omitempty"`                               // the bitrate (in bits) of the voice channel
	UserLimit                  int              `json:"user_limit,omitempty"`                            // the user limit of the voice channel
	RateLimitPerUser           int              `json:"rate_limit_per_user,omitempty" valid:"max=21600"` // amount of seconds a user has to wait before sending another message (0-21600); bots, as well as users with the permission `manage_messages` or `manage_channel`, are unaffected
	Recipients                 []User           `json:"recipients,omitempty"`                            // the recipients of the DM
	Icon                       string           `json:"icon,omitempty"`                                  // icon hash
	OwnerID                    dltype.Snowflake `json:"owner_id,omitempty"`                              // id of the DM or creator
	ApplicationID              dltype.Snowflake `json:"application_id,omitempty"`                        // application id of the group DM creator if it is bot-created
	ParentID                   dltype.Snowflake `json:"parent_id,omitempty"`                             // for guild channels: id of the parent category for a channel (each parent category can contain up to 50 channels), for threads: id of the text channel this thread was created
	LastPinTimestamp           *time.Time       `json:"last_pin_timestamp,omitempty"`                    // when the last pinned message was pinned. This may be `null` in events such as `GUILD_CREATE` when a message is not pinned.
	RTCRegion                  dltype.Region    `json:"rtc_region,omitempty"`                            // voice region id for the voice channel, automatic when set to null                                                                  |
	VideoQualityMode           VideoQuality     `json:"video_quality_mode,omitempty"`                    // the camera video quality mode of the voice channel, 1 when not present                                            |
	MessageCount               int              `json:"message_count,omitempty"`                         // an approximate count of messages in a thread, stops counting at 50                                                                                                              |
	MemberCount                int              `json:"member_count,omitempty"`                          // an approximate count of users in a thread, stops counting at 50                                                                                                                 |
	ThreadMetadata             ThreadMetadata   `json:"thread_metadata,omitempty"`                       // thread-specific fields not needed by other channels                                                                                                                             |
	Member                     ThreadMember     `json:"member,omitempty"`                                // thread member object for the current user, if they have joined the thread, only included on certain API endpoints                                                               |
	DefaultAutoArchiveDuration int              `json:"default_auto_archive_duration,omitempty"`         // default duration for newly created threads, in minutes, to automatically archive the thread after recent activity, can be set to: 60, 1440, 4320, 10080
	Permissions                Permission       `json:"permissions,omitempty"`                           // computed permissions for the invoking user in the channel, including overwrites, only included when part of the resolved data received on a slash command interaction
}

type FollowedChannel struct {
	ChannelID dltype.Snowflake `json:"channel_id"` // source channel id
	WebhookID dltype.Snowflake `json:"webhook_id"` // created target webhook id
}

// OverwriteType either 0 (role) or 1 (member)
type OverwriteType int

const (
	OverwriteRole   OverwriteType = 0
	OverwriteMember OverwriteType = 0
)

// Overwrite Object See permissions for more information about the `allow` and `deny` fields.
type Overwrite struct {
	ID    dltype.Snowflake `json:"id"`    // role or user id
	Type  OverwriteType    `json:"type"`  //
	Allow Permission       `json:"allow"` // permission bit set
	Deny  Permission       `json:"deny"`  // permission bit set
}

type AllowedMentionType string

const (
	AllowedMentionRole     = "roles"    // Controls role mentions
	AllowedMentionUser     = "users"    // Controls user mentions
	AllowedMentionEveryone = "everyone" // Controls @everyone and @here mentions
)

// AllowedMentions Object allows for more granular control over mentions without various hacks to the message content.
// This will always validate against message content to avoid phantom pings
// (e.g. to ping everyone, you must still have `@everyone` in the message content), and check against user/bot permissions.
type AllowedMentions struct {
	Parse []AllowedMentionType `json:"parse"`                 // An AllowedMentionType to parse from the content.
	Roles []dltype.Snowflake   `json:"roles" valid:"max=100"` // Array of role_ids to mention (Max size of 100)
	Users []dltype.Snowflake   `json:"users" valid:"max=100"` // Array of user_ids to mention (Max size of 100)
}
