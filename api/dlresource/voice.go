// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dlresource

import "pkg.botr.me/discolib/api/dltype"

type VoiceState struct {
	GuildID    dltype.Snowflake `json:"guild_id,omitempty"`    // the guild id this voice state is for
	ChannelID  dltype.Snowflake `json:"channel_id"`            // the channel id this user is connected to
	UserID     dltype.Snowflake `json:"user_id"`               // the user id this voice state is for
	Member     *GuildMember     `json:"member,omitempty"`      // the guild member this voice state is for
	SessionID  string           `json:"session_id"`            // the session id for this voice state
	Deaf       bool             `json:"deaf"`                  // whether this user is deafened by the server
	Mute       bool             `json:"mute"`                  // whether this user is muted by the server
	SelfDeaf   bool             `json:"self_deaf"`             // whether this user is locally deafened
	SelfMute   bool             `json:"self_mute"`             // whether this user is locally muted
	SelfStream bool             `json:"self_stream,omitempty"` // whether this user is streaming using "Go Live"
	SelfVideo  bool             `json:"self_video"`            // whether this user's camera is enabled
	Suppress   bool             `json:"suppress"`              // whether this user is muted by the current user
}

type VoiceRegion struct {
	ID         dltype.Region `json:"id"`         //  unique ID for the region
	Name       string        `json:"name"`       //  name of the region
	VIP        bool          `json:"vip"`        //  true if this is a vip-only server
	Optimal    bool          `json:"optimal"`    //  true for a single server that is closest to the current user's client
	Deprecated bool          `json:"deprecated"` //  whether this is a deprecated voice region (avoid switching to these)
	Custom     bool          `json:"custom"`     //  whether this is a custom voice region (used for events/etc)
}
