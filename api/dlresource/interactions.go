// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dlresource

import (
	"encoding/json"

	"pkg.botr.me/discolib/api/dltype"
)

// var optionRegexp = regexp.MustCompile(`^[\w-]{1,32}$`)

type ApplicationCommandType int

const (
	ApplicationCommandChatInput ApplicationCommandType = 1 // Slash commands; a text-based command that shows up when a user types
	ApplicationCommandUser      ApplicationCommandType = 2 // A UI-based command that shows up when you right click or tap on a user
	ApplicationCommandMessage   ApplicationCommandType = 3 // A UI-based command that shows up when you right click or tap on a message
)

// ApplicationCommand is the base "command" model that belongs to an application.
// This is what you are creating when you POST a new command.
// A command, or each individual subcommand, can have a maximum of 25 options
type ApplicationCommand struct {
	ID                dltype.Snowflake       `json:"id"`                           // unique id of the command
	Type              ApplicationCommandType `json:"type,omitempty"`               // the type of command, defaults 1 if not set
	ApplicationID     dltype.Snowflake       `json:"application_id"`               // unique id of the parent application
	GuildID           dltype.Snowflake       `json:"guild_id,omitempty"`           // guild id of the command, if not global
	Name              string                 `json:"name" valid:"min=1,max=32"`    // 1-32 character name
	Description       string                 `json:"description" valid:"max=100"`  // 1-100 character description for CHAT_INPUT commands, empty string for USER and MESSAGE commands
	DefaultPermission *bool                  `json:"default_permission,omitempty"` // whether the command is enabled by default when the app is added to a guild (default: true)
	Version           dltype.Snowflake       `json:"version"`

	Options []ApplicationCommandOption `json:"options,omitempty"` // the parameters for the command (CHAT_INPUT)
}

type ApplicationCommandOptionType int

const (
	ApplicationCommandOptionSubCommand      ApplicationCommandOptionType = 1
	ApplicationCommandOptionSubCommandGroup ApplicationCommandOptionType = 2
	ApplicationCommandOptionString          ApplicationCommandOptionType = 3
	ApplicationCommandOptionInteger         ApplicationCommandOptionType = 4 // Any integer between -2^53 and 2^53
	ApplicationCommandOptionBoolean         ApplicationCommandOptionType = 5
	ApplicationCommandOptionUser            ApplicationCommandOptionType = 6
	ApplicationCommandOptionChannel         ApplicationCommandOptionType = 7 // Includes all channel types + categories
	ApplicationCommandOptionRole            ApplicationCommandOptionType = 8
	ApplicationCommandOptionMentionable     ApplicationCommandOptionType = 9  // Includes users and roles
	ApplicationCommandOptionNumber          ApplicationCommandOptionType = 10 // Any double between -2^53 and 2^53
)

// ApplicationCommandOption contains maximum of 25 choices per option
//
// NOTE: Required options must be listed before optional options
type ApplicationCommandOption struct {
	Type        ApplicationCommandOptionType `json:"type"              valid:"required"`      // value of ApplicationCommandOptionType
	Name        string                       `json:"name"              valid:"min=1,max=32"`  // 1-32 character name
	Description string                       `json:"description"       valid:"min=1,max=100"` // 1-100 character description
	Required    bool                         `json:"required,omitempty"`                      // (default: false) if the parameter is required or optional

	Choices      []ApplicationCommandOptionChoice `json:"choices,omitempty" valid:"max=25"` // choices for STRING, INTEGER, and NUMBER types for the user to pick from, max 25
	Options      []ApplicationCommandOption       `json:"options,omitempty" valid:"max=25"` // if the option is a subcommand or subcommand group type, this nested options will be the parameters
	ChannelTypes []ChannelType                    `json:"channel_types,omitempty"`          // if the option is a channel type, the channels shown will be restricted to these types
}

// ApplicationCommandOptionChoice are the only valid values for a user to pick if you specify choices for an option
type ApplicationCommandOptionChoice struct {
	Name  string        `json:"name" valid:"min=1,max=100"` // 1-100 character choice name
	Value dltype.StrNum `json:"value"`                      // value of the choice, up to 100 characters if string
}

// ApplicationCommandInteractionDataOption can either be a parameter and input value - in which case value will be set
// or it can denote a subcommand or group - in which case it will contain a top-level key and another array of options.
//
// NOTE: value and options are mututally exclusive.
type ApplicationCommandInteractionDataOption struct {
	Name string                       `json:"name"` // the name of the parameter
	Type ApplicationCommandOptionType `json:"type"` // value of ApplicationCommandOptionType

	Value   json.RawMessage                           `json:"value,omitempty"`   // the value of the pair
	Options []ApplicationCommandInteractionDataOption `json:"options,omitempty"` // present if this option is a group or subcommand
}

// GuildApplicationCommandPermissions returned when fetching the permissions for a command in a guild.
type GuildApplicationCommandPermissions struct {
	ID            dltype.Snowflake                `json:"id"`                       // the id of the command
	ApplicationID dltype.Snowflake                `json:"application_id,omitempty"` // the id of the application the command belongs to (optional when updating)
	GuildID       dltype.Snowflake                `json:"guild_id,omitempty"`       // the id of the guild (optional when updating)
	Permissions   []ApplicationCommandPermissions `json:"permissions"`              // the permissions for the command in the guild
}

type ApplicationCommandPermissionType int

const (
	ApplicationCommandPermissionRole ApplicationCommandPermissionType = 1
	ApplicationCommandPermissionUser ApplicationCommandPermissionType = 2
)

// ApplicationCommandPermissions allow you to enable or disable commands for specific users or roles within a guild.
type ApplicationCommandPermissions struct {
	ID         dltype.Snowflake                 `json:"id"`         // the id of the role or user
	Type       ApplicationCommandPermissionType `json:"type"`       // role or user
	Permission bool                             `json:"permission"` // true to allow, false, to disallow
}

type ComponentType int

const (
	ComponentActionRow  ComponentType = 1 // A container for other components
	ComponentButton     ComponentType = 2 // A clickable button
	ComponentSelectMenu ComponentType = 3 // A select menu for picking from choices
)

// Component is a framework for adding interactive elements to the messages your app or bot sends
type Component struct {
	Type ComponentType `json:"type"` // component type (all types)

	CustomID string `json:"custom_id,omitempty" valid:"max=100"` // a developer-defined identifier for the button, max 100 characters (Buttons, Select Menu)
	Disabled bool   `json:"disabled,omitempty"`                  // whether the button is disabled, default false (Buttons, Select Menu)

	Style ButtonStyle `json:"style,omitempty"`                    // one of button styles (Buttons)
	Label string      `json:"label,omitempty"     valid:"max=80"` // text that appears on the button, max 80 characters (Buttons)
	Emoji Emoji       `json:"emoji,omitempty"`                    // name, id, and animated (Buttons)
	URL   string      `json:"url,omitempty"`                      // a url for link-style buttons (Buttons)

	Options     []SelectOption `json:"options,omitempty"     valid:"max=25"`                         // the choices in the select, max 25 (Select Menu)
	Placeholder string         `json:"placeholder,omitempty" valid:"max=100"`                        // custom placeholder text if nothing is selected, max 100 characters (Select Menu)
	MinValues   int            `json:"min_values,omitempty"  valid:"omitempty,min=1,max=25"`         // the minimum number of items that must be chosen; default 1, min 0, max 25 (Select Menu)
	MaxValues   int            `json:"max_values,omitempty"  valid:"omitempty,min=MinValues,max=25"` // the maximum number of items that can be chosen; default 1, max 25 (Select Menu)

	Components []Component `json:"components,omitempty" valid:"max=5"` // sub-array of components of other types (ActionRow)
}

type ButtonStyle int

const (
	ButtonPrimary   ButtonStyle = 1
	ButtonSecondary ButtonStyle = 2
	ButtonSuccess   ButtonStyle = 3
	ButtonDanger    ButtonStyle = 4
	ButtonLink      ButtonStyle = 5
)

type SelectOption struct {
	Label       string `json:"label"                 valid:"max=100"` // the user-facing name of the option, max 100 characters
	Value       string `json:"value"                 valid:"max=100"` // the dev-define value of the option, max 100 characters
	Description string `json:"description,omitempty" valid:"max=50"`  // an additional description of the option, max 50 characters
	Emoji       Emoji  `json:"emoji,omitempty"`                       // id, name, and animated
	Default     bool   `json:"default,omitempty"`                     // will render this option as selected by default
}

type InteractionType int

const (
	InteractionPing               InteractionType = 1
	InteractionApplicationCommand InteractionType = 2
	InteractionMessageComponent   InteractionType = 3
)

// Interaction is the base "thing" that is sent when a user invokes a command,
// and is the same for Slash Commands and other future interaction types
//
// member is sent when the command is invoked in a guild, and user is sent when invoked in a DM
type Interaction struct {
	ID            dltype.Snowflake `json:"id"`                   // id of the interaction
	ApplicationID dltype.Snowflake `json:"application_id"`       // id of the application this interaction is for
	Type          InteractionType  `json:"type"`                 // the type of interaction
	Data          *InteractionData `json:"data,omitempty"`       // the command data payload
	GuildID       dltype.Snowflake `json:"guild_id,omitempty"`   // the guild it was sent from
	ChannelID     dltype.Snowflake `json:"channel_id,omitempty"` // the channel it was sent from
	Member        *GuildMember     `json:"member,omitempty"`     // guild member data for the invoking user
	User          *User            `json:"user,omitempty"`       // user object for the invoking user, if invoked in a DM
	Token         string           `json:"token"`                // a continuation token for responding to the interaction
	Version       int              `json:"version"`              // read-only property, always 1
	Message       *Message         `json:"omitempty"`            // for components, the message they were attached to
}

type InteractionData struct {
	ID       dltype.Snowflake                          `json:"id"`                 // the ID of the invoked command
	Name     string                                    `json:"name"`               // the name of the invoked command
	Type     ApplicationCommandType                    `json:"type"`               // the type of the invoked command
	Resolved *ResolvedData                             `json:"resolved,omitempty"` // converted users + roles + channels
	Options  []ApplicationCommandInteractionDataOption `json:"options,omitempty"`  // the params + values from the user

	CustomID      string           `json:"custom_id,omitempty"`      // the custom_id of the component (Component)
	ComponentType ComponentType    `json:"component_type,omitempty"` // the type of the component (Component)
	Values        []string         `json:"values,omitempty"`         // the values the user selected (Component/Select)
	TargetID      dltype.Snowflake `json:"target_id,omitempty"`      // id the of user or message targetted by a user or message command (User Command, Message Command)
}

type ResolvedData struct {
	Users    map[dltype.Snowflake]User        `json:"users,omitempty"`    // the IDs and User objects
	Members  map[dltype.Snowflake]GuildMember `json:"members,omitempty"`  // the IDs and partial Member objects
	Roles    map[dltype.Snowflake]Role        `json:"roles,omitempty"`    // the IDs and Role objects
	Channels map[dltype.Snowflake]Channel     `json:"channels,omitempty"` // the IDs and partial Channel objects
	Messages map[dltype.Snowflake]Channel     `json:"messages,omitempty"` // the IDs and partial Message objects
}

// MessageInteraction sent on the message object when the message is a response to an Interaction
type MessageInteraction struct {
	ID   dltype.Snowflake `json:"id"`   // id of the interaction
	Type InteractionType  `json:"type"` //
	Name string           `json:"name"` // the name of the ApplicationCommand
	User User             `json:"user"` // the user who invoked the interaction
}

type InteractionCallbackType int

const (
	InteractionCallbackPong                             InteractionCallbackType = 1 // ACK a Ping
	InteractionCallbackChannelMessageWithSource         InteractionCallbackType = 4 // respond to an interaction with a message
	InteractionCallbackDeferredChannelMessageWithSource InteractionCallbackType = 5 // ACK an interaction and edit to a response later, the user sees a loading state
	InteractionCallbackDeferredUpdateMessage            InteractionCallbackType = 6 // for components, ACK an interaction and edit the original message later; the user does not see a loading state
	InteractionCallbackUpdateMessage                    InteractionCallbackType = 7 // for components, edit the message the component was attached to
)

type InteractionResponse struct {
	Type InteractionCallbackType  `json:"type"`           // the type of response
	Data *InteractionCallbackData `json:"data,omitempty"` // an optional response message
}

type InteractionCallbackData struct {
	TTS             bool             `json:"tts,omitempty"`                                                          // is the response TTS
	Content         string           `json:"content,omitempty" valid:"required_if=Flags 64,required_without=Embeds"` // message content
	Embeds          []Embed          `json:"embeds,omitempty"  valid:"max=10,required_without=Content"`              // supports up to 10 embeds
	AllowedMentions *AllowedMentions `json:"allowed_mentions,omitempty"`                                             // allowed mentions object
	Flags           MessageFlag      `json:"flags,omitempty"`                                                        // set to MessageEphemeral to make your response ephemeral
	Components      []Component      `json:"components,omitempty"`
}
