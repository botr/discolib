// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dlresource

import "pkg.botr.me/discolib/api/dltype"

type Status string

const (
	StatusOffline   Status = "offline"
	StatusOnline    Status = "online"
	StatusIdle      Status = "idle"
	StatusDND       Status = "dnd"
	StatusInvisible Status = "invisible"
)

type Client string

const (
	ClientDesktop Client = "desktop"
	ClientMobile  Client = "mobile"
	ClientWeb     Client = "web"
)

type Presence struct {
	User         User              `json:"user"`          // the user presence is being updated for
	GuildID      dltype.Snowflake  `json:"guild_id"`      // id of the guild
	Status       Status            `json:"status"`        //
	Activities   []Activity        `json:"activities"`    // user's current activities
	ClientStatus map[Client]Status `json:"client_status"` // user's platform-dependent status, if a user is offline or invisible, the corresponding field is not present.
}
