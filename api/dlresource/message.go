// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dlresource

import (
	"time"

	"pkg.botr.me/discolib/api/dltype"
)

type MessageType int

const (
	MessageDefault                                 MessageType = 0
	MessageRecipientAdd                            MessageType = 1
	MessageRecipientRemove                         MessageType = 2
	MessageCall                                    MessageType = 3
	MessageChannelNameChange                       MessageType = 4
	MessageChannelIconChange                       MessageType = 5
	MessageChannelPinnedMessage                    MessageType = 6
	MessageGuildMemberJoin                         MessageType = 7
	MessageUserPremiumGuildSubscription            MessageType = 8
	MessageUserPremiumGuildSubscriptionTier1       MessageType = 9
	MessageUserPremiumGuildSubscriptionTier2       MessageType = 10
	MessageUserPremiumGuildSubscriptionTier3       MessageType = 11
	MessageChannelFollowAdd                        MessageType = 12
	MessageGuildDiscoveryDisqualified              MessageType = 14
	MessageGuildDiscoveryRequalified               MessageType = 15
	MessageGuildDiscoveryGracePeriodInitialWarning MessageType = 16
	MessageGuildDiscoveryGracePeriodFinalWarning   MessageType = 17
	MessageThreadCreated                           MessageType = 18
	MessageReply                                   MessageType = 19
	MessageChatInputCommand                        MessageType = 20
	MessageThreadStarterMessage                    MessageType = 21
	MessageGuildInviteReminder                     MessageType = 22
	MessageContextMenuCommand                      MessageType = 23
)

type MessageActivityType int

const (
	MessageActivityJoin        MessageActivityType = 1
	MessageActivitySpectate    MessageActivityType = 2
	MessageActivityListen      MessageActivityType = 3
	MessageActivityJoinRequest MessageActivityType = 5
)

type MessageFlag int

const (
	MessageFlagCrossposted          MessageFlag = 1 << 0 // this message has been published to subscribed channels (via Channel Following)
	MessageFlagIsCrosspost          MessageFlag = 1 << 1 // this message originated from a message in another channel (via Channel Following)
	MessageFlagSuppressEmbeds       MessageFlag = 1 << 2 // do not include any embeds when serializing this message
	MessageFlagSourceMessageDeleted MessageFlag = 1 << 3 // the source message for this crosspost has been deleted (via Channel Following)
	MessageFlagUrgent               MessageFlag = 1 << 4 // this message came from the urgent message system
	MessageHasThread                MessageFlag = 1 << 5 // this message has an associated thread, with the same id as the message
	MessageEphemeral                MessageFlag = 1 << 6 // this message is only visible to the user who invoked the Interaction
	MessageLoading                  MessageFlag = 1 << 7 // this message is an Interaction Response and the bot is "thinking"
)

func (i MessageFlag) Is(other MessageFlag) bool { return i&other == other }
func (i MessageFlag) Or(other MessageFlag) bool { return i&other != 0 }

// Message represents a message sent in a channel within Discord.
type Message struct {
	ID               dltype.Snowflake    `json:"id"`                            // id of the message
	ChannelID        dltype.Snowflake    `json:"channel_id"`                    // id of the channel the message was sent in
	GuildID          dltype.Snowflake    `json:"guild_id,omitempty"`            // id of the guild the message was sent in
	Author           User                `json:"author"`                        // the author of this message (not guaranteed to be a valid user, see below)
	Member           *GuildMember        `json:"member,omitempty"`              // member properties for this message's author
	Content          string              `json:"content,omitempty"`             // contents of the message
	Timestamp        time.Time           `json:"timestamp"`                     // when this message was sent
	EditedTimestamp  time.Time           `json:"edited_timestamp"`              // when this message was edited (or 0 if never)
	TTS              bool                `json:"tts"`                           // whether this was a TTS message
	MentionEveryone  bool                `json:"mention_everyone"`              // whether this message mentions everyone
	Mentions         []User              `json:"mentions"`                      // users specifically mentioned in the message
	MentionRoles     []dltype.Snowflake  `json:"mention_roles"`                 // roles specifically mentioned in this message
	MentionChannels  []ChannelMention    `json:"mention_channels,omitempty"`    // channels specifically mentioned in this message
	Attachments      []Attachment        `json:"attachments,omitempty"`         // any attached files
	Embeds           []Embed             `json:"embeds,omitempty"`              // any embedded content
	Reactions        []Reaction          `json:"reactions,omitempty"`           // reactions to the message
	Nonce            *dltype.StrNum      `json:"nonce,omitempty"`               // used for validating a message was sent
	Pinned           bool                `json:"pinned"`                        // whether this message is pinned
	WebhookID        dltype.Snowflake    `json:"webhook_id,omitempty"`          // if the message is generated by a webhook, this is the webhook's id
	Type             MessageType         `json:"type"`                          // type of message
	Activity         *MessageActivity    `json:"activity,omitempty"`            // sent with Rich Presence-related chat embeds
	Application      *MessageApplication `json:"application,omitempty"`         // sent with Rich Presence-related chat embeds
	MessageReference *MessageReference   `json:"message_reference,omitempty"`   // reference data sent with crossposted messages
	Flags            MessageFlag         `json:"flags,omitempty"`               // message flags combined as a bitfield
	Interaction      *MessageInteraction `json:"message_interaction,omitempty"` // sent if the message is a response to an Interaction
	Thread           *Channel            `json:"thread"`                        // the thread that was started from this message, includes ThreadMember object
	Components       []Component         `json:"components,omitempty"`          // sent if the message contains components like buttons, action rows, or other interactive components
	StickerItems     []StickerItem       `json:"sticker_items,omitempty"`       // sent if the message contains stickers
}

type MessageActivity struct {
	Type    MessageActivityType `json:"type"`               // type of message activity
	PartyID string              `json:"party_id,omitempty"` // party_id from a Rich Presence event
}

type MessageApplication struct {
	ID          dltype.Snowflake `json:"id"`                    // id of the application
	CoverImage  string           `json:"cover_image,omitempty"` // id of the embed's image asset
	Description string           `json:"description"`           // application's description
	Icon        *string          `json:"icon"`                  // id of the application's icon
	Name        string           `json:"name"`                  // name of the application
}

type MessageReference struct {
	MessageID       dltype.Snowflake `json:"message_id,omitempty"`         // id of the originating message
	ChannelID       dltype.Snowflake `json:"channel_id,omitempty"`         // id of the originating message's channel
	GuildID         dltype.Snowflake `json:"guild_id,omitempty"`           // id of the originating message's guild
	FailIfNotExists *bool            `json:"fail_if_not_exists,omitempty"` // whether to error if the referenced message doesn't exist instead of sending as a normal (non-reply) message (default: true)
}

type Reaction struct {
	Count int   `json:"count"` //  times this emoji has been used to react
	Me    bool  `json:"me"`    //  whether the current user reacted using this emoji
	Emoji Emoji `json:"emoji"` // emoji information
}

type Attachment struct {
	ID          dltype.Snowflake `json:"id"`           // attachment id
	Filename    string           `json:"filename"`     // name of file attached
	ContentType string           `json:"content_type"` //
	Size        int              `json:"size"`         // size of file in bytes
	URL         string           `json:"url"`          // source url of file
	ProxyURL    string           `json:"proxy_url"`    // a proxied url of file
	Height      *int             `json:"height"`       // height of file (if image)
	Width       *int             `json:"width"`        // width of file (if image)
}

type ChannelMention struct {
	ID      dltype.Snowflake `json:"id"`       // id of the channel
	GuildID dltype.Snowflake `json:"guild_id"` // id of the guild containing the channel
	Type    int              `json:"type"`     // the type of channel
	Name    string           `json:"name"`     // the name of the channel
}
