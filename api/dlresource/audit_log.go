// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dlresource

import (
	"encoding/json"

	"pkg.botr.me/discolib/api/dltype"
)

type AuditLogEvent int

const (
	AuditLogEventGuildUpdate            AuditLogEvent = 1
	AuditLogEventChannelCreate          AuditLogEvent = 10
	AuditLogEventChannelUpdate          AuditLogEvent = 11
	AuditLogEventChannelDelete          AuditLogEvent = 12
	AuditLogEventChannelOverwriteCreate AuditLogEvent = 13
	AuditLogEventChannelOverwriteUpdate AuditLogEvent = 14
	AuditLogEventChannelOverwriteDelete AuditLogEvent = 15
	AuditLogEventMemberKick             AuditLogEvent = 20
	AuditLogEventMemberPrune            AuditLogEvent = 21
	AuditLogEventMemberBanAdd           AuditLogEvent = 22
	AuditLogEventMemberBanRemove        AuditLogEvent = 23
	AuditLogEventMemberUpdate           AuditLogEvent = 24
	AuditLogEventMemberRoleUpdate       AuditLogEvent = 25
	AuditLogEventMemberMove             AuditLogEvent = 26
	AuditLogEventMemberDisconnect       AuditLogEvent = 27
	AuditLogEventBotAdd                 AuditLogEvent = 28
	AuditLogEventRoleCreate             AuditLogEvent = 30
	AuditLogEventRoleUpdate             AuditLogEvent = 31
	AuditLogEventRoleDelete             AuditLogEvent = 32
	AuditLogEventInviteCreate           AuditLogEvent = 40
	AuditLogEventInviteUpdate           AuditLogEvent = 41
	AuditLogEventInviteDelete           AuditLogEvent = 42
	AuditLogEventWebhookCreate          AuditLogEvent = 50
	AuditLogEventWebhookUpdate          AuditLogEvent = 51
	AuditLogEventWebhookDelete          AuditLogEvent = 52
	AuditLogEventEmojiCreate            AuditLogEvent = 60
	AuditLogEventEmojiUpdate            AuditLogEvent = 61
	AuditLogEventEmojiDelete            AuditLogEvent = 62
	AuditLogEventMessageDelete          AuditLogEvent = 72
	AuditLogEventMessageBulkDelete      AuditLogEvent = 73
	AuditLogEventMessagePin             AuditLogEvent = 74
	AuditLogEventMessageUnpin           AuditLogEvent = 75
	AuditLogEventIntegrationCreate      AuditLogEvent = 80
	AuditLogEventIntegrationUpdate      AuditLogEvent = 81
	AuditLogEventIntegrationDelete      AuditLogEvent = 82
)

type AuditLogChangeKey string

const (
	AuditLogChangeKeyName                        AuditLogChangeKey = "name"                          // (guild, string)        - name changed
	AuditLogChangeKeyDescription                 AuditLogChangeKey = "description"                   // (guild, string)        - description changed
	AuditLogChangeKeyIconHash                    AuditLogChangeKey = "icon_hash"                     // (guild, string)        - icon changed
	AuditLogChangeKeySplashHash                  AuditLogChangeKey = "splash_hash"                   // (guild, string)        - invite splash page artwork changed
	AuditLogDiscoverySplashHash                  AuditLogChangeKey = "discovery_splash_hash"         // (guild, string)        - discovery splash changed
	AuditLogBannerHash                           AuditLogChangeKey = "banner_hash"                   // (guild, string)        - guild banner changed
	AuditLogChangeKeyOwnerID                     AuditLogChangeKey = "owner_id"                      // (guild, Snowflake)     - owner changed
	AuditLogRegion                               AuditLogChangeKey = "region"                        // (guild, string)        - region changed
	AuditLogPreferredLocale                      AuditLogChangeKey = "preferred_locale"              // (guild, string)        - preferred locale changed
	AuditLogChangeKeyAFKChannelID                AuditLogChangeKey = "afk_channel_id"                // (guild, Snowflake)     - afk channel changed
	AuditLogAfkTimeout                           AuditLogChangeKey = "afk_timeout"                   // (guild, Integer)       - afk timeout duration changed
	AuditLogRulesChannelID                       AuditLogChangeKey = "rules_channel_id"              // (guild, Integer)       - id of the rules channel changed
	AuditLogPublicUpdatesChannelID               AuditLogChangeKey = "public_updates_channel_id"     // (guild, Integer)       - id of the public updates channel changed
	AuditLogChangeKeyMFALevel                    AuditLogChangeKey = "mfa_level"                     // (guild, Integer)       - two-factor auth requirement changed
	AuditLogChangeKeyVerificationLevel           AuditLogChangeKey = "verification_level"            // (guild, Integer)       - required verification level changed
	AuditLogChangeKeyExplicitContentFilter       AuditLogChangeKey = "explicit_content_filter"       // (guild, Integer)       - change in whose messages are scanned and deleted for explicit content in the server
	AuditLogChangeKeyDefaultMessageNotifications AuditLogChangeKey = "default_message_notifications" // (guild, Integer)       - default message notification level changed
	AuditLogChangeKeyVanityURLCode               AuditLogChangeKey = "vanity_url_code"               // (guild, string)        - guild invite vanity url changed
	AuditLogChangeKeyAdd                         AuditLogChangeKey = "$add"                          // (guild, []Role)        - new role added
	AuditLogChangeKeyRemove                      AuditLogChangeKey = "$remove"                       // (guild, []Role)        - role removed
	AuditLogChangeKeyPruneDeleteDays             AuditLogChangeKey = "prune_delete_days"             // (guild, Integer)       - change in number of days after which inactive and role-unassigned members are kicked
	AuditLogChangeKeyWidgetEnabled               AuditLogChangeKey = "widget_enabled"                // (guild, bool)          - server widget enabled/disable
	AuditLogChangeKeyWidgetChannelID             AuditLogChangeKey = "widget_channel_id"             // (guild, Snowflake)     - channel id of the server widget changed
	AuditLogChangeKeySystemChannelID             AuditLogChangeKey = "system_channel_id"             // (guild, Snowflake)     - id of the system channel changed
	AuditLogChangeKeyPosition                    AuditLogChangeKey = "position"                      // (channel, Integer)     - text or voice channel position changed
	AuditLogChangeKeyTopic                       AuditLogChangeKey = "topic"                         // (channel, string)      - text channel topic changed
	AuditLogChangeKeyBitrate                     AuditLogChangeKey = "bitrate"                       // (channel, Integer)     - voice channel bitrate changed
	AuditLogChangeKeyPermissionOverwrites        AuditLogChangeKey = "permission_overwrites"         // (channel, []Overwrite) - permissions on a channel changed
	AuditLogChangeKeyNSFW                        AuditLogChangeKey = "nsfw"                          // (channel, bool)        - channel nsfw restriction changed
	AuditLogChangeKeyApplicationID               AuditLogChangeKey = "application_id"                // (channel, Snowflake)   - application id of the added or removed webhook or bot
	AuditLogChangeKeyRateLimitPerUser            AuditLogChangeKey = "rate_limit_per_user"           // (channel, Integer)     - amount of seconds a user has to wait before sending another message changed
	AuditLogChangeKeyPermissions                 AuditLogChangeKey = "permissions"                   // (role, string)         - permissions for a role changed
	AuditLogChangeKeyColor                       AuditLogChangeKey = "color"                         // (role, Integer)        - role color changed
	AuditLogChangeKeyHoist                       AuditLogChangeKey = "hoist"                         // (role, bool)           - role is now displayed/no longer displayed separate from online users
	AuditLogChangeKeyMentionable                 AuditLogChangeKey = "mentionable"                   // (role, bool)           - role is now mentionable/unmentionable
	AuditLogChangeKeyAllow                       AuditLogChangeKey = "allow"                         // (role, string)         - a permission on a text or voice channel was allowed for a role
	AuditLogChangeKeyDeny                        AuditLogChangeKey = "deny"                          // (role, string)         - a permission on a text or voice channel was denied for a role
	AuditLogChangeKeyCode                        AuditLogChangeKey = "code"                          // (invite, string)       - invite code changed
	AuditLogChangeKeyChannelID                   AuditLogChangeKey = "channel_id"                    // (invite, Snowflake)    - channel for invite code changed
	AuditLogChangeKeyInviterID                   AuditLogChangeKey = "inviter_id"                    // (invite, Snowflake)    - person who created invite code changed
	AuditLogChangeKeyMaxUses                     AuditLogChangeKey = "max_uses"                      // (invite, Integer)      - change to max number of times invite code can be used
	AuditLogChangeKeyUses                        AuditLogChangeKey = "uses"                          // (invite, Integer)      - number of times invite code used changed
	AuditLogChangeKeyMaxAge                      AuditLogChangeKey = "max_age"                       // (invite, Integer)      - how long invite code lasts changed
	AuditLogChangeKeyTemporary                   AuditLogChangeKey = "temporary"                     // (invite, bool)         - invite code is temporary/never expires
	AuditLogChangeKeyDeaf                        AuditLogChangeKey = "deaf"                          // (user, bool)           - user server deafened/undeafened
	AuditLogChangeKeyMute                        AuditLogChangeKey = "mute"                          // (user, bool)           - user server muted/unmuted
	AuditLogChangeKeyNick                        AuditLogChangeKey = "nick"                          // (user, string)         - user nickname changed
	AuditLogChangeKeyAvatarHash                  AuditLogChangeKey = "avatar_hash"                   // (user, string)         - user avatar changed
	AuditLogChangeKeyID                          AuditLogChangeKey = "id"                            // (any, Snowflake)       - the id of the changed entity - sometimes used in conjunction with other keys
	AuditLogChangeKeyType                        AuditLogChangeKey = "type"                          // (any, IntOrStr)        - type of entity created
	AuditLogChangeKeyEnableEmoticons             AuditLogChangeKey = "enable_emoticons"              // (integration, bool)    - integration emoticons enabled/disabled
	AuditLogChangeKeyExpireBehavior              AuditLogChangeKey = "expire_behavior"               // (integration, Integer) - integration expiring subscriber behavior changed
	AuditLogExpireGracePeriod                    AuditLogChangeKey = "expire_grace_period"           // (integration, Integer) - integration expire grace period changed
	AuditLogUserLimit                            AuditLogChangeKey = "user_limit"                    // (channel, Integer)     - new user limit in a voice channel
)

type AuditLog struct {
	Webhooks        []Webhook       `json:"webhooks"`          // list of webhooks found in the audit log
	Users           []User          `json:"users"`             // list of users found in the audit log
	AuditLogEntries []AuditLogEntry `json:"audit_log_entries"` // list of audit log entries
	Integrations    []Integration   `json:"integrations"`      // list of partial integration objects
}

type AuditLogEntry struct {
	TargetID   *string            `json:"target_id"`         // id of the affected entity (webhook, user, role, etc.)
	Changes    []AuditLogChange   `json:"changes,omitempty"` // changes made to the target_id
	UserID     dltype.Snowflake   `json:"user_id"`           // the user who made the changes
	ID         dltype.Snowflake   `json:"id"`                // id of the entry
	ActionType AuditLogEvent      `json:"action_type"`       // type of action that occurred
	Options    *AuditLogEntryInfo `json:"options,omitempty"` // additional info for certain action types
	Reason     string             `json:"reason,omitempty"`  // the reason for the change (0-512 characters)
}

type AuditLogEntryInfo struct {
	DeleteMemberDays string           `json:"delete_member_days"` // number of days after which inactive members were kicked         (MEMBER_PRUNE)
	MembersRemoved   string           `json:"members_removed"`    // number of members removed by the prune                          (MEMBER_PRUNE)
	ChannelID        dltype.Snowflake `json:"channel_id"`         // channel in which the entities were targeted                     (MEMBER_MOVE & MESSAGE_PIN & MESSAGE_UNPIN & MESSAGE_DELETE)
	MessageID        dltype.Snowflake `json:"message_id"`         // id of the message that was targeted                             (MESSAGE_PIN & MESSAGE_UNPIN)
	Count            string           `json:"count"`              // number of entities that were targeted                           (MESSAGE_DELETE & MESSAGE_BULK_DELETE & MEMBER_DISCONNECT & MEMBER_MOVE)
	ID               dltype.Snowflake `json:"id"`                 // id of the overwritten entity                                    (CHANNEL_OVERWRITE_CREATE & CHANNEL_OVERWRITE_UPDATE & CHANNEL_OVERWRITE_DELETE)
	Type             string           `json:"type"`               // type of overwritten entity - "0" for "role" or "1" for "member" (CHANNEL_OVERWRITE_CREATE & CHANNEL_OVERWRITE_UPDATE & CHANNEL_OVERWRITE_DELETE)
	RoleName         string           `json:"role_name"`          // name of the role if type is "0" (not present if type is "1")    (CHANNEL_OVERWRITE_CREATE & CHANNEL_OVERWRITE_UPDATE & CHANNEL_OVERWRITE_DELETE)
}

type AuditLogChange struct {
	NewValue json.RawMessage   `json:"new_value,omitempty"` // new value of the key
	OldValue json.RawMessage   `json:"old_value,omitempty"` // old value of the key
	Key      AuditLogChangeKey `json:"key"`                 // name of audit log change key
}
