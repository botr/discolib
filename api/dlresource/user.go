// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dlresource

import (
	"golang.org/x/text/language"

	"pkg.botr.me/discolib/api/dltype"
)

type UserFlag int

const (
	UserNone                      = 0
	UserDiscordEmployee           = 1 << 0
	UserPartneredServerOwner      = 1 << 1
	UserHypeSquadEvents           = 1 << 2
	UserBugHunterLevel1           = 1 << 3
	UserHouseBravery              = 1 << 6
	UserHouseBrilliance           = 1 << 7
	UserHouseBalance              = 1 << 8
	UserEarlySupporter            = 1 << 9
	UserTeamUser                  = 1 << 10
	UserSystem                    = 1 << 12
	UserBugHunterLevel2           = 1 << 14
	UserVerifiedBot               = 1 << 16
	UserEarlyVerifiedBotDeveloper = 1 << 17
	UserDiscordCertifiedModerator = 1 << 18
)

func (i UserFlag) Is(other UserFlag) bool { return i&other == other }
func (i UserFlag) Or(other UserFlag) bool { return i&other != 0 }

// PremiumType denote the level of premium a user has.
type PremiumType int

const (
	PremiumNone         PremiumType = 0
	PremiumNitroClassic PremiumType = 1
	PremiumNitro        PremiumType = 2
)

type VisibilityType int

const (
	VisibilityNone     VisibilityType = 0 // invisible to everyone except the user themselves
	VisibilityEveryone VisibilityType = 1 // visible to everyone
)

type User struct {
	ID            dltype.Snowflake `json:"id"`                     // the user's id
	Username      string           `json:"username"`               // the user's username, not unique across the platform
	Discriminator string           `json:"discriminator"`          // the user's 4-digit discord-tag
	Avatar        *string          `json:"avatar"`                 // the user's avatar hash
	Bot           bool             `json:"bot,omitempty"`          // whether the user belongs to an OAuth2 application
	System        bool             `json:"system,omitempty"`       // whether the user is an Official Discord System user (part of the urgent message system)
	MFAEnabled    bool             `json:"mfa_enabled,omitempty"`  // whether the user has two factor enabled on their account
	Banner        *string          `json:"banner,omitempty"`       // the user's banner hash
	AccentColor   *dltype.Color    `json:"accent_color,omitempty"` // the user's banner color encoded as an integer representation of hexadecimal color code
	Locale        language.Tag     `json:"locale,omitempty"`       // the user's chosen language option (NOTE: not available for bots, requires identity scope)
	Verified      bool             `json:"verified,omitempty"`     // whether the email on this account has been verified (requires email scope)
	Email         string           `json:"email,omitempty"`        // the user's email (requires email scope)
	Flags         UserFlag         `json:"flags,omitempty"`        // the flags on a user's account
	PremiumType   PremiumType      `json:"premium_type,omitempty"` // the type of Nitro subscription on a user's account
	PublicFlags   UserFlag         `json:"public_flags,omitempty"` // the public flags on a user's account
}

// Connection object that the user has attached.
type Connection struct {
	ID           string         `json:"id"`                     // id of the connection account
	Name         string         `json:"name"`                   // the username of the connection account
	Type         string         `json:"type"`                   // the service of the connection (twitch, youtube)
	Revoked      bool           `json:"revoked,omitempty"`      // whether the connection is revoked
	Integrations []Integration  `json:"integrations,omitempty"` // an array of partial server integrations
	Verified     bool           `json:"verified"`               // whether the connection is verified
	FriendSync   bool           `json:"friend_sync"`            // whether friend sync is enabled for this connection
	ShowActivity bool           `json:"show_activity"`          // whether activities related to this connection will be shown in presence updates
	Visibility   VisibilityType `json:"visibility"`             // visibility of this connection
}
