// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dlresource

import (
	"time"

	"pkg.botr.me/discolib/api/dltype"
)

// TODO: check for total length

// Embed contains structured rich content.
//
// To facilitate showing rich content, rich embeds do not follow the traditional limits of message content.
// However, some limits are still in place to prevent excessively large embeds.
//
// The following table describes the limits (measured inclusively):
//  - title       256 characters
//  - description 2048 characters
//  - author.name 256 characters
//  - footer.text 2048 characters
//  - fields      25 field objects
//  - field.name  256 characters
//  - field.value 1024 characters
//
// Leading and trailing whitespace characters are not included (they are trimmed automatically).
//
// Additionally, the characters in all
// title, description, field.name, field.value, footer.text, and author.name fields
// must not exceed 6000 characters in total.
//
// Violating any of these constraints will result in a `Bad Request` response.
type Embed struct {
	Title       string          `json:"title,omitempty" valid:"max=256"`
	Type        string          `json:"type,omitempty"` // always "rich" for webhook embeds
	Description string          `json:"description,omitempty" valid:"max=2048"`
	URL         string          `json:"url,omitempty"`
	Timestamp   *time.Time      `json:"timestamp,omitempty"`
	Color       dltype.Color    `json:"color,omitempty"` // color code of the embed
	Footer      *EmbedFooter    `json:"footer,omitempty"`
	Image       *EmbedImage     `json:"image,omitempty"`
	Thumbnail   *EmbedThumbnail `json:"thumbnail,omitempty"`
	Video       *EmbedVideo     `json:"video,omitempty"`
	Provider    *EmbedProvider  `json:"provider,omitempty"` // discord internal
	Author      *EmbedAuthor    `json:"author,omitempty"`
	Fields      []EmbedField    `json:"fields,omitempty" valid:"max=25"`
}

type EmbedVideo struct {
	URL    string `json:"url,omitempty"`
	Height int    `json:"height,omitempty"`
	Width  int    `json:"width,omitempty"`
}

type EmbedImage struct {
	URL      string `json:"url,omitempty"`
	Height   int    `json:"height,omitempty"`
	Width    int    `json:"width,omitempty"`
	ProxyURL string `json:"proxy_url,omitempty"`
}

type EmbedThumbnail = EmbedImage

type EmbedProvider struct {
	Name string `json:"name,omitempty"`
	URL  string `json:"url,omitempty"`
}

type EmbedAuthor struct {
	Name         string `json:"name,omitempty" valid:"max=256"`
	URL          string `json:"url,omitempty"`
	IconURL      string `json:"icon_url,omitempty"`       // url of icon (only supports http(s) and attachments)
	ProxyIconURL string `json:"proxy_icon_url,omitempty"` // a proxied url of icon
}

type EmbedFooter struct {
	Text         string `json:"text" valid:"max=2048"`    // footer text
	IconURL      string `json:"icon_url,omitempty"`       // url of icon (only supports http(s) and attachments)
	ProxyIconURL string `json:"proxy_icon_url,omitempty"` // a proxied url of icon
}

type EmbedField struct {
	Name   string `json:"name"  valid:"max=256"`  // name of the field
	Value  string `json:"value" valid:"max=1024"` // value of the field
	Inline bool   `json:"inline,omitempty"`       // whether or not this field should display inline
}
