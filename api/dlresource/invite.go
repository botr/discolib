// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dlresource

import (
	"time"
)

type TargetUserType int

const (
	TargetUserStream TargetUserType = 1
)

// Invite Object Represents a code that when used, adds a user to a guild or group DM channel.
type Invite struct {
	Code                     string         `json:"code"`                                 // the invite code (unique ID)
	Guild                    *Guild         `json:"guild,omitempty"`                      // the guild this invite is for
	Channel                  *Channel       `json:"channel,omitempty"`                    // the channel this invite is for
	Inviter                  *User          `json:"inviter,omitempty"`                    // the user who created the invite
	TargetUser               *User          `json:"target_user,omitempty"`                // the target user for this invite
	TargetUserType           TargetUserType `json:"target_user_type,omitempty"`           // the type of user target for this invite
	ApproximatePresenceCount int            `json:"approximate_presence_count,omitempty"` // approximate count of online members (only present when target_user is set)
	ApproximateMemberCount   int            `json:"approximate_member_count,omitempty"`   // approximate count of total members
}

// InviteMetadata - Extra information about an invite, will extend the invite object
type InviteMetadata struct {
	Uses          int       `json:"uses"`       // number of times this invite has been used
	MaxUses       int       `json:"max_uses"`   // max number of times this invite can be used
	MaxAgeSeconds int       `json:"max_age"`    // duration (in seconds) after which the invite expires
	Temporary     bool      `json:"temporary"`  // whether this invite only grants temporary membership
	CreatedAt     time.Time `json:"created_at"` // when this invite was created
}
