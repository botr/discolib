package dlresource

import (
	"time"

	"pkg.botr.me/discolib/api/dltype"
)

// ThreadMetadata contains a number of thread-specific channel fields that are not needed by other channel types.
type ThreadMetadata struct {
	Archived            bool      `json:"archived"`              // whether the thread is archived
	AutoArchiveDuration int       `json:"auto_archive_duration"` // duration in minutes to automatically archive the thread after recent activity, can be set to: 60, 1440, 4320, 10080
	ArchiveTimestamp    time.Time `json:"archive_timestamp"`     // timestamp when the thread's archive status was last changed, used for calculating recent activity
	Locked              bool      `json:"locked,omitempty"`      // when a thread is locked, only users with MANAGE_THREADS can unarchive it
	Invitable           bool      `json:"invitable,omitempty"`   // whether non-moderators can add other non-moderators to a thread; only available on private threads
}

// ThreadMember is used to indicate whether a user has joined a thread or not.
//
// NOTE: id and user_id are ommitted on the member sent within each thread in the GUILD_CREATE event
type ThreadMember struct {
	ID            dltype.Snowflake `json:"id,omitempty"`      // the id of the thread
	UserID        dltype.Snowflake `json:"user_id,omitempty"` // the id of the user
	JoinTimestamp time.Time        `json:"join_timestamp"`    // the time the current user last joined the thread
	Flags         int              `json:"flags"`             // any user-thread settings, currently only used for notifications
}
