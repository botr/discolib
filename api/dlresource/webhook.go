// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dlresource

import "pkg.botr.me/discolib/api/dltype"

type WebhookType int

const (
	WebhookIncoming        WebhookType = 1 // Incoming Webhooks can post messages to channels with a generated token
	WebhookChannelFollower WebhookType = 2 // Channel Follower Webhooks are internal webhooks used with Channel Following to post new messages into channels
)

type Webhook struct {
	ID            dltype.Snowflake  `json:"id"`                 // the id of the webhook
	Type          WebhookType       `json:"type"`               // the type of the webhook
	GuildID       dltype.Snowflake  `json:"guild_id,omitempty"` // the guild id this webhook is for
	ChannelID     dltype.Snowflake  `json:"channel_id"`         // the channel id this webhook is for
	User          *User             `json:"user,omitempty"`     // the user this webhook was created by (not returned when getting a webhook with its token)
	Name          *string           `json:"name"`               // the default name of the webhook
	Avatar        *string           `json:"avatar"`             // the default avatar of the webhook
	Token         string            `json:"token,omitempty"`    // the secure token of the webhook (returned for Incoming Webhooks)
	ApplicationID *dltype.Snowflake `json:"application_id"`     // the bot/OAuth2 application that created this webhook
}
