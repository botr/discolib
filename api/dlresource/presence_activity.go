// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dlresource

import "pkg.botr.me/discolib/api/dltype"

type ActivityType int

const (
	ActivityGame      ActivityType = 0 // "Playing {name}"
	ActivityStreaming ActivityType = 1 // "Streaming {details}"
	ActivityListening ActivityType = 2 // "Listening to {name}"
	ActivityWatching  ActivityType = 3 // "Watching {name}"
	ActivityCustom    ActivityType = 4 // "{emoji} {name}"
	ActivityCompeting ActivityType = 5 // "Competing in {name}"
)

type ActivityFlag int

const (
	ActivityFlagInstance    ActivityFlag = 1 << 0
	ActivityFlagJoin        ActivityFlag = 1 << 1
	ActivityFlagSpectate    ActivityFlag = 1 << 2
	ActivityFlagJoinRequest ActivityFlag = 1 << 3
	ActivityFlagSync        ActivityFlag = 1 << 4
	ActivityFlagPlay        ActivityFlag = 1 << 5
)

func (i ActivityFlag) Is(other ActivityFlag) bool { return i&other == other }
func (i ActivityFlag) Or(other ActivityFlag) bool { return i&other != 0 }

type Activity struct {
	Name          string              `json:"name"`                     // the activity's name
	Type          ActivityType        `json:"type"`                     // activity type
	URL           string              `json:"url,omitempty"`            // stream url, is validated when type is 1
	CreatedAt     dltype.UnixSeconds  `json:"created_at"`               // unix timestamp of when the activity was added to the user's session
	Timestamps    *ActivityTimestamps `json:"timestamps,omitempty"`     // unix timestamps for start and/or end of the game
	ApplicationID dltype.Snowflake    `json:"application_id,omitempty"` // application id for the game
	Details       string              `json:"details,omitempty"`        // what the player is currently doing
	State         string              `json:"state,omitempty"`          // the user's current party status
	Emoji         *Emoji              `json:"emoji,omitempty"`          // the emoji used for a custom status
	Party         *ActivityParty      `json:"party,omitempty"`          // information for the current party of the player
	Assets        *ActivityAssets     `json:"assets,omitempty"`         // images for the presence and their hover texts
	Secrets       *ActivitySecrets    `json:"secrets,omitempty"`        // secrets for Rich Presence joining and spectating
	Instance      bool                `json:"instance,omitempty"`       // whether or not the activity is an instanced game session
	Flags         ActivityFlag        `json:"flags,omitempty"`          // activity flags `OR`d together, describes what the payload includes
}

type ActivityTimestamps struct {
	Start *dltype.UnixMillis `json:"start,omitempty"` // unix time (in milliseconds) of when the activity started
	End   *dltype.UnixMillis `json:"end,omitempty"`   // unix time (in milliseconds) of when the activity ends
}

type ActivityEmoji struct {
	Name     string           `json:"name"`               // the name of the emoji
	ID       dltype.Snowflake `json:"id,omitempty"`       // the id of the emoji
	Animated bool             `json:"animated,omitempty"` // whether this emoji is animated
}

type ActivityParty struct {
	ID   string `json:"id,omitempty"`   // the id of the party
	Size []int  `json:"size,omitempty"` // array of two integers (current_size, max_size) - used to show the party's current and maximum size
}

type ActivityAssets struct {
	LargeImage string `json:"large_image,omitempty"` // the id for a large asset of the activity, usually a snowflake
	LargeText  string `json:"large_text,omitempty"`  // text displayed when hovering over the large image of the activity
	SmallImage string `json:"small_image,omitempty"` // the id for a small asset of the activity, usually a snowflake
	SmallText  string `json:"small_text,omitempty"`  // text displayed when hovering over the small image of the activity
}

type ActivitySecrets struct {
	Join     string `json:"join,omitempty"`     // the secret for joining a party
	Spectate string `json:"spectate,omitempty"` // the secret for spectating a game
	Match    string `json:"match,omitempty"`    // the secret for a specific instanced match
}
