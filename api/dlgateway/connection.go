// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dlgateway

import (
	"strconv"

	"pkg.botr.me/discolib/api/dlresource"
	"pkg.botr.me/discolib/api/dltype"
)

const Version = 10

type Encoding string

const (
	EncodingJSON Encoding = "json"
	EncodingETF  Encoding = "etf"
)

type Compression string

const (
	CompressNone       Compression = ""
	CompressZlibStream Compression = "zlib-stream"
)

type ConnectionProperties struct {
	OS      string `json:"os"      etf:"os"      valid:"required"` // operating system
	Browser string `json:"browser" etf:"browser" valid:"required"` // library name
	Device  string `json:"device"  etf:"device"  valid:"required"` // library name
}

type StatusUpdate struct {
	Since      *dltype.UnixMillis    `json:"since"      etf:"since"`                              // unix time (in milliseconds) of when the client went idle, or null if the client is not idle
	Activities []dlresource.Activity `json:"activities" etf:"activities" valid:"omitempty,min=1"` // null, or the user's activities
	Status     dlresource.Status     `json:"status"     etf:"status"`                             // the user's new status
	AFK        bool                  `json:"afk"        etf:"afk"`                                // whether or not the client is afk
}

type CloseEventCode int

const (
	CloseNormalClosure CloseEventCode = 1000
	CloseGoingAway     CloseEventCode = 1001

	CloseUnknownError         CloseEventCode = 4000 // We're not sure what went wrong. Try reconnecting
	CloseUnknownOpcode        CloseEventCode = 4001 // You sent an invalid Gateway opcode or an invalid payload for an opcode. Don't do that
	CloseDecodeError          CloseEventCode = 4002 // You sent an invalid payload to us. Don't do that
	CloseNotAuthenticated     CloseEventCode = 4003 // You sent us a payload prior to identifying
	CloseAuthenticationFailed CloseEventCode = 4004 // The account token sent with your identify payload is incorrect
	CloseAlreadyAuthenticated CloseEventCode = 4005 // You sent more than one identify payload. Don't do that
	CloseInvalidSeq           CloseEventCode = 4007 // The sequence sent when resuming the session was invalid. Reconnect and start a new session
	CloseRateLimited          CloseEventCode = 4008 // Woah nelly! You're sending payloads to us too quickly. Slow it down! You will be disconnected on receiving this
	CloseSessionTimedOut      CloseEventCode = 4009 // Your session timed out. Reconnect and start a new one
	CloseInvalidShard         CloseEventCode = 4010 // You sent us an invalid shard when identifying
	CloseShardingRequired     CloseEventCode = 4011 // The session would have handled too many guilds - you are required to shard your connection in order to connect
	CloseInvalidAPIVersion    CloseEventCode = 4012 // You sent an invalid version for the gateway
	CloseInvalidIntent        CloseEventCode = 4013 // You sent an invalid intent for a Gateway Intent. You may have incorrectly calculated the bitwise value
	CloseDisallowedIntent     CloseEventCode = 4014 // You sent a disallowed intent for a Gateway Intent. You may have tried to specify an intent that you have not enabled or are not whitelisted for
)

func (c CloseEventCode) Error() string {
	return "gateway closed connection (code: " + strconv.Itoa(int(c)) + ")"
}

// Resumable returns whether it is possible to resume connection if transport closing with this error code.
// As documentation says, if client closing connection with 1000 or 1001 status code it will also drop connection.
func (c CloseEventCode) Resumable() bool {
	switch c {
	case CloseNormalClosure, CloseGoingAway:
		return false
	}
	return true
}
