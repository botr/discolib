// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dlgateway

// Intent defines group of events defined by Discord
type Intent uint64

//go:generate enumer -type=Intent -trimprefix=Intent $GOFILE
const (
	IntentGuilds                 Intent = 1 << 0
	IntentGuildMembers           Intent = 1 << 1
	IntentGuildBans              Intent = 1 << 2
	IntentGuildEmojis            Intent = 1 << 3
	IntentGuildIntegrations      Intent = 1 << 4
	IntentGuildWebhooks          Intent = 1 << 5
	IntentGuildInvites           Intent = 1 << 6
	IntentGuildVoiceStates       Intent = 1 << 7
	IntentGuildPresences         Intent = 1 << 8
	IntentGuildMessages          Intent = 1 << 9
	IntentGuildMessageReactions  Intent = 1 << 10
	IntentGuildMessageTyping     Intent = 1 << 11
	IntentDirectMessages         Intent = 1 << 12
	IntentDirectMessageReactions Intent = 1 << 13
	IntentDirectMessageTyping    Intent = 1 << 14

	// composite events to group some events
	intentThreadMembers    Intent = IntentGuilds | IntentGuildMembers
	intentPinsUpdate       Intent = IntentGuilds | IntentDirectMessages
	intentMessages         Intent = IntentGuildMessages | IntentDirectMessages
	intentMessageReactions Intent = IntentGuildMessageReactions | IntentDirectMessageReactions
	intentMessageTyping    Intent = IntentGuildMessageTyping | IntentDirectMessageTyping
)

// EventType describes values of payload name to dispatch events from dispatch payload
type EventType string

const (
	EventReady                      EventType = "READY"                         // contains the initial state information
	EventResumed                    EventType = "RESUMED"                       // response to Resume
	EventChannelCreate              EventType = "CHANNEL_CREATE"                //  new guild channel created
	EventChannelUpdate              EventType = "CHANNEL_UPDATE"                //  channel was updated
	EventChannelDelete              EventType = "CHANNEL_DELETE"                //  channel was deleted
	EventChannelPinsUpdate          EventType = "CHANNEL_PINS_UPDATE"           //  message was pinned or unpinned
	EventThreadCreate               EventType = "THREAD_CREATE"                 //  thread created, also sent when being added to a private thread
	EventThreadUpdate               EventType = "THREAD_UPDATE"                 //  thread was updated
	EventThreadDelete               EventType = "THREAD_DELETE"                 //  thread was deleted
	EventThreadListSync             EventType = "THREAD_LIST_SYNC"              //  sent when gaining access to a channel, contains all active threads in that channel
	EventThreadMemberUpdate         EventType = "THREAD_MEMBER_UPDATE"          //  thread member for the current user was updated
	EventThreadMembersUpdate        EventType = "THREAD_MEMBERS_UPDATE"         //  some user(s) were added to or removed from a thread
	EventGuildCreate                EventType = "GUILD_CREATE"                  //  lazy-load for unavailable guild, guild became available, or user joined a new guild
	EventGuildUpdate                EventType = "GUILD_UPDATE"                  //  guild was updated
	EventGuildDelete                EventType = "GUILD_DELETE"                  //  guild became unavailable, or user left/was removed from a guild
	EventGuildBanAdd                EventType = "GUILD_BAN_ADD"                 //  user was banned from a guild
	EventGuildBanRemove             EventType = "GUILD_BAN_REMOVE"              //  user was unbanned from a guild
	EventGuildEmojisUpdate          EventType = "GUILD_EMOJIS_UPDATE"           //  guild emojis were updated
	EventGuildIntegrationsUpdate    EventType = "GUILD_INTEGRATIONS_UPDATE"     //  guild integration was updated
	EventGuildMemberAdd             EventType = "GUILD_MEMBER_ADD"              //  new user joined a guild
	EventGuildMemberRemove          EventType = "GUILD_MEMBER_REMOVE"           //  user was removed from a guild
	EventGuildMemberUpdate          EventType = "GUILD_MEMBER_UPDATE"           //  guild member was updated
	EventGuildMembersChunk          EventType = "GUILD_MEMBERS_CHUNK"           // response to Request Guild Members
	EventGuildRoleCreate            EventType = "GUILD_ROLE_CREATE"             //  guild role was created
	EventGuildRoleUpdate            EventType = "GUILD_ROLE_UPDATE"             //  guild role was updated
	EventGuildRoleDelete            EventType = "GUILD_ROLE_DELETE"             //  guild role was deleted
	EventInviteCreate               EventType = "INVITE_CREATE"                 //  invite to a channel was created
	EventInviteDelete               EventType = "INVITE_DELETE"                 //  invite to a channel was deleted
	EventMessageCreate              EventType = "MESSAGE_CREATE"                //  message was created
	EventMessageUpdate              EventType = "MESSAGE_UPDATE"                //  message was edited
	EventMessageDelete              EventType = "MESSAGE_DELETE"                //  message was deleted
	EventMessageDeleteBulk          EventType = "MESSAGE_DELETE_BULK"           //  multiple messages were deleted at once
	EventMessageReactionAdd         EventType = "MESSAGE_REACTION_ADD"          //  user reacted to a message
	EventMessageReactionRemove      EventType = "MESSAGE_REACTION_REMOVE"       //  user removed a reaction from a message
	EventMessageReactionRemoveAll   EventType = "MESSAGE_REACTION_REMOVE_ALL"   //  all reactions were explicitly removed from a message
	EventMessageReactionRemoveEmoji EventType = "MESSAGE_REACTION_REMOVE_EMOJI" //  all reactions for a given emoji were explicitly removed from a message
	EventPresenceUpdate             EventType = "PRESENCE_UPDATE"               //  user was updated
	EventTypingStart                EventType = "TYPING_START"                  //  user started typing in a channel
	EventUserUpdate                 EventType = "USER_UPDATE"                   // properties about the user changed
	EventVoiceStateUpdate           EventType = "VOICE_STATE_UPDATE"            //  someone joined, left, or moved a voice channel
	EventVoiceServerUpdate          EventType = "VOICE_SERVER_UPDATE"           // guild's voice server was updated
	EventWebhooksUpdate             EventType = "WEBHOOKS_UPDATE"               //  guild channel webhook was created, update, or deleted
	EventInteractionCreate          EventType = "INTERACTION_CREATE"            // user used a Slash Command
)

// Intents returns bitmask of all intents that enable receiving events of such type
func (e EventType) Intents() Intent {
	switch e {
	case EventGuildCreate, EventGuildUpdate, EventGuildDelete, EventGuildRoleCreate, EventGuildRoleUpdate, EventGuildRoleDelete, EventChannelCreate, EventChannelUpdate, EventChannelDelete:
		return IntentGuilds
	case EventGuildMemberAdd, EventGuildMemberUpdate, EventGuildMemberRemove:
		return IntentGuildMembers
	case EventGuildBanAdd, EventGuildBanRemove:
		return IntentGuildBans
	case EventGuildEmojisUpdate:
		return IntentGuildEmojis
	case EventGuildIntegrationsUpdate:
		return IntentGuildIntegrations
	case EventWebhooksUpdate:
		return IntentGuildWebhooks
	case EventInviteCreate, EventInviteDelete:
		return IntentGuildInvites
	case EventVoiceStateUpdate:
		return IntentGuildVoiceStates
	case EventPresenceUpdate:
		return IntentGuildPresences
	case EventMessageDeleteBulk:
		return IntentGuildMessages
	case EventThreadCreate, EventThreadUpdate, EventThreadDelete, EventThreadListSync, EventThreadMemberUpdate:
		return IntentGuilds

	case EventThreadMembersUpdate:
		return intentThreadMembers

	case EventChannelPinsUpdate:
		return intentPinsUpdate

	case EventMessageCreate, EventMessageUpdate, EventMessageDelete:
		return intentMessages

	case EventMessageReactionAdd, EventMessageReactionRemove, EventMessageReactionRemoveAll, EventMessageReactionRemoveEmoji:
		return intentMessageReactions

	case EventTypingStart:
		return intentMessageTyping
	}

	return 0
}
