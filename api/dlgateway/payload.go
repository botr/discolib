// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dlgateway

import (
	"encoding/json"
	"strconv"
	"time"

	"pkg.botr.me/discolib/api/dltype"
	"pkg.botr.me/discolib/internal/errstr"
)

type Unmarshaler interface {
	Unmarshal([]byte, interface{}) error
}

// Payload contains packet
// s and t are null when OpCode is not OpDispatch
type Payload struct {
	OpCode OpCode     `json:"op"          etf:"op"`          // opcode for the payload
	Data   RawPayload `json:"d"           etf:"d"`           // event data
	Type   EventType  `json:"t,omitempty" etf:"t,omitempty"` // the event name for this payload
	Seq    uint64     `json:"s,omitempty" etf:"s,omitempty"` // sequence number, used for resuming sessions and heartbeats
}

// RawPayload is a raw encoded value in payload
type RawPayload []byte

// UnmarshalETF copies data into term
func (t *RawPayload) UnmarshalETF(data []byte) error {
	if t == nil {
		return errstr.Error("RawPayload: UnmarshalETF on nil pointer")
	}

	*t = append((*t)[0:0], data...)
	return nil
}

// UnmarshalJSON copies data into term
func (t *RawPayload) UnmarshalJSON(data []byte) error {
	if t == nil {
		return errstr.Error("RawPayload: UnmarshalJSON on nil pointer")
	}

	*t = append((*t)[0:0], data...)
	return nil
}

func (t RawPayload) MarshalJSON() ([]byte, error) {
	if t == nil {
		return []byte("null"), nil
	}
	return t, nil
}

// OpCode tags gateway opcode that denotes the Payload type.
type OpCode uint8

//go:generate enumer -type=OpCode -trimprefix=Op $GOFILE
const (
	OpDispatch            OpCode = 0  // An event was dispatched (received)
	OpHeartbeat           OpCode = 1  // Fired periodically by the client to keep the connection alive (sent, received)
	OpIdentify            OpCode = 2  // Starts a new session during the initial handshake (sent)
	OpPresenceUpdate      OpCode = 3  // Update the client's presence (sent)
	OpVoiceStateUpdate    OpCode = 4  // Used to join/leave or move between voice channels (sent)
	OpResume              OpCode = 6  // Resume a previous session that was disconnected (sent)
	OpReconnect           OpCode = 7  // You should attempt to reconnect and resume immediately (received)
	OpRequestGuildMembers OpCode = 8  // Request information about offline guild members in a large guild (sent)
	OpInvalidSession      OpCode = 9  // The session has been invalidated. You should reconnect and identify/resume accordingly (received)
	OpHello               OpCode = 10 // Sent immediately after connecting (received)
	OpHeartbeatACK        OpCode = 11 // Sent in response to receiving a heartbeat to acknowledge that it has been received (received)
)

// HeartbeatPayload (opcode 1) Used to maintain an active gateway connection.
// Must be sent every heartbeat_interval milliseconds after the Opcode 10 Hello payload is received.
// Payload Data is the last sequence number received by the client. If you have not yet received one, send null
type HeartbeatPayload struct {
	Seq uint64
}

func (HeartbeatPayload) OpCode() OpCode { return OpHeartbeat }

func (p *HeartbeatPayload) MarshalJSON() ([]byte, error) {
	s := "null"
	if p.Seq > 0 {
		s = strconv.FormatUint(p.Seq, 10)
	}
	return []byte(s), nil
}

// IdentifyPayload (opcode 2) sent by client after connection
type IdentifyPayload struct {
	Token          string               `json:"token"                     etf:"token"                     valid:"required"`                 // authentication token
	Properties     ConnectionProperties `json:"properties"                etf:"properties"                valid:"required"`                 // connection properties
	LargeThreshold int                  `json:"large_threshold,omitempty" etf:"large_threshold,omitempty" valid:"omitempty,min=50,max=250"` // (default: 50) value between 50 and 250, total number of members where the gateway will stop sending offline members in the guild member list
	Shard          []int                `json:"shard,omitempty"           etf:"shard,omitempty"           valid:"omitempty,len=2"`          // [shard_id, num_shards] used for Guild Sharding
	Presence       *StatusUpdate        `json:"presence,omitempty"        etf:"presence,omitempty"`                                         // presence structure for initial presence information
	Intents        Intent               `json:"intents"                   etf:"intents"`                                                    // the Gateway Intents you wish to receive

	// While not deprecated, Guild Subscriptions have been superceded by Gateway Intents.
	// GuildSubscriptions bool `json:"guild_subscriptions,omitempty" etf:"guild_subscriptions,omitempty"` // (default: true) enables dispatching of guild subscription events (presence and typing events)

	// Compress bool `json:"compress,omitempty" etf:"compress,omitempty"` // (default: false) whether this connection supports compression of packets
}

func (IdentifyPayload) OpCode() OpCode { return OpIdentify }

// PresenceUpdatePayload (opcode 3) Sent by the client to indicate a presence or status update
type PresenceUpdatePayload StatusUpdate

func (PresenceUpdatePayload) OpCode() OpCode { return OpPresenceUpdate }

// VoiceStateUpdatePayload (opcode 4) Sent when a client wants to join, move, or disconnect from a voice channel
type VoiceStateUpdatePayload struct {
	GuildID   dltype.Snowflake  `json:"guild_id"   etf:"guild_id" valid:"required"` // id of the guild
	ChannelID *dltype.Snowflake `json:"channel_id" etf:"channel_id"`                // id of the voice channel client wants to join (null if disconnecting)
	SelfMute  bool              `json:"self_mute"  etf:"self_mute"`                 // is the client muted
	SelfDeaf  bool              `json:"self_deaf"  etf:"self_deaf"`                 // is the client deafened
}

func (VoiceStateUpdatePayload) OpCode() OpCode { return OpVoiceStateUpdate }

// ResumePayload (opcode 6) Used to replay missed events when a disconnected client resumes
type ResumePayload struct {
	Token     string `json:"token"      etf:"token"      valid:"required"` // session token
	SessionID string `json:"session_id" etf:"session_id" valid:"required"` // session id
	Seq       uint64 `json:"seq"        etf:"seq"        valid:"required"` // last sequence number received
}

func (ResumePayload) OpCode() OpCode { return OpResume }

// RequestGuildMembersPayload (opcode 8) Used to request all members for a guild or a list of guilds.
// When initially connecting, if you don't have the `GUILD_PRESENCES` Gateway Intent, or if the guild is over 75k members,
// it will only send members who are in voice, plus the member for you (the connecting user).
// Otherwise, if a guild has over `large_threshold` members (value in the Gateway Identify),
// it will only send members who are online, have a role, have a nickname, or are in a voice channel,
// and if it has under `large_threshold` members, it will send all members. If a client wishes to receive additional members,
// they need to explicitly request them via this operation. The server will send GuildMembersChunk events in response
// with up to 1000 members per chunk until all members that match the request have been sent.
//
// One of (query, limit) or (user_ids) required.
type RequestGuildMembersPayload struct {
	GuildID   dltype.Snowflake `json:"guild_id"            etf:"guild_id" valid:"required"` // id of the guild to get members for
	Presences bool             `json:"presences,omitempty" etf:"presences,omitempty"`       // to specify if we want the presences of the matched members
	Nonce     string           `json:"nonce,omitempty"     etf:"nonce,omitempty"`           // nonce to identify the Guild Members Chunk response

	Query   string             `json:"query,omitempty"    etf:"query,omitempty"    valid:"required_without=UserIDs"`            // string that username starts with, or an empty string to return all members
	Limit   int                `json:"limit,omitempty"    etf:"limit,omitempty"    valid:"required_with=Query,omitempty,min=0"` // maximum number of members to send matching the query; a limit of 0 can be used with an empty string query to return all members
	UserIDs []dltype.Snowflake `json:"user_ids,omitempty" etf:"user_ids,omitempty" valid:"required_without=Query"`              // used to specify which users you wish to fetch
}

func (RequestGuildMembersPayload) OpCode() OpCode { return OpRequestGuildMembers }

// DispatchPayload has opcode 0
type DispatchPayload struct {
	Data RawPayload
	Type EventType
	Seq  uint64
}

// ReconnectPayload has opcode 7
type ReconnectPayload struct{}

// InvalidSessionPayload has opcode 9
type InvalidSessionPayload struct {
	Resumable bool // indicates whether the session may be resumable
}

func (p *InvalidSessionPayload) UnmarshalJSON(data []byte) error {
	var err error
	p.Resumable, err = strconv.ParseBool(string(data))
	return err
}

func (p *InvalidSessionPayload) UnmarshalETF(data []byte) error {
	var err error
	p.Resumable, err = strconv.ParseBool(string(data))
	return err
}

// HelloPayload has opcode 10
type HelloPayload struct {
	HeartbeatInterval time.Duration // the interval (in milliseconds) the client should heartbeat with
}

func (p *HelloPayload) UnmarshalJSON(data []byte) error {
	type aux struct {
		Interval int64 `json:"heartbeat_interval"`
	}

	var v aux
	if err := json.Unmarshal(data, &v); err != nil {
		return err
	}

	p.HeartbeatInterval = time.Duration(v.Interval) * time.Millisecond
	return nil
}

// HeartbeatACKPayload (opcode 11) received by client after Heartbeat
type HeartbeatACKPayload struct{}

func (HeartbeatACKPayload) OpCode() OpCode { return OpHeartbeatACK }
