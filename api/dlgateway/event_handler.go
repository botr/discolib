// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dlgateway

import (
	"context"
	"time"

	"pkg.botr.me/discolib/api/dlresource"
	"pkg.botr.me/discolib/api/dltype"
)

// EventDispatcher interface ensures that EventHandler can forward requests and get a meaningful response
// even if receiver didn't implement some of the methods
type EventDispatcher interface {
	// Sent when a client has completed the initial handshake with the gateway (for new sessions).
	// The ready event can be the largest and most complex event the gateway will send, as it contains all the state required for a client to begin interacting with the rest of the platform.
	Ready(context.Context, ReadyEvent)

	// Sent when a client has sent a resume payload to the gateway (for resuming existing sessions).
	Resumed(context.Context, ResumedEvent)

	// Sent when a new guild channel is created, relevant to the current user. The inner payload is a channel object.
	ChannelCreate(context.Context, ChannelCreateEvent)

	// Sent when a channel is updated. The inner payload is a channel object. This is not sent when the field last_message_id is altered.
	// To keep track of the last_message_id changes, you must listen for Message Create events.
	ChannelUpdate(context.Context, ChannelUpdateEvent)

	// Sent when a channel relevant to the current user is deleted. The inner payload is a channel object.
	ChannelDelete(context.Context, ChannelDeleteEvent)

	// Sent when a message is pinned or unpinned in a text channel. This is not sent when a pinned message is deleted.
	ChannelPinsUpdate(context.Context, ChannelPinsUpdateEvent)

	// Sent when a thread is created, relevant to the current user, or when the current user is added to a thread.
	// The inner payload is a Channel object. When being added to an existing private thread, includes a ThreadMember object.
	ThreadCreate(context.Context, ThreadCreateEvent)

	// Sent when a thread is updated. The inner payload is a Channel object. This is not sent when the field `last_message_id` is altered.
	// To keep track of the last_message_id changes, you must listen for MessageCreate events.
	ThreadUpdate(context.Context, ThreadUpdateEvent)

	// Sent when a thread relevant to the current user is deleted. The inner payload is a subset of the Channel object,
	// containing just the `id`, `guild_id`, `parent_id`, and `type` fields.
	ThreadDelete(context.Context, ThreadDeleteEvent)

	// Sent when the current user gains access to a channel.
	ThreadListSync(context.Context, ThreadListSyncEvent)

	// Sent when the ThreadMember object for the current user is updated. The inner payload is a ThreadMember object.
	ThreadMemberUpdate(context.Context, ThreadMemberUpdateEvent)

	// Sent when anyone is added to or removed from a thread.
	// If the current user does not have the `GUILD_MEMBERS` intent, then this event will only be sent if the current user was added to or removed from the thread.
	ThreadMembersUpdate(context.Context, ThreadMembersUpdateEvent)

	// Sent in three different scenarios:
	//  - When a user is initially connecting, to lazily load and backfill information for all unavailable guilds sent in the Ready event. Guilds that are unavailable due to an outage will send a Guild Delete event.
	//  - When a Guild becomes available again to the client.
	//  - When the current user joins a new Guild.
	// The inner payload is a guild object, with all the extra fields specified.
	GuildCreate(context.Context, GuildCreateEvent)

	// Sent when a guild is updated. The inner payload is a guild object.
	GuildUpdate(context.Context, GuildUpdateEvent)

	// Sent when a guild becomes or was already unavailable due to an outage, or when the user leaves or is removed from a guild.
	// The inner payload is an unavailable guild object. If the unavailable field is not set, the user was removed from the guild.
	GuildDelete(context.Context, GuildDeleteEvent)

	// Sent when a user is banned from a guild.
	GuildBanAdd(context.Context, GuildBanAddEvent)

	// Sent when a user is unbanned from a guild.
	GuildBanRemove(context.Context, GuildBanRemoveEvent)

	// Sent when a guild's emojis have been updated.
	GuildEmojisUpdate(context.Context, GuildEmojisUpdateEvent)

	// Sent when a guild integration is updated.
	GuildIntegrationsUpdate(context.Context, GuildIntegrationsUpdateEvent)

	// Sent when a new user joins a guild. The inner payload is a guild member object with an extra guild_id key.
	GuildMemberAdd(context.Context, GuildMemberAddEvent)

	// Sent when a user is removed from a guild (leave/kick/ban).
	GuildMemberRemove(context.Context, GuildMemberRemoveEvent)

	// Sent when a guild member is updated. This will also fire when the user object of a guild member changes.
	GuildMemberUpdate(context.Context, GuildMemberUpdateEvent)

	// Sent in response to Guild Request Members. You can use the chunk_index and chunk_count to calculate how many chunks are left for your request.
	GuildMembersChunk(context.Context, GuildMembersChunkEvent)

	// Sent when a guild role is created.
	GuildRoleCreate(context.Context, GuildRoleCreateEvent)

	// Sent when a guild role is updated.
	GuildRoleUpdate(context.Context, GuildRoleUpdateEvent)

	// Sent when a guild role is deleted.
	GuildRoleDelete(context.Context, GuildRoleDeleteEvent)

	// Sent when a new invite to a channel is created.
	InviteCreate(context.Context, InviteCreateEvent)

	// Sent when an invite is deleted.
	InviteDelete(context.Context, InviteDeleteEvent)

	// Sent when a message is created. The inner payload is a message object.
	MessageCreate(context.Context, MessageCreateEvent)

	// Sent when a message is updated. The inner payload is a message object.
	// Unlike creates, message updates may contain only a subset of the full message object payload (but will always contain an id and channel_id).
	MessageUpdate(context.Context, MessageUpdateEvent)

	// Sent when a message is deleted.
	MessageDelete(context.Context, MessageDeleteEvent)

	// Sent when multiple messages are deleted at once.
	MessageDeleteBulk(context.Context, MessageDeleteBulkEvent)

	// Sent when a user adds a reaction to a message.
	MessageReactionAdd(context.Context, MessageReactionAddEvent)

	// Sent when a user removes a reaction from a message.
	MessageReactionRemove(context.Context, MessageReactionRemoveEvent)

	// Sent when a user explicitly removes all reactions from a message.
	MessageReactionRemoveAll(context.Context, MessageReactionRemoveAllEvent)

	// Sent when a bot removes all instances of a given emoji from the reactions of a message.
	MessageReactionRemoveEmoji(context.Context, MessageReactionRemoveEmojiEvent)

	// A user's presence is their current state on a guild. This event is sent when a user's presence or info, such as name or avatar, is updated.
	// The user object within this event can be partial, the only field which must be sent is the id field, everything else is optional.
	// Along with this limitation, no fields are required, and the types of the fields are not validated.
	// Your client should expect any combination of fields and types within this event.
	PresenceUpdate(context.Context, PresenceUpdateEvent)

	// Sent when a user starts typing in a channel.
	TypingStart(context.Context, TypingStartEvent)

	// Sent when properties about the user change. Inner payload is a user object.
	UserUpdate(context.Context, UserUpdateEvent)

	// Sent when someone joins/leaves/moves voice channels. Inner payload is a voice state object.
	VoiceStateUpdate(context.Context, VoiceStateUpdateEvent)

	// Sent when a guild's voice server is updated.
	// This is sent when initially connecting to voice, and when the current voice instance fails over to a new server.
	VoiceServerUpdate(context.Context, VoiceServerUpdateEvent)

	// Sent when a guild channel's webhook is created, updated, or deleted.
	WebhooksUpdate(context.Context, WebhooksUpdateEvent)

	// Sent when a user in a guild uses a Slash Command. Inner payload is an Interaction.
	InteractionCreate(context.Context, InteractionCreateEvent)

	mustEmbedEventDispatcher()
}

// EventHandler is a wrapper for dispatching gateway events based on their opcode
type EventHandler struct {
	dispatcher EventDispatcher
	decoder    Unmarshaler
}

func NewEventHandler(ed EventDispatcher, decoder Unmarshaler) EventHandler {
	return EventHandler{ed, decoder}
}

// Handle only returns error if incoming payload contains malformed json data
func (h *EventHandler) Handle(ctx context.Context, typ EventType, data []byte) error {
	var err error

	switch typ {
	case EventReady:
		var evt ReadyEvent
		if err = h.decoder.Unmarshal(data, &evt); err == nil {
			h.dispatcher.Ready(ctx, evt)
		}
	case EventResumed:
		var evt ResumedEvent
		if err = h.decoder.Unmarshal(data, &evt); err == nil {
			h.dispatcher.Resumed(ctx, evt)
		}
	case EventChannelCreate:
		var evt ChannelCreateEvent
		if err = h.decoder.Unmarshal(data, &evt); err == nil {
			h.dispatcher.ChannelCreate(ctx, evt)
		}
	case EventChannelUpdate:
		var evt ChannelUpdateEvent
		if err = h.decoder.Unmarshal(data, &evt); err == nil {
			h.dispatcher.ChannelUpdate(ctx, evt)
		}
	case EventChannelDelete:
		var evt ChannelDeleteEvent
		if err = h.decoder.Unmarshal(data, &evt); err == nil {
			h.dispatcher.ChannelDelete(ctx, evt)
		}
	case EventChannelPinsUpdate:
		var evt ChannelPinsUpdateEvent
		if err = h.decoder.Unmarshal(data, &evt); err == nil {
			h.dispatcher.ChannelPinsUpdate(ctx, evt)
		}
	case EventGuildCreate:
		var evt GuildCreateEvent
		if err = h.decoder.Unmarshal(data, &evt); err == nil {
			h.dispatcher.GuildCreate(ctx, evt)
		}
	case EventGuildUpdate:
		var evt GuildUpdateEvent
		if err = h.decoder.Unmarshal(data, &evt); err == nil {
			h.dispatcher.GuildUpdate(ctx, evt)
		}
	case EventGuildDelete:
		var evt GuildDeleteEvent
		if err = h.decoder.Unmarshal(data, &evt); err == nil {
			h.dispatcher.GuildDelete(ctx, evt)
		}
	case EventGuildBanAdd:
		var evt GuildBanAddEvent
		if err = h.decoder.Unmarshal(data, &evt); err == nil {
			h.dispatcher.GuildBanAdd(ctx, evt)
		}
	case EventGuildBanRemove:
		var evt GuildBanRemoveEvent
		if err = h.decoder.Unmarshal(data, &evt); err == nil {
			h.dispatcher.GuildBanRemove(ctx, evt)
		}
	case EventGuildEmojisUpdate:
		var evt GuildEmojisUpdateEvent
		if err = h.decoder.Unmarshal(data, &evt); err == nil {
			h.dispatcher.GuildEmojisUpdate(ctx, evt)
		}
	case EventGuildIntegrationsUpdate:
		var evt GuildIntegrationsUpdateEvent
		if err = h.decoder.Unmarshal(data, &evt); err == nil {
			h.dispatcher.GuildIntegrationsUpdate(ctx, evt)
		}
	case EventGuildMemberAdd:
		var evt GuildMemberAddEvent
		if err = h.decoder.Unmarshal(data, &evt); err == nil {
			h.dispatcher.GuildMemberAdd(ctx, evt)
		}
	case EventGuildMemberRemove:
		var evt GuildMemberRemoveEvent
		if err = h.decoder.Unmarshal(data, &evt); err == nil {
			h.dispatcher.GuildMemberRemove(ctx, evt)
		}
	case EventGuildMemberUpdate:
		var evt GuildMemberUpdateEvent
		if err = h.decoder.Unmarshal(data, &evt); err == nil {
			h.dispatcher.GuildMemberUpdate(ctx, evt)
		}
	case EventGuildMembersChunk:
		var evt GuildMembersChunkEvent
		if err = h.decoder.Unmarshal(data, &evt); err == nil {
			h.dispatcher.GuildMembersChunk(ctx, evt)
		}
	case EventGuildRoleCreate:
		var evt GuildRoleCreateEvent
		if err = h.decoder.Unmarshal(data, &evt); err == nil {
			h.dispatcher.GuildRoleCreate(ctx, evt)
		}
	case EventGuildRoleUpdate:
		var evt GuildRoleUpdateEvent
		if err = h.decoder.Unmarshal(data, &evt); err == nil {
			h.dispatcher.GuildRoleUpdate(ctx, evt)
		}
	case EventGuildRoleDelete:
		var evt GuildRoleDeleteEvent
		if err = h.decoder.Unmarshal(data, &evt); err == nil {
			h.dispatcher.GuildRoleDelete(ctx, evt)
		}
	case EventInviteCreate:
		var evt InviteCreateEvent
		if err = h.decoder.Unmarshal(data, &evt); err == nil {
			h.dispatcher.InviteCreate(ctx, evt)
		}
	case EventInviteDelete:
		var evt InviteDeleteEvent
		if err = h.decoder.Unmarshal(data, &evt); err == nil {
			h.dispatcher.InviteDelete(ctx, evt)
		}
	case EventMessageCreate:
		var evt MessageCreateEvent
		if err = h.decoder.Unmarshal(data, &evt); err == nil {
			h.dispatcher.MessageCreate(ctx, evt)
		}
	case EventMessageUpdate:
		var evt MessageUpdateEvent
		if err = h.decoder.Unmarshal(data, &evt); err == nil {
			h.dispatcher.MessageUpdate(ctx, evt)
		}
	case EventMessageDelete:
		var evt MessageDeleteEvent
		if err = h.decoder.Unmarshal(data, &evt); err == nil {
			h.dispatcher.MessageDelete(ctx, evt)
		}
	case EventMessageDeleteBulk:
		var evt MessageDeleteBulkEvent
		if err = h.decoder.Unmarshal(data, &evt); err == nil {
			h.dispatcher.MessageDeleteBulk(ctx, evt)
		}
	case EventMessageReactionAdd:
		var evt MessageReactionAddEvent
		if err = h.decoder.Unmarshal(data, &evt); err == nil {
			h.dispatcher.MessageReactionAdd(ctx, evt)
		}
	case EventMessageReactionRemove:
		var evt MessageReactionRemoveEvent
		if err = h.decoder.Unmarshal(data, &evt); err == nil {
			h.dispatcher.MessageReactionRemove(ctx, evt)
		}
	case EventMessageReactionRemoveAll:
		var evt MessageReactionRemoveAllEvent
		if err = h.decoder.Unmarshal(data, &evt); err == nil {
			h.dispatcher.MessageReactionRemoveAll(ctx, evt)
		}
	case EventMessageReactionRemoveEmoji:
		var evt MessageReactionRemoveEmojiEvent
		if err = h.decoder.Unmarshal(data, &evt); err == nil {
			h.dispatcher.MessageReactionRemoveEmoji(ctx, evt)
		}
	case EventPresenceUpdate:
		var evt PresenceUpdateEvent
		if err = h.decoder.Unmarshal(data, &evt); err == nil {
			h.dispatcher.PresenceUpdate(ctx, evt)
		}
	case EventTypingStart:
		var evt TypingStartEvent
		if err = h.decoder.Unmarshal(data, &evt); err == nil {
			h.dispatcher.TypingStart(ctx, evt)
		}
	case EventUserUpdate:
		var evt UserUpdateEvent
		if err = h.decoder.Unmarshal(data, &evt); err == nil {
			h.dispatcher.UserUpdate(ctx, evt)
		}
	case EventVoiceStateUpdate:
		var evt VoiceStateUpdateEvent
		if err = h.decoder.Unmarshal(data, &evt); err == nil {
			h.dispatcher.VoiceStateUpdate(ctx, evt)
		}
	case EventVoiceServerUpdate:
		var evt VoiceServerUpdateEvent
		if err = h.decoder.Unmarshal(data, &evt); err == nil {
			h.dispatcher.VoiceServerUpdate(ctx, evt)
		}
	case EventWebhooksUpdate:
		var evt WebhooksUpdateEvent
		if err = h.decoder.Unmarshal(data, &evt); err == nil {
			h.dispatcher.WebhooksUpdate(ctx, evt)
		}
	case EventInteractionCreate:
		var evt InteractionCreateEvent
		if err = h.decoder.Unmarshal(data, &evt); err == nil {
			h.dispatcher.InteractionCreate(ctx, evt)
		}
	}

	return err
}

// ReadyEvent is dispatched when a client has completed the initial handshake with the gateway (for new sessions).
// The ready event can be the largest and most complex event the gateway will send,
// as it contains all the state required for a client to begin interacting with the rest of the platform.
type ReadyEvent struct {
	Version          int                           `json:"v"                  etf:"v"`                  // gateway version
	User             dlresource.User               `json:"user"               etf:"user"`               // information about the user including email
	Guilds           []dlresource.UnavailableGuild `json:"guilds"             etf:"guilds"`             // the guilds the user is in
	SessionID        string                        `json:"session_id"         etf:"session_id"`         // used for resuming connections
	ResumeGatewayURL string                        `json:"resume_gateway_url" etf:"resume_gateway_url"` // Gateway URL for resuming connections
	Shard            []int                         `json:"shard,omitempty"    etf:"shard,omitempty"`    // [shard_id, num_shards] the shard information associated with this session, if sent when identifying
	Application      dlresource.PartialApplication `json:"application"        etf:"application"`        // contains id and flags

	SessionType          string   `json:"session_type"            etf:"session_type"`            // NOTE: undocumented
	GeoOrderedRTCRegions []string `json:"geo_ordered_rtc_regions" etf:"geo_ordered_rtc_regions"` // NOTE: undocumented
	// Presences            json.RawMessage `json:"presences"               etf:"presences"`               // NOTE: undocumented
	// PrivateChannels      json.RawMessage `json:"private_channels"        etf:"private_channels"`        // NOTE: undocumented
	// Relationships        json.RawMessage `json:"relationships"           etf:"relationships"`           // NOTE: undocumented
	// GuildJoinRequests    json.RawMessage `json:"guild_join_requests"     etf:"guild_join_requests"`     // NOTE: undocumented
	// UserSettings         json.RawMessage `json:"user_settings"           etf:"user_settings"`           // NOTE: undocumented
}

type ResumedEvent struct{}

type ChannelPinsUpdateEvent struct {
	GuildID          dltype.Snowflake `json:"guild_id,omitempty"           etf:"guild_id,omitempty"`           // the id of the guild
	ChannelID        dltype.Snowflake `json:"channel_id"                   etf:"channel_id"`                   // the id of the channel
	LastPinTimestamp time.Time        `json:"last_pin_timestamp,omitempty" etf:"last_pin_timestamp,omitempty"` // the time at which the most recent pinned message was pinned
}

type ThreadListSyncEvent struct {
	GuildID    dltype.Snowflake          `json:"guild_id"              etf:"guild_id"`              // the id of the guild
	ChannelIDs []dltype.Snowflake        `json:"channel_ids,omitempty" etf:"channel_ids,omitempty"` // the parent channel ids whose threads are being synced. If omitted, then threads were synced for the entire guild
	Threads    []dlresource.Channel      `json:"threads"               etf:"threads"`               // all active threads in the given channels that the current user can access
	Members    []dlresource.ThreadMember `json:"members"               etf:"members"`               // all thread member objects from the synced threads for the current user, indicating which threads the current user has been added to
}

type ThreadMembersUpdateEvent struct {
	ID               dltype.Snowflake          `json:"id"                           etf:"id"`                           // the id of the thread
	GuildID          dltype.Snowflake          `json:"guild_id"                     etf:"guild_id"`                     // the id of the guild
	MemberCount      int                       `json:"member_count"                 etf:"member_count"`                 // the approximate number of members in the thread, capped at 50
	AddedMembers     []dlresource.ThreadMember `json:"added_members,omitempty"      etf:"added_members,omitempty"`      // the users who were added to the thread
	RemovedMemberIds []dltype.Snowflake        `json:"removed_member_ids,omitempty" etf:"removed_member_ids,omitempty"` // the id of the users who were removed from the thread
}

type GuildEmojisUpdateEvent struct {
	GuildID dltype.Snowflake   `json:"guild_id" etf:"guild_id"`
	Emojis  []dlresource.Emoji `json:"emojis"   etf:"emojis"`
}

type GuildIntegrationsUpdateEvent struct {
	GuildID dltype.Snowflake `json:"guild_id" etf:"guild_id"`
}

type GuildMemberAddEvent struct {
	dlresource.GuildMember

	GuildID dltype.Snowflake `json:"guild_id" etf:"guild_id"`
}

type GuildMemberUpdateEvent struct {
	User         dlresource.User    `json:"user,omitempty"          etf:"user,omitempty"`          // the user this guild member represents
	Nick         string             `json:"nick,omitempty"          etf:"nick,omitempty"`          // this users guild nickname
	Roles        []dltype.Snowflake `json:"roles"                   etf:"roles"`                   // array of role object ids
	JoinedAt     time.Time          `json:"joined_at"               etf:"joined_at"`               // when the user joined the guild
	PremiumSince time.Time          `json:"premium_since,omitempty" etf:"premium_since,omitempty"` // when the user started boosting the guild
	Pending      *bool              `json:"pending,omitempty"       etf:"pending,omitempty"`       // whether the user has not yet passed the guild's Membership Screening requirements

	GuildID dltype.Snowflake `json:"guild_id" etf:"guild_id"`
}

type GuildMembersChunkEvent struct {
	GuildID    dltype.Snowflake         `json:"guild_id"            etf:"guild_id"`            // the id of the guild
	Members    []dlresource.GuildMember `json:"members"             etf:"members"`             // set of guild members
	ChunkIndex int                      `json:"chunk_index"         etf:"chunk_index"`         // the chunk index in the expected chunks for this response (0 <= chunk_index < chunk_count)
	ChunkCount int                      `json:"chunk_count"         etf:"chunk_count"`         // the total number of expected chunks for this response
	NotFound   []dltype.Snowflake       `json:"not_found,omitempty" etf:"not_found,omitempty"` // if passing an invalid id to REQUEST_GUILD_MEMBERS, it will be returned here
	Presences  []dlresource.Presence    `json:"presences,omitempty" etf:"presences,omitempty"` // if passing true to REQUEST_GUILD_MEMBERS, presences of the returned members will be here
	Nonce      string                   `json:"nonce,omitempty"     etf:"nonce,omitempty"`     // the nonce used in the Guild Members Request
}

type GuildRoleCreateEvent struct {
	GuildID dltype.Snowflake `json:"guild_id" etf:"guild_id"` // the id of the guild
	Role    dlresource.Role  `json:"role"     etf:"role"`     // the role created
}

type GuildRoleDeleteEvent struct {
	GuildID dltype.Snowflake `json:"guild_id" etf:"guild_id"`
	RoleID  dltype.Snowflake `json:"role_id"  etf:"role_id"`
}

type InviteCreateEvent struct {
	ChannelID      dltype.Snowflake          `json:"channel_id"                 etf:"channel_id"`
	Code           string                    `json:"code"                       etf:"code"`
	CreatedAt      dltype.UnixSeconds        `json:"created_at"                 etf:"created_at"`
	GuildID        dltype.Snowflake          `json:"guild_id,omitempty"         etf:"guild_id,omitempty"`
	Inviter        *dlresource.User          `json:"inviter,omitempty"          etf:"inviter,omitempty"`
	MaxAgeSeconds  int                       `json:"max_age"                    etf:"max_age"`  // how long the invite is valid for (in seconds)
	MaxUses        int                       `json:"max_uses"                   etf:"max_uses"` // the maximum number of times the invite can be used
	TargetUser     *dlresource.User          `json:"target_user,omitempty"      etf:"target_user,omitempty"`
	TargetUserType dlresource.TargetUserType `json:"target_user_type,omitempty" etf:"target_user_type,omitempty"`
	Temporary      bool                      `json:"temporary"                  etf:"temporary"` // whether or not the invite is temporary (invited users will be kicked on disconnect unless they're assigned a role)
	Uses           int                       `json:"uses"                       etf:"uses"`      // how many times the invite has been used (always will be 0) // TODO: why use if it always 0?
}

type InviteDeleteEvent struct {
	ChannelID dltype.Snowflake `json:"channel_id"         etf:"channel_id"`         // the channel of the invite
	GuildID   dltype.Snowflake `json:"guild_id,omitempty" etf:"guild_id,omitempty"` // the guild of the invite
	Code      string           `json:"code"               etf:"code"`               // the unique invite code
}

type MessageDeleteEvent struct {
	ID        dltype.Snowflake `json:"id"                 etf:"id"`
	ChannelID dltype.Snowflake `json:"channel_id"         etf:"channel_id"`
	GuildID   dltype.Snowflake `json:"guild_id,omitempty" etf:"guild_id,omitempty"`
}

type MessageDeleteBulkEvent struct {
	IDs       []dltype.Snowflake `json:"ids"                etf:"ids"`
	ChannelID dltype.Snowflake   `json:"channel_id"         etf:"channel_id"`
	GuildID   dltype.Snowflake   `json:"guild_id,omitempty" etf:"guild_id,omitempty"`
}

type MessageReactionAddEvent struct {
	UserID    dltype.Snowflake        `json:"user_id"            etf:"user_id"`
	ChannelID dltype.Snowflake        `json:"channel_id"         etf:"channel_id"`
	MessageID dltype.Snowflake        `json:"message_id"         etf:"message_id"`
	GuildID   dltype.Snowflake        `json:"guild_id,omitempty" etf:"guild_id,omitempty"`
	Member    *dlresource.GuildMember `json:"member,omitempty"   etf:"member,omitempty"` // the member who reacted if this happened in a guild
	Emoji     dlresource.Emoji        `json:"emoji"              etf:"emoji"`            // partial emoji object (only contains id, name, animated)
}

type MessageReactionRemoveEvent struct {
	UserID    dltype.Snowflake `json:"user_id"            etf:"user_id"`
	ChannelID dltype.Snowflake `json:"channel_id"         etf:"channel_id"`
	MessageID dltype.Snowflake `json:"message_id"         etf:"message_id"`
	GuildID   dltype.Snowflake `json:"guild_id,omitempty" etf:"guild_id,omitempty"`
	Emoji     dlresource.Emoji `json:"emoji"              etf:"emoji"` // partial emoji object (only contains id, name, animated)
}

type MessageReactionRemoveAllEvent struct {
	ChannelID dltype.Snowflake `json:"channel_id"         etf:"channel_id"`
	MessageID dltype.Snowflake `json:"message_id"         etf:"message_id"`
	GuildID   dltype.Snowflake `json:"guild_id,omitempty" etf:"guild_id,omitempty"`
}

type MessageReactionRemoveEmojiEvent struct {
	ChannelID dltype.Snowflake `json:"channel_id"         etf:"channel_id"`
	MessageID dltype.Snowflake `json:"message_id"         etf:"message_id"`
	GuildID   dltype.Snowflake `json:"guild_id,omitempty" etf:"guild_id,omitempty"`
	Emoji     dlresource.Emoji `json:"emoji"              etf:"emoji"` // partial emoji object (only contains id, name, animated)
}

type TypingStartEvent struct {
	ChannelID dltype.Snowflake        `json:"channel_id"         etf:"channel_id"`         // id of the channel
	GuildID   dltype.Snowflake        `json:"guild_id,omitempty" etf:"guild_id,omitempty"` // id of the guild
	UserID    dltype.Snowflake        `json:"user_id"            etf:"user_id"`            // id of the user
	Timestamp dltype.UnixSeconds      `json:"timestamp"          etf:"timestamp"`          // unix time (in seconds) of when the user started typing
	Member    *dlresource.GuildMember `json:"member,omitempty"   etf:"member,omitempty"`   // the member who started typing if this happened in a guild
}

type VoiceServerUpdateEvent struct {
	Token    string           `json:"token"    etf:"token"`    // voice connection token
	GuildID  dltype.Snowflake `json:"guild_id" etf:"guild_id"` // the guild this voice server update is for
	Endpoint string           `json:"endpoint" etf:"endpoint"` // the voice server host
}

type WebhooksUpdateEvent struct {
	GuildID   dltype.Snowflake `json:"guild_id,omitempty" etf:"guild_id,omitempty"` // id of the guild
	ChannelID dltype.Snowflake `json:"channel_id"         etf:"channel_id"`         // id of the channel
}

type ChannelCreateEvent = dlresource.Channel
type ChannelUpdateEvent = dlresource.Channel
type ChannelDeleteEvent = dlresource.Channel
type ThreadCreateEvent = dlresource.Channel
type ThreadUpdateEvent = dlresource.Channel
type ThreadDeleteEvent = dlresource.Channel
type ThreadMemberUpdateEvent = dlresource.ThreadMember
type GuildCreateEvent = dlresource.Guild
type GuildUpdateEvent = dlresource.Guild
type GuildDeleteEvent = dlresource.UnavailableGuild
type GuildBanAddEvent = guildUserEvent
type GuildBanRemoveEvent = guildUserEvent
type GuildMemberRemoveEvent = guildUserEvent
type GuildRoleUpdateEvent = GuildRoleCreateEvent
type MessageCreateEvent = dlresource.Message
type MessageUpdateEvent = dlresource.Message
type PresenceUpdateEvent = dlresource.Presence
type UserUpdateEvent = dlresource.User
type VoiceStateUpdateEvent = dlresource.VoiceState
type InteractionCreateEvent = dlresource.Interaction

type guildUserEvent struct {
	GuildID dltype.Snowflake `json:"guild_id" etf:"guild_id"`
	User    dlresource.User  `json:"user"     etf:"user"`
}

// UnimplementedEventDispatcher should be inherited by dispatcher handlers
type UnimplementedEventDispatcher struct{}

func (*UnimplementedEventDispatcher) mustEmbedEventDispatcher() {}

func (*UnimplementedEventDispatcher) Ready(context.Context, ReadyEvent)                             {}
func (*UnimplementedEventDispatcher) Resumed(context.Context, ResumedEvent)                         {}
func (*UnimplementedEventDispatcher) ChannelCreate(context.Context, ChannelCreateEvent)             {}
func (*UnimplementedEventDispatcher) ChannelUpdate(context.Context, ChannelUpdateEvent)             {}
func (*UnimplementedEventDispatcher) ChannelDelete(context.Context, ChannelDeleteEvent)             {}
func (*UnimplementedEventDispatcher) ChannelPinsUpdate(context.Context, ChannelPinsUpdateEvent)     {}
func (*UnimplementedEventDispatcher) ThreadCreate(context.Context, ThreadCreateEvent)               {}
func (*UnimplementedEventDispatcher) ThreadUpdate(context.Context, ThreadUpdateEvent)               {}
func (*UnimplementedEventDispatcher) ThreadDelete(context.Context, ThreadDeleteEvent)               {}
func (*UnimplementedEventDispatcher) ThreadListSync(context.Context, ThreadListSyncEvent)           {}
func (*UnimplementedEventDispatcher) ThreadMemberUpdate(context.Context, ThreadMemberUpdateEvent)   {}
func (*UnimplementedEventDispatcher) ThreadMembersUpdate(context.Context, ThreadMembersUpdateEvent) {}
func (*UnimplementedEventDispatcher) GuildCreate(context.Context, GuildCreateEvent)                 {}
func (*UnimplementedEventDispatcher) GuildUpdate(context.Context, GuildUpdateEvent)                 {}
func (*UnimplementedEventDispatcher) GuildDelete(context.Context, GuildDeleteEvent)                 {}
func (*UnimplementedEventDispatcher) GuildBanAdd(context.Context, GuildBanAddEvent)                 {}
func (*UnimplementedEventDispatcher) GuildBanRemove(context.Context, GuildBanRemoveEvent)           {}
func (*UnimplementedEventDispatcher) GuildEmojisUpdate(context.Context, GuildEmojisUpdateEvent)     {}

func (*UnimplementedEventDispatcher) GuildIntegrationsUpdate(context.Context, GuildIntegrationsUpdateEvent) {
}

func (*UnimplementedEventDispatcher) GuildMemberAdd(context.Context, GuildMemberAddEvent)         {}
func (*UnimplementedEventDispatcher) GuildMemberRemove(context.Context, GuildMemberRemoveEvent)   {}
func (*UnimplementedEventDispatcher) GuildMemberUpdate(context.Context, GuildMemberUpdateEvent)   {}
func (*UnimplementedEventDispatcher) GuildMembersChunk(context.Context, GuildMembersChunkEvent)   {}
func (*UnimplementedEventDispatcher) GuildRoleCreate(context.Context, GuildRoleCreateEvent)       {}
func (*UnimplementedEventDispatcher) GuildRoleUpdate(context.Context, GuildRoleUpdateEvent)       {}
func (*UnimplementedEventDispatcher) GuildRoleDelete(context.Context, GuildRoleDeleteEvent)       {}
func (*UnimplementedEventDispatcher) InviteCreate(context.Context, InviteCreateEvent)             {}
func (*UnimplementedEventDispatcher) InviteDelete(context.Context, InviteDeleteEvent)             {}
func (*UnimplementedEventDispatcher) MessageCreate(context.Context, MessageCreateEvent)           {}
func (*UnimplementedEventDispatcher) MessageUpdate(context.Context, MessageUpdateEvent)           {}
func (*UnimplementedEventDispatcher) MessageDelete(context.Context, MessageDeleteEvent)           {}
func (*UnimplementedEventDispatcher) MessageDeleteBulk(context.Context, MessageDeleteBulkEvent)   {}
func (*UnimplementedEventDispatcher) MessageReactionAdd(context.Context, MessageReactionAddEvent) {}

func (*UnimplementedEventDispatcher) MessageReactionRemove(context.Context, MessageReactionRemoveEvent) {
}

func (*UnimplementedEventDispatcher) MessageReactionRemoveAll(context.Context, MessageReactionRemoveAllEvent) {
}

func (*UnimplementedEventDispatcher) MessageReactionRemoveEmoji(context.Context, MessageReactionRemoveEmojiEvent) {
}

func (*UnimplementedEventDispatcher) PresenceUpdate(context.Context, PresenceUpdateEvent)       {}
func (*UnimplementedEventDispatcher) TypingStart(context.Context, TypingStartEvent)             {}
func (*UnimplementedEventDispatcher) UserUpdate(context.Context, UserUpdateEvent)               {}
func (*UnimplementedEventDispatcher) VoiceStateUpdate(context.Context, VoiceStateUpdateEvent)   {}
func (*UnimplementedEventDispatcher) VoiceServerUpdate(context.Context, VoiceServerUpdateEvent) {}
func (*UnimplementedEventDispatcher) WebhooksUpdate(context.Context, WebhooksUpdateEvent)       {}
func (*UnimplementedEventDispatcher) InteractionCreate(context.Context, InteractionCreateEvent) {}

// compile-type interface checking
var _ EventDispatcher = (*UnimplementedEventDispatcher)(nil)
