// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dlgateway

import (
	"context"

	"pkg.botr.me/discolib/internal/errstr"
)

const ErrNotImplemented errstr.Error = "method not implemented for type"

// PayloadDispatcher interface ensures that PayloadHandler can forward requests and get a meaningful response
// even if receiver didn't implement some of the methods
type PayloadDispatcher interface {
	// Sent when gateway dispatches an event
	Dispatch(context.Context, DispatchPayload)

	// Used to maintain an active gateway connection. Must be sent every heartbeat_interval milliseconds after the Opcode 10 Hello payload is received. The inner d key is the last sequence number—s—received by the client. If you have not yet received one, send null.
	Heartbeat(context.Context, HeartbeatPayload)

	// Sent to indicate one of at least three different situations:
	//  - The gateway could not initialize a session after receiving an CmdIdentify
	//  - The gateway could not resume a previous session after receiving an CmdResume
	//  - The gateway has invalidated an active session and is requesting client action
	InvalidSession(context.Context, InvalidSessionPayload)

	// Sent on connection to the websocket. Defines the heartbeat interval that the client should heartbeat to.
	Hello(context.Context, HelloPayload)

	// Dispatched when a client should reconnect to the gateway (and resume their existing session, if they have one).
	// This event usually occurs during deploys to migrate sessions gracefully off old hosts.
	Reconnect(context.Context, ReconnectPayload)

	// Sent as a response to outgoing Heartbeat. Can be used to detect zombied or failed connections.
	HeartbeatACK(context.Context, HeartbeatACKPayload)

	mustEmbedPayloadDispatcher()
}

// PayloadHandler is a wrapper for dispatching gateway payloads based on their opcode
type PayloadHandler struct {
	dispatcher PayloadDispatcher
	decoder    Unmarshaler
}

func NewPayloadHandler(ed PayloadDispatcher, decoder Unmarshaler) PayloadHandler {
	return PayloadHandler{ed, decoder}
}

// Handle returns an error only if incoming payload contains invalid json data
func (h *PayloadHandler) Handle(ctx context.Context, payload Payload) error {
	var err error

	switch payload.OpCode {
	case OpDispatch:
		in := DispatchPayload{payload.Data, payload.Type, payload.Seq}
		h.dispatcher.Dispatch(ctx, in)
	case OpHeartbeat:
		var in HeartbeatPayload
		if err = h.decoder.Unmarshal(payload.Data, &in); err == nil {
			h.dispatcher.Heartbeat(ctx, in)
		}
	case OpInvalidSession:
		var in InvalidSessionPayload
		if err = h.decoder.Unmarshal(payload.Data, &in); err == nil {
			h.dispatcher.InvalidSession(ctx, in)
		}
	case OpHello:
		var in HelloPayload
		if err = h.decoder.Unmarshal(payload.Data, &in); err == nil {
			h.dispatcher.Hello(ctx, in)
		}
	case OpReconnect:
		var in ReconnectPayload
		if err = h.decoder.Unmarshal(payload.Data, &in); err == nil {
			h.dispatcher.Reconnect(ctx, in)
		}
	case OpHeartbeatACK:
		var in HeartbeatACKPayload
		if err = h.decoder.Unmarshal(payload.Data, &in); err == nil {
			h.dispatcher.HeartbeatACK(ctx, in)
		}
	}

	return err
}

// UnimplementedPayloadDispatcher should be inherited by dispatcher handlers
type UnimplementedPayloadDispatcher struct{}

func (*UnimplementedPayloadDispatcher) Dispatch(context.Context, DispatchPayload)             {}
func (*UnimplementedPayloadDispatcher) Heartbeat(context.Context, HeartbeatPayload)           {}
func (*UnimplementedPayloadDispatcher) InvalidSession(context.Context, InvalidSessionPayload) {}
func (*UnimplementedPayloadDispatcher) Hello(context.Context, HelloPayload)                   {}
func (*UnimplementedPayloadDispatcher) Reconnect(context.Context, ReconnectPayload)           {}
func (*UnimplementedPayloadDispatcher) HeartbeatACK(context.Context, HeartbeatACKPayload)     {}
func (*UnimplementedPayloadDispatcher) mustEmbedPayloadDispatcher()                           {}

// compile-type interface checking
var _ PayloadDispatcher = (*UnimplementedPayloadDispatcher)(nil)
