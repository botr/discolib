// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dltype

import (
	"strconv"
	"time"
)

// UnixMillis is compatibility wrapper for representing Discord timestamps as time.Time
type UnixMillis struct {
	time.Time
}

func (t *UnixMillis) UnmarshalJSON(b []byte) error {
	n, err := strconv.ParseInt(string(b), 10, 64)
	if err == nil {
		t.Time = time.Time(time.Unix(n/1e3, (n%1e3)*1e6))
	}
	return err
}

func (t *UnixMillis) MarshalJSON() ([]byte, error) {
	utc := t.UTC()
	sec, nsec := utc.Unix(), int64(utc.Nanosecond())
	return []byte(strconv.FormatInt(sec*1e3+nsec/1e6, 10)), nil
}

// UnixSeconds is compatibility wrapper for representing Discord timestamps as time.Time
type UnixSeconds struct {
	time.Time
}

func (t *UnixSeconds) UnmarshalJSON(b []byte) error {
	n, err := strconv.ParseInt(string(b), 10, 64)
	if err == nil {
		t.Time = time.Time(time.Unix(n, 0))
	}
	return err
}

func (t *UnixSeconds) MarshalJSON() ([]byte, error) {
	return []byte(strconv.FormatInt(t.UTC().Unix(), 10)), nil
}
