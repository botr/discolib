// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dltype

import (
	"bytes"
	"strconv"
)

// StrNum is used in couple of places where Discord developers didn't bother to implement homogenous types and return "number or string" value.
// Since this is not really handy to deal with in statically typed language you may use this handle.
// MarshalJSON, UnmarshalJSON and String prioritize numeric value over string.
type StrNum struct {
	f float64
	s string
}

// FromString sets string. If value already contains float64 it will be reset to 0
func (sn *StrNum) FromString(s string) { sn.f, sn.s = 0, s }

// FromInt sets float64. If value already contains string it will be reset to ""
func (sn *StrNum) FromInt(i int) { sn.FromFloat64(float64(i)) }

// FromFloat64 sets float64. If value already contains string it will be reset to ""
func (sn *StrNum) FromFloat64(f float64) { sn.f, sn.s = f, "" }

// AsString returns string if StrNum value holds string. It will be empty if no string was written
func (sn *StrNum) AsString() string { return sn.s }

// AsInt returns int if StrNum value holds float64. It will be 0 if no float64 was written
func (sn *StrNum) AsInt() int { return int(sn.f) }

// AsInt returns float64 if StrNum value holds float64. It will be 0 if no float64 was written
func (sn *StrNum) AsFloat64() float64 { return sn.f }

// String returns string representation of StrNum whether it stores integer or string
func (sn *StrNum) String() string {
	if sn.f != 0 {
		return strconv.FormatFloat(sn.f, 'f', -1, 64)
	}
	return sn.s
}

func (sn *StrNum) UnmarshalJSON(data []byte) (err error) {
	ln := len(data)
	if ln == 0 {
		return
	}

	s := string(bytes.Trim(data, `"`))
	sn.f, err = strconv.ParseFloat(s, 64)

	if ln == len(s) || err == nil {
		return
	}

	sn.s, err = s, nil
	return
}

func (sn *StrNum) MarshalJSON() ([]byte, error) {
	s := "null"
	switch {
	case sn.f != 0:
		s = sn.String()
	case sn.s != "":
		s = `"` + sn.s + `"`
	}

	return []byte(s), nil
}

// True returns true for use in commands where discord expecting bool to be pointer-type
func True() *bool {
	b := true
	return &b
}

// False returns false for use in commands where discord expecting bool to be pointer-type
func False() *bool {
	b := false
	return &b
}
