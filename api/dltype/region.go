package dltype

import "golang.org/x/text/language"

type Region string

const (
	Brazil      Region = "brazil"
	Russia      Region = "russia"
	Japan       Region = "japan"
	India       Region = "india"
	USWest      Region = "us-west"
	USEast      Region = "us-east"
	USCentral   Region = "us-central"
	USSouth     Region = "us-south"
	Singapore   Region = "singapore"
	SouthAfrica Region = "southafrica"
	Sydney      Region = "sydney"
	HongKong    Region = "hongkong"
	Rotterdam   Region = "rotterdam"
)

func (r Region) Locale() language.Tag {
	switch r {
	case Brazil:
		return language.Portuguese
	case Russia:
		return language.Russian
	case Japan:
		return language.Japanese
	case India:
		return language.Hindi
	case USWest, USEast, USCentral, USSouth:
		return language.AmericanEnglish
	}

	return language.English
}

// PickLocale selects first non-English locale from the list or falls back to English
func PickLocale(languages ...language.Tag) language.Tag {
	for _, tag := range languages {
		if tag == language.Und {
			continue
		}

		b, _, r := tag.Raw()
		loc, reg := b.String(), r.String()

		// en-US seem to be default preferred locale so we just skip it
		if loc != "en" || (reg != "US" && reg != "") {
			return tag
		}
	}

	return language.English
}
