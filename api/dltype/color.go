// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dltype

import (
	"encoding/binary"
	"encoding/hex"
	"strconv"
	"strings"
)

// Color represents RGB color value with 8bpc. This type is compatible with image/color.Color interface
type Color uint32

func ColorFromHex(v string) Color {
	v = strings.TrimLeft(v, "#")

	rgb := make([]byte, 4)
	h, _ := hex.DecodeString(v)
	copy(rgb[1:], h)

	i := binary.BigEndian.Uint32(rgb)
	return Color(i)
}

func (c Color) RGBA() (r, g, b, a uint32) {
	// value range for image/color.Color is 16bpc so we need to extend range

	b = (uint32(c)&0xFF+1)<<010 - 1
	c >>= 010

	g = (uint32(c)&0xFF+1)<<010 - 1
	c >>= 010

	r = (uint32(c)&0xFF+1)<<010 - 1
	c >>= 010

	a = 0xFFFF
	return
}

func (c *Color) MarshalJSON() ([]byte, error) {
	return []byte(strconv.FormatUint(uint64(*c), 10)), nil
}

func (c *Color) UnmarshalJSON(data []byte) (err error) {
	i, err := strconv.ParseUint(string(data), 10, 32)
	if err == nil {
		*c = Color(i)
	}
	return
}
