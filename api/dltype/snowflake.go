// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dltype

import (
	"bytes"
	"errors"
	"strconv"
	"time"
)

var discordEpoch = time.Unix(1420070400, 0) // 2015-01-01 00:00:00

// Snowflake represents identifiers across Discord API
// Twitter's snowflake format for uniquely identifiable descriptors (IDs) up to 64 bits in size (e.g. a uint64).
type Snowflake uint64

// Shard returns shard ID for this Snowflake ID. It is only meaningful for Guild IDs.
func (id Snowflake) Shard(n int) int {
	return int(id>>22) % n
}

// Time returns 42 bits timestamp (bits 63 to 22)
func (id Snowflake) Time() time.Time {
	return discordEpoch.Add(time.Duration(id>>22) * time.Millisecond)
}

// Worker returns 5 bits internal worker id (bits 21 to 17)
func (id Snowflake) Worker() int {
	return int((id & 0x3E0000) >> 17)
}

// Process returns 5 bits internal process id (bits 16 to 12)
func (id Snowflake) Process() int {
	return int((id & 0x1F000) >> 12)
}

// Increment ...
func (id Snowflake) Increment() int {
	return int(id & 0xFFF)
}

// FromTimestamp fills Snowflake with unix timestamp
// func (id *Snowflake) FromTimestamp(t time.Time) {
// 	ts := t.Sub(discordEpoch)
// 	*id = Snowflake(ts.Milliseconds()) << 22
// }

func (id Snowflake) String() string {
	return strconv.FormatUint(uint64(id), 10)
}

func (id Snowflake) DebugString() string {
	return id.String() +
		"(ts:" + id.Time().String() +
		", worker:" + strconv.Itoa(id.Worker()) +
		", process:" + strconv.Itoa(id.Process()) +
		", increment:" + strconv.Itoa(id.Increment()) +
		")"
}

func (id *Snowflake) UnmarshalText(text []byte) error {
	if string(text) == "null" {
		return nil
	}

	text = bytes.Trim(text, `"`)
	i, err := strconv.ParseUint(string(text), 10, 64)
	if err == nil {
		*id = Snowflake(i)
	}
	return err
}

func (id *Snowflake) MarshalText() ([]byte, error) {
	return []byte(id.String()), nil
}

func (id *Snowflake) MarshalJSON() ([]byte, error) {
	return []byte(`"` + id.String() + `"`), nil
}

// Scan implements sql.Scanner
func (id *Snowflake) Scan(src interface{}) error {
	var err error

	switch src := src.(type) {
	case nil:
	case int64:
		*id = Snowflake(src)
	case []byte:
		err = id.UnmarshalText(src)
	case string:
		err = id.UnmarshalText([]byte(src))
	default:
		return errors.New("unsupported type")
	}

	return err
}
