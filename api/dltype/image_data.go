// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dltype

import (
	"bytes"
	"encoding/base64"
)

type ImageType int

//go:generate enumer -type=ImageType -trimprefix=Image -transform=snake $GOFILE
const (
	ImageUnknown ImageType = iota
	ImageJPEG
	ImagePNG
	ImageGIF
)

func (t ImageType) ContentType() string {
	return "image/" + t.String()
}

// ImageData is a Data URI scheme that supports JPG, GIF, and PNG formats. An example Data URI format is:
// data:{ContentType};base64,{base64:Data}
// Ensure you use the proper content type (image/jpeg, image/png, image/gif) that matches the image data being provided.
type ImageData struct {
	Type ImageType
	Data []byte
}

func (id *ImageData) MarshalJSON() (data []byte, err error) {
	var buf bytes.Buffer
	encoder := base64.NewEncoder(base64.StdEncoding, &buf)

	_, err = buf.WriteString("data:" + id.Type.ContentType() + ";base64,")
	if err != nil {
		return
	}

	_, err = encoder.Write(id.Data)
	if err != nil {
		return
	}

	data = buf.Bytes()
	return
}
