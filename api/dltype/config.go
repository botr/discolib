// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dltype

// UserAgent stores pair of values to construct bot user agent:
// "DiscordBot(Name, Version) Meta"
//
// Meta field is optional
type UserAgent struct {
	Name    string `valid:"required"`
	Version string `valid:"required"`

	Meta string
}

func (ua UserAgent) String() string {
	s := "DiscordBot (" + ua.Name + ", " + ua.Version + ")"
	if ua.Meta == "" {
		return s
	}
	return s + " " + ua.Meta
}
