// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dltype

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestColorFromHex(t *testing.T) {
	assert := assert.New(t)

	var src string
	var exp Color

	src, exp = "FFFFFF", 0x00FFFFFF
	assert.Equal(exp, ColorFromHex(src))

	src, exp = "#123456", 0x00123456
	assert.Equal(exp, ColorFromHex(src))
}
