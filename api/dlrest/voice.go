// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dlrest

import "pkg.botr.me/discolib/api/dlresource"

// ListVoiceRegions returns an array of voice region objects that can be used when creating servers.
type ListVoiceRegions struct {
	response []dlresource.VoiceRegion `url:"-" form:"-"`
}

func (req *ListVoiceRegions) Path() string                       { return "/voice/regions" }
func (req *ListVoiceRegions) Response() []dlresource.VoiceRegion { return req.response }
func (req *ListVoiceRegions) Prepare() Request                   { return Request{Method: mGet, Response: &req.response} }
