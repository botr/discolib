// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dlrest

import (
	"pkg.botr.me/discolib/api/dlresource"
	"pkg.botr.me/discolib/api/dltype"
)

// GetChannel returns a channel object. If the channel is a thread, a ThreadMember object is included in the returned result.
type GetChannel struct {
	ChannelID dltype.Snowflake `url:"-" valid:"required"`

	response dlresource.Channel `url:"-" form:"-"`
}

func (req *GetChannel) Path() string                  { return "/channels/" + req.ChannelID.String() }
func (req *GetChannel) Response() *dlresource.Channel { return &req.response }
func (req *GetChannel) Prepare() Request              { return Request{Method: mGet, Response: &req.response} }

// ModifyChannel updates a channel's settings. Requires the MANAGE_CHANNELS permission for the guild.
// Returns a channel on success, and a 400 BAD REQUEST on invalid parameters. Fires a Channel Update Gateway event.
// If modifying a category, individual Channel Update events will fire for each child channel that also changes.
// If modifying permission overwrites, the MANAGE_ROLES permission is required.
// Only permissions your bot has in the guild or channel can be allowed/denied (unless your bot has a MANAGE_ROLES overwrite in the channel).
// All JSON parameters are optional.
type ModifyChannel struct {
	ChannelID dltype.Snowflake `json:"-" url:"-" valid:"required"`

	Name                 string                 `url:"-" json:"name,omitempty"                valid:"omitempty,min=2,max=100"`   // 2-100 character channel name
	Type                 dlresource.ChannelType `url:"-" json:"type,omitempty"`                                                  // the type of channel; only conversion between text and news is supported and only in guilds with the "NEWS" feature
	Position             int                    `url:"-" json:"position,omitempty"`                                              // the position of the channel in the left-hand listing
	Topic                string                 `url:"-" json:"topic,omitempty"               valid:"omitempty,min=0,max=1024"`  // 0-1024 character channel topic (Text, News)
	NSFW                 bool                   `url:"-" json:"nsfw,omitempty"`                                                  // whether the channel is nsfw (Text, News, Store)
	RateLimitPerUser     int                    `url:"-" json:"rate_limit_per_user,omitempty" valid:"omitempty,min=0,max=21600"` // amount of seconds a user has to wait before sending another message (0-21600); bots, as well as users with the permission manage_messages or manage_channel, are unaffected (Text)
	Bitrate              int                    `url:"-" json:"bitrate,omitempty" `                                              // the bitrate (in bits) of the voice channel; 8000 to 96000 (128000 for VIP servers) (Voice)
	UserLimit            int                    `url:"-" json:"user_limit,omitempty"          valid:"omitempty,min=0,max=99"`    // the user limit of the voice channel; 0 refers to no limit, 1 to 99 refers to a user limit (Voice)
	PermissionOverwrites []dlresource.Overwrite `url:"-" json:"permission_overwrites,omitempty"`                                 // channel or category-specific permissions
	ParentID             dltype.Snowflake       `url:"-" json:"parent_id,omitempty"`                                             // id of the new parent category for a channel (Text, News, Store, Voice)

	response dlresource.Channel `url:"-" form:"-"`
}

func (req *ModifyChannel) Path() string                  { return "/channels/" + req.ChannelID.String() }
func (req *ModifyChannel) Response() *dlresource.Channel { return &req.response }
func (req *ModifyChannel) Prepare() Request              { return Request{Method: mPatch, Response: &req.response} }

// DeleteChannel deletes a channel. Requires the MANAGE_CHANNELS permission for the guild.
// Deleting a category does not delete its child channels; they will have their parent_id removed and a Channel Update Gateway event will fire for each of them.
// Returns a channel object on success. Fires a Channel Delete Gateway event.
//
// Deleting a guild channel cannot be undone. Use this with caution, as it is impossible to undo this action when performed on a guild channel.
// In contrast, when used with a private message, it is possible to undo the action by opening a private message with the recipient again.
//
// For Community guilds, the Rules or Guidelines channel and the Community Updates channel cannot be deleted.
type DeleteChannel struct {
	ChannelID dltype.Snowflake `json:"-" url:"-" valid:"required"`

	response dlresource.Channel `url:"-" form:"-"`
}

func (req *DeleteChannel) Path() string                  { return "/channels/" + req.ChannelID.String() }
func (req *DeleteChannel) Response() *dlresource.Channel { return &req.response }
func (req *DeleteChannel) Prepare() Request              { return Request{Method: mDelete, Response: &req.response} }

// CloseChannel closes a private message. Alias for DeleteChannel
type CloseChannel = DeleteChannel

// GetChannelInvites returns a list of invite objects (with invite metadata) for the channel. Only usable for guild channels. Requires the MANAGE_CHANNELS permission.
type GetChannelInvites struct {
	ChannelID dltype.Snowflake `url:"-" valid:"required"`

	response []dlresource.Invite `url:"-" form:"-"`
}

func (req *GetChannelInvites) Path() string {
	return "/channels/" + req.ChannelID.String() + "/invites"
}

func (req *GetChannelInvites) Response() []dlresource.Invite { return req.response }

func (req *GetChannelInvites) Prepare() Request {
	return Request{Method: mGet, Response: &req.response}
}

// CreateChannelInvite create a new invite object for the channel. Only usable for guild channels. Requires the CREATE_INSTANT_INVITE permission.
// All JSON parameters for this route are optional, however the request body is not.
// If you are not sending any fields, you still have to send an empty JSON object ({}).
// Returns an invite object. Fires an Invite Create Gateway event.
type CreateChannelInvite struct {
	ChannelID dltype.Snowflake `json:"-" url:"-" valid:"required"`

	MaxAgeSeconds  *int                      `url:"-" json:"max_age,omitempty"`          // duration of invite in seconds before expiry, or 0 for never (default: 86400)
	MaxUses        int                       `url:"-" json:"max_uses,omitempty"`         // max number of uses or 0 for unlimited (default: 0)
	Temporary      bool                      `url:"-" json:"temporary,omitempty"`        // whether this invite only grants temporary membership (default: false)
	Unique         bool                      `url:"-" json:"unique,omitempty"`           // if true, don't try to reuse a similar invite (useful for creating many unique one time use invites) (default: false)
	TargetUser     dltype.Snowflake          `url:"-" json:"target_user,omitempty"`      // the target user id for this invite
	TargetUserType dlresource.TargetUserType `url:"-" json:"target_user_type,omitempty"` // the type of target user for this invite

	response dlresource.Invite `url:"-" form:"-"`
}

func (req *CreateChannelInvite) Path() string {
	return "/channels/" + req.ChannelID.String() + "/invites"
}

func (req *CreateChannelInvite) Response() *dlresource.Invite { return &req.response }

func (req *CreateChannelInvite) Prepare() Request {
	return Request{Method: mPost, Response: &req.response}
}

// EditChannelPermissions edit the channel permission overwrites for a user or role in a channel. Only usable for guild channels.
// Requires the MANAGE_ROLES permission. Only permissions your bot has in the guild or channel can be allowed/denied (unless your bot has a MANAGE_ROLES overwrite in the channel).
// Returns a 204 empty response on success.
type EditChannelPermissions struct {
	ChannelID   dltype.Snowflake `json:"-" url:"-" valid:"required"`
	OverwriteID dltype.Snowflake `json:"-" url:"-" valid:"required"`

	Allow dlresource.Permission    `url:"-" json:"allow"` // the bitwise value of all allowed permissions
	Deny  dlresource.Permission    `url:"-" json:"deny"`  // the bitwise value of all disallowed permissions
	Type  dlresource.OverwriteType `url:"-" json:"type"`  // 0 for a role or 1 for a member
}

func (req *EditChannelPermissions) Path() string {
	return "/channels/" + req.ChannelID.String() + "/permissions/" + req.OverwriteID.String()
}

func (req *EditChannelPermissions) Prepare() Request { return Request{Method: mPut} }

// DeleteChannelPermission delete a channel permission overwrite for a user or role in a channel. Only usable for guild channels.
// Requires the MANAGE_ROLES permission. Returns a 204 empty response on success.
type DeleteChannelPermission struct {
	ChannelID   dltype.Snowflake `json:"-" url:"-" valid:"required"`
	OverwriteID dltype.Snowflake `json:"-" url:"-" valid:"required"`
}

func (req *DeleteChannelPermission) Path() string {
	return "/channels/" + req.ChannelID.String() + "/permissions/" + req.OverwriteID.String()
}

func (req *DeleteChannelPermission) Prepare() Request { return Request{Method: mDelete} }

// FollowNewsChannel follow a News Channel to send messages to a target channel.
// Requires the MANAGE_WEBHOOKS permission in the target channel. Returns a followed channel object.
type FollowNewsChannel struct {
	ChannelID dltype.Snowflake `json:"-" url:"-" valid:"required"`

	WebhookChannelID dltype.Snowflake `url:"-" json:"webhook_channel_id"` // id of target channel

	response dlresource.FollowedChannel `url:"-" form:"-"`
}

func (req *FollowNewsChannel) Path() string {
	return "/channels/" + req.ChannelID.String() + "/followers"
}

func (req *FollowNewsChannel) Response() *dlresource.FollowedChannel { return &req.response }

func (req *FollowNewsChannel) Prepare() Request {
	return Request{Method: mPost, Response: &req.response}
}

// TriggerTypingIndicator post a typing indicator for the specified channel. Generally bots should not implement this route.
// However, if a bot is responding to a command and expects the computation to take a few seconds, this endpoint may be called to let the user know that the bot is processing their message.
// Returns a 204 empty response on success. Fires a Typing Start Gateway event.
type TriggerTypingIndicator struct {
	ChannelID dltype.Snowflake `json:"-" url:"-" valid:"required"`
}

func (req *TriggerTypingIndicator) Path() string {
	return "/channels/" + req.ChannelID.String() + "/typing"
}

func (req *TriggerTypingIndicator) Prepare() Request { return Request{Method: mPost} }

// CreateWebhook creates a new webhook. Requires the MANAGE_WEBHOOKS permission.
type CreateWebhook struct {
	ChannelID dltype.Snowflake `json:"-" url:"-" valid:"required"`

	Name   string            `url:"-" json:"name" valid:"min=1,max=80"` // name of the webhook (1-80 characters)
	Avatar *dltype.ImageData `url:"-" json:"avatar"`                    // image for the default webhook avatar

	response dlresource.Webhook `url:"-" form:"-"`
}

func (req *CreateWebhook) Path() string                  { return "/channels/" + req.ChannelID.String() + "/webhooks" }
func (req *CreateWebhook) Response() *dlresource.Webhook { return &req.response }
func (req *CreateWebhook) Prepare() Request              { return Request{Method: mPost, Response: &req.response} }

// GetChannelWebhooks returns a list of channel webhook objects. Requires the MANAGE_WEBHOOKS permission.
type GetChannelWebhooks struct {
	ChannelID dltype.Snowflake `url:"-" valid:"required"`

	response []dlresource.Webhook `url:"-" form:"-"`
}

func (req *GetChannelWebhooks) Path() string {
	return "/channels/" + req.ChannelID.String() + "/webhooks"
}

func (req *GetChannelWebhooks) Response() []dlresource.Webhook { return req.response }

func (req *GetChannelWebhooks) Prepare() Request {
	return Request{Method: mGet, Response: &req.response}
}
