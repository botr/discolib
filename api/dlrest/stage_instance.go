package dlrest

import (
	"pkg.botr.me/discolib/api/dlresource"
	"pkg.botr.me/discolib/api/dltype"
)

// CreateStageInstance creates a new Stage instance associated to a Stage channel. Requires the user to be a moderator of the Stage channel
type CreateStageInstance struct {
	ChannelID    dltype.Snowflake `url:"-" json:"channel_id"`                  // The id of the Stage channel
	Topic        string           `url:"-" json:"topic" valid:"min=1,max=120"` // The topic of the Stage instance (1-120 characters)
	PrivacyLevel int              `url:"-" json:"privacy_level,omitempty"`     // The privacy level of the Stage instance (default GUILD_ONLY)
}

func (req *CreateStageInstance) Path() string     { return "/stage-instances" }
func (req *CreateStageInstance) Prepare() Request { return Request{Method: mPost} }

// GetStageInstance gets the stage instance associated with the Stage channel, if it exists
type GetStageInstance struct {
	ChannelID dltype.Snowflake `json:"-" url:"-" valid:"required"`

	response dlresource.StageInstance `url:"-" form:"-"`
}

func (req *GetStageInstance) Path() string                        { return "/stage-instances/" + req.ChannelID.String() }
func (req *GetStageInstance) Response() *dlresource.StageInstance { return &req.response }
func (req *GetStageInstance) Prepare() Request                    { return Request{Method: mGet, Response: &req.response} }

// ModifyStageInstance updates fields of an existing Stage instance.Requires the user to be a moderator of the Stage channel
type ModifyStageInstance struct {
	ChannelID dltype.Snowflake `json:"-" url:"-" valid:"required"`

	Topic        string                  `url:"-" json:"topic,omitempty" valid:"omitempty,max=120"` // The topic of the Stage instance (1-120 characters)
	PrivacyLevel dlresource.PrivacyLevel `url:"-" json:"privacy_level,omitempty"`                   // The privacy level of the Stage instance
}

func (req *ModifyStageInstance) Path() string     { return "/stage-instances/" + req.ChannelID.String() }
func (req *ModifyStageInstance) Prepare() Request { return Request{Method: mPatch} }

// DeleteStageInstance deletes the Stage instance. Requires the user to be a moderator of the Stage channel
type DeleteStageInstance struct {
	ChannelID dltype.Snowflake `json:"-" url:"-" valid:"required"`
}

func (req *DeleteStageInstance) Path() string     { return "/stage-instances/" + req.ChannelID.String() }
func (req *DeleteStageInstance) Prepare() Request { return Request{Method: mDelete} }
