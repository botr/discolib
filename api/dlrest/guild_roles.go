// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dlrest

import (
	"pkg.botr.me/discolib/api/dlresource"
	"pkg.botr.me/discolib/api/dltype"
)

// GetGuildRoles returns a list of role objects for the guild.
type GetGuildRoles struct {
	GuildID dltype.Snowflake `url:"-" valid:"required"`
	RoleID  dltype.Snowflake `url:"-" valid:"required"`

	response []dlresource.Role `url:"-" form:"-"`
}

func (req *GetGuildRoles) Path() string                { return "/guilds/" + req.GuildID.String() + "/roles" }
func (req *GetGuildRoles) Response() []dlresource.Role { return req.response }
func (req *GetGuildRoles) Prepare() Request            { return Request{Method: mGet, Response: &req.response} }

// CreateGuildRole create a new role for the guild. Requires the MANAGE_ROLES permission.
// Returns the new role object on success. Fires a Guild Role Create Gateway event.
type CreateGuildRole struct {
	GuildID dltype.Snowflake `json:"-" url:"-" valid:"required"`
	RoleID  dltype.Snowflake `json:"-" url:"-" valid:"required"`

	Name        string       `url:"-" json:"name,omitempty"`        // name of the role (default: "new role")
	Permissions string       `url:"-" json:"permissions,omitempty"` // bitwise value of the enabled/disabled permissions (default: @everyone permissions in guild)
	Color       dltype.Color `url:"-" json:"color,omitempty"`       // RGB color value (default: 0)
	Hoist       bool         `url:"-" json:"hoist,omitempty"`       // whether the role should be displayed separately in the sidebar (default: false)
	Mentionable bool         `url:"-" json:"mentionable,omitempty"` // whether the role should be mentionable (default: false)

	response dlresource.Role `url:"-" form:"-"`
}

func (req *CreateGuildRole) Path() string               { return "/guilds/" + req.GuildID.String() + "/roles" }
func (req *CreateGuildRole) Response() *dlresource.Role { return &req.response }
func (req *CreateGuildRole) Prepare() Request           { return Request{Method: mPost, Response: &req.response} }

// ModifyGuildRolePositions modify the positions of a set of role objects for the guild. Requires the MANAGE_ROLES permission.
// Returns a list of all of the guild's role objects on success. Fires multiple Guild Role Update Gateway events.
// This endpoint takes a JSON array of parameters in the following format:
type ModifyGuildRolePositions struct {
	GuildID dltype.Snowflake `json:"-" url:"-" valid:"required"`

	Positions []dlresource.Role `url:"-" json:"positions,omitempty"` // array of roles with only id and position

	response []dlresource.Role `url:"-" form:"-"`
}

func (req *ModifyGuildRolePositions) Path() string {
	return "/guilds/" + req.GuildID.String() + "/roles"
}

func (req *ModifyGuildRolePositions) Response() []dlresource.Role { return req.response }

func (req *ModifyGuildRolePositions) Prepare() Request {
	return Request{Method: mPatch, Response: &req.response}
}

// ModifyGuildRole modify a guild role. Requires the MANAGE_ROLES permission.
// Returns the updated role on success. Fires a Guild Role Update Gateway event.
type ModifyGuildRole struct {
	GuildID dltype.Snowflake `json:"-" url:"-" valid:"required"`
	RoleID  dltype.Snowflake `json:"-" url:"-" valid:"required"`

	Name        string       `url:"-" json:"name,omitempty"`        // name of the role
	Permissions string       `url:"-" json:"permissions,omitempty"` // bitwise value of the enabled/disabled permissions
	Color       dltype.Color `url:"-" json:"color,omitempty"`       // RGB color value
	Hoist       bool         `url:"-" json:"hoist,omitempty"`       // whether the role should be displayed separately in the sidebar
	Mentionable bool         `url:"-" json:"mentionable,omitempty"` // whether the role should be mentionable

	response dlresource.Role `url:"-" form:"-"`
}

func (req *ModifyGuildRole) Path() string {
	return "/guilds/" + req.GuildID.String() + "/roles/" + req.RoleID.String()
}

func (req *ModifyGuildRole) Response() *dlresource.Role { return &req.response }

func (req *ModifyGuildRole) Prepare() Request {
	return Request{Method: mPatch, Response: &req.response}
}

// DeleteGuildRole delete a guild role. Requires the MANAGE_ROLES permission.
// Returns a 204 empty response on success. Fires a Guild Role Delete Gateway event.
type DeleteGuildRole struct {
	GuildID dltype.Snowflake `json:"-" url:"-" valid:"required"`
	RoleID  dltype.Snowflake `json:"-" url:"-" valid:"required"`
}

func (req *DeleteGuildRole) Path() string {
	return "/guilds/" + req.GuildID.String() + "/roles/" + req.RoleID.String()
}

func (req *DeleteGuildRole) Prepare() Request { return Request{Method: mDelete} }
