// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dlrest

// GetGateway returns WebSocket Gateway URL.
// Clients should cache this value and only call this endpoint to retrieve a new URL
// if they are unable to properly establish a connection using the cached version of the URL.
//
// NOTE: This endpoint does not require authentication.
type GetGateway struct {
	response struct {
		URL string `json:"url"`
	} `url:"-" form:"-"`
}

func (req *GetGateway) Path() string { return "/gateway" }

// Response returns gateway URL
func (req *GetGateway) Response() string { return req.response.URL }

func (req *GetGateway) Prepare() Request {
	return Request{Method: mGet, NoAuth: true, Response: &req.response}
}

// GetGatewayBot returns an object based on the information in GetGateway,
// plus additional metadata that can help during the operation of large or sharded bots.
// This endpoint requires authentication using a valid bot token.
// Unlike the GetGateway, this route should not be cached for extended periods of time
// as the value is not guaranteed to be the same per-call, and changes as the bot joins/leaves guilds.
type GetGatewayBot struct {
	response GetGatewayBotResponse `url:"-" form:"-"`
}

type GetGatewayBotResponse struct {
	URL string `json:"url"`

	Shards            int `json:"shards"` // The recommended number of shards to use when connecting
	SessionStartLimit *struct {
		Total          int `json:"total"`           // The total number of session starts the current user is allowed
		Remaining      int `json:"remaining"`       // The remaining number of session starts the current user is allowed
		ResetAfter     int `json:"reset_after"`     // The number of milliseconds after which the limit resets
		MaxConcurrency int `json:"max_concurrency"` // The number of identify requests allowed per 5 seconds
	} `json:"session_start_limit"` // Information on the current session start limit
}

func (req *GetGatewayBot) Path() string { return "/gateway/bot" }

func (req *GetGatewayBot) Response() *GetGatewayBotResponse { return &req.response }

func (req *GetGatewayBot) Prepare() Request {
	return Request{Method: mGet, Response: &req.response}
}
