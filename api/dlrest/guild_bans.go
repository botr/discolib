// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dlrest

import (
	"pkg.botr.me/discolib/api/dlresource"
	"pkg.botr.me/discolib/api/dltype"
)

// GetGuildBans returns a list of ban objects for the users banned from this guild. Requires the BAN_MEMBERS permission.
type GetGuildBans struct {
	GuildID dltype.Snowflake `url:"-" valid:"required"`

	response []dlresource.Ban `url:"-" form:"-"`
}

func (req *GetGuildBans) Path() string               { return "/guilds/" + req.GuildID.String() + "/bans" }
func (req *GetGuildBans) Response() []dlresource.Ban { return req.response }
func (req *GetGuildBans) Prepare() Request           { return Request{Method: mGet, Response: &req.response} }

// GetGuildBan returns a ban object for the given user or a 404 not found if the ban cannot be found.
// Requires the BAN_MEMBERS permission.
type GetGuildBan struct {
	GuildID dltype.Snowflake `url:"-" valid:"required"`
	UserID  dltype.Snowflake `url:"-" valid:"required"`

	response dlresource.Ban `url:"-" form:"-"`
}

func (req *GetGuildBan) Path() string {
	return "/guilds/" + req.GuildID.String() + "/bans/" + req.UserID.String()
}

func (req *GetGuildBan) Response() *dlresource.Ban { return &req.response }
func (req *GetGuildBan) Prepare() Request          { return Request{Method: mGet, Response: &req.response} }

// CreateGuildBan create a guild ban, and optionally delete previous messages sent by the banned user.
// Requires the BAN_MEMBERS permission.
// Returns a 204 empty response on success. Fires a Guild Ban Add Gateway event.
type CreateGuildBan struct {
	GuildID dltype.Snowflake `json:"-" url:"-" valid:"required"`
	UserID  dltype.Snowflake `json:"-" url:"-" valid:"required"`

	DeleteMessageDays int    `url:"-" json:"delete_message_days,omitempty" valid:"omitempty,min=0,max=7"` // number of days to delete messages for (0-7)
	Reason            string `url:"-" json:"reason,omitempty"`                                            // reason for the ban
}

func (req *CreateGuildBan) Path() string {
	return "/guilds/" + req.GuildID.String() + "/bans/" + req.UserID.String()
}

func (req *CreateGuildBan) Prepare() Request { return Request{Method: mPut} }

// RemoveGuildBan remove the ban for a user. Requires the BAN_MEMBERS permissions.
// Returns a 204 empty response on success. Fires a Guild Ban Remove Gateway event.
type RemoveGuildBan struct {
	GuildID dltype.Snowflake `json:"-" url:"-" valid:"required"`
	UserID  dltype.Snowflake `json:"-" url:"-" valid:"required"`
}

func (req *RemoveGuildBan) Path() string {
	return "/guilds/" + req.GuildID.String() + "/bans/" + req.UserID.String()
}

func (req *RemoveGuildBan) Prepare() Request { return Request{Method: mDelete} }
