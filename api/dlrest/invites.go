// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dlrest

import "pkg.botr.me/discolib/api/dlresource"

// GetInvite returns an invite object for the given code
type GetInvite struct {
	Code       string `url:"-" valid:"required"`
	WithCounts bool   `url:"with_counts,omitempty"` // whether the invite should contain approximate member counts; optional

	response dlresource.Invite `url:"-" form:"-"`
}

func (req *GetInvite) Path() string                 { return "/invites/" + req.Code }
func (req *GetInvite) Response() *dlresource.Invite { return &req.response }
func (req *GetInvite) Prepare() Request             { return Request{Method: mGet, Response: &req.response} }

// DeleteInvite requires the MANAGE_CHANNELS permission on the channel this invite belongs to
// or MANAGE_GUILD to remove any invite across the guild. Fires a Invite Delete Gateway event.
type DeleteInvite struct {
	Code string `json:"-" url:"-" valid:"required"`
}

func (req *DeleteInvite) Path() string     { return "/invites/" + req.Code }
func (req *DeleteInvite) Prepare() Request { return Request{Method: mDelete} }
