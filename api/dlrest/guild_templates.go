// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dlrest

import (
	"pkg.botr.me/discolib/api/dlresource"
	"pkg.botr.me/discolib/api/dltype"
)

// GetTemplate returns a template object for the given code.
type GetTemplate struct {
	Code string `url:"-" valid:"required"`

	response dlresource.Template `url:"-" form:"-"`
}

func (req *GetTemplate) Path() string                   { return "/guilds/templates/" + req.Code }
func (req *GetTemplate) Response() *dlresource.Template { return &req.response }
func (req *GetTemplate) Prepare() Request               { return Request{Method: mGet, Response: &req.response} }

// CreateGuildFromTemplate fires a Guild Create Gateway event.
// This endpoint can be used only by bots in less than 10 guilds.
type CreateGuildFromTemplate struct {
	Code string `json:"-" url:"-" valid:"required"`

	Name string            `url:"-" json:"name" valid:"min=2,max=100"` // name of the guild (2-100 characters)
	Icon *dltype.ImageData `url:"-" json:"icon,omitempty"`             // base64 128x128 image for the guild icon

	response dlresource.Guild `url:"-" form:"-"`
}

func (req *CreateGuildFromTemplate) Path() string { return "/guilds/templates/" + req.Code }

func (req *CreateGuildFromTemplate) Response() *dlresource.Guild { return &req.response }

func (req *CreateGuildFromTemplate) Prepare() Request {
	return Request{Method: mPost, Response: &req.response}
}

// GetGuildTemplates returns an array of template objects. Requires the MANAGE_GUILD permission.
type GetGuildTemplates struct {
	GuildID dltype.Snowflake `url:"-" valid:"required"`

	response []dlresource.Template `url:"-" form:"-"`
}

func (req *GetGuildTemplates) Path() string { return "/guilds/" + req.GuildID.String() + "/templates" }

func (req *GetGuildTemplates) Response() []dlresource.Template { return req.response }

func (req *GetGuildTemplates) Prepare() Request {
	return Request{Method: mGet, Response: &req.response}
}

// CreateGuildTemplate creates a template for the guild. Requires the MANAGE_GUILD permission. Returns the created template object on success.
type CreateGuildTemplate struct {
	GuildID dltype.Snowflake `json:"-" url:"-" valid:"required"`

	Name        string `url:"-" json:"name"                  valid:"min=1,max=100"` // name of the template (1-100 characters)
	Description string `url:"-" json:"description,omitempty" valid:"max=120"`       // description for the template (optional, 1-120 characters)

	response dlresource.Template `url:"-" form:"-"`
}

func (req *CreateGuildTemplate) Path() string {
	return "/guilds/" + req.GuildID.String() + "/templates"
}

func (req *CreateGuildTemplate) Response() *dlresource.Template { return &req.response }

func (req *CreateGuildTemplate) Prepare() Request {
	return Request{Method: mPost, Response: &req.response}
}

// SyncGuildTemplate syncs the template to the guild's current state. Requires the MANAGE_GUILD permission. Returns the template object on success.
type SyncGuildTemplate struct {
	GuildID dltype.Snowflake `json:"-" url:"-" valid:"required"`
	Code    string           `json:"-" url:"-" valid:"required"`

	response dlresource.Template `url:"-" form:"-"`
}

func (req *SyncGuildTemplate) Path() string {
	return "/guilds/" + req.GuildID.String() + "/templates/" + req.Code
}

func (req *SyncGuildTemplate) Response() *dlresource.Template { return &req.response }

func (req *SyncGuildTemplate) Prepare() Request {
	return Request{Method: mPut, Response: &req.response}
}

// ModifyGuildTemplate modifies the template's metadata. Requires the MANAGE_GUILD permission. Returns the template object on success.
type ModifyGuildTemplate struct {
	GuildID dltype.Snowflake `json:"-" url:"-" valid:"required"`
	Code    string           `json:"-" url:"-" valid:"required"`

	Name        string `url:"-" json:"name,omitempty"        valid:"max=100"` // name of the template (optional, 1-100 characters)
	Description string `url:"-" json:"description,omitempty" valid:"max=120"` // description for the template (optional, 0-120 characters)

	response dlresource.Template `url:"-" form:"-"`
}

func (req *ModifyGuildTemplate) Path() string {
	return "/guilds/" + req.GuildID.String() + "/templates/" + req.Code
}

func (req *ModifyGuildTemplate) Response() *dlresource.Template { return &req.response }

func (req *ModifyGuildTemplate) Prepare() Request {
	return Request{Method: mPatch, Response: &req.response}
}

// DeleteGuildTemplate deletes the template. Requires the MANAGE_GUILD permission. Returns the deleted template object on success.
type DeleteGuildTemplate struct {
	GuildID dltype.Snowflake `json:"-" url:"-" valid:"required"`
	Code    string           `json:"-" url:"-" valid:"required"`

	response dlresource.Template `url:"-" form:"-"`
}

func (req *DeleteGuildTemplate) Path() string {
	return "/guilds/" + req.GuildID.String() + "/templates/" + req.Code
}

func (req *DeleteGuildTemplate) Response() *dlresource.Template { return &req.response }

func (req *DeleteGuildTemplate) Prepare() Request {
	return Request{Method: mDelete, Response: &req.response}
}
