// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dlrest

import (
	"pkg.botr.me/discolib/api/dlresource"
	"pkg.botr.me/discolib/api/dltype"
)

// ListGuildEmojis returns a list of emoji objects for the given guild.
type ListGuildEmojis struct {
	GuildID dltype.Snowflake `url:"-" valid:"required"`

	response []dlresource.Emoji `url:"-" form:"-"`
}

func (req *ListGuildEmojis) Path() string                 { return "/guilds/" + req.GuildID.String() + "/emojis" }
func (req *ListGuildEmojis) Response() []dlresource.Emoji { return req.response }
func (req *ListGuildEmojis) Prepare() Request             { return Request{Method: mGet, Response: &req.response} }

// GetGuildEmoji returns an emoji object for the given guild and emoji IDs.
type GetGuildEmoji struct {
	GuildID dltype.Snowflake `json:"-" url:"-" valid:"required"`
	EmojiID dltype.Snowflake `json:"-" url:"-" valid:"required"`

	response dlresource.Emoji `url:"-" form:"-"`
}

func (req *GetGuildEmoji) Path() string {
	return "/guilds/" + req.GuildID.String() + "/emojis/" + req.EmojiID.String()
}

func (req *GetGuildEmoji) Response() *dlresource.Emoji { return &req.response }

func (req *GetGuildEmoji) Prepare() Request { return Request{Method: mGet, Response: &req.response} }

// CreateGuildEmoji requires the MANAGE_EMOJIS permission. Returns the new emoji object on success.
// Fires a Guild Emojis Update Gateway event. Emojis and animated emojis have a maximum file size of 256kb.
// Attempting to upload an emoji larger than this limit will fail and return 400 Bad Request and an error message, but not a JSON status code.
type CreateGuildEmoji struct {
	GuildID dltype.Snowflake `json:"-" url:"-" valid:"required"`

	Name  string             `url:"-" json:"name"  valid:"required"` // name of the emoji
	Image dltype.ImageData   `url:"-" json:"image" valid:"required"` // the 128x128 emoji image // TODO: check for file size
	Roles []dltype.Snowflake `url:"-" json:"roles"`                  // roles to which this emoji will be whitelisted

	response dlresource.Emoji `url:"-" form:"-"`
}

func (req *CreateGuildEmoji) Path() string { return "/guilds/" + req.GuildID.String() + "/emojis" }

func (req *CreateGuildEmoji) Response() *dlresource.Emoji { return &req.response }

func (req *CreateGuildEmoji) Prepare() Request {
	return Request{Method: mPost, Response: &req.response}
}

// ModifyGuildEmoji requires the MANAGE_EMOJIS permission. Returns the updated emoji object on success.
// Fires a Guild Emojis Update Gateway event.
type ModifyGuildEmoji struct {
	GuildID dltype.Snowflake `json:"-" url:"-" valid:"required"`
	EmojiID dltype.Snowflake `json:"-" url:"-" valid:"required"`

	Name  string             `url:"-" json:"name,omitempty"`  // name of the emoji
	Roles []dltype.Snowflake `url:"-" json:"roles,omitempty"` // roles to which this emoji will be whitelisted

	response dlresource.Emoji `url:"-" form:"-"`
}

func (req *ModifyGuildEmoji) Path() string {
	return "/guilds/" + req.GuildID.String() + "/emojis/" + req.EmojiID.String()
}

func (req *ModifyGuildEmoji) Response() *dlresource.Emoji { return &req.response }

func (req *ModifyGuildEmoji) Prepare() Request {
	return Request{Method: mPatch, Response: &req.response}
}

// DeleteGuildEmoji requires the MANAGE_EMOJIS permission. Returns 204 No Content on success.
// Fires a Guild Emojis Update Gateway event.
type DeleteGuildEmoji struct {
	GuildID dltype.Snowflake `json:"-" url:"-" valid:"required"`
	EmojiID dltype.Snowflake `json:"-" url:"-" valid:"required"`
}

func (req *DeleteGuildEmoji) Path() string {
	return "/guilds/" + req.GuildID.String() + "/emojis/" + req.EmojiID.String()
}

func (req *DeleteGuildEmoji) Prepare() Request { return Request{Method: mDelete} }
