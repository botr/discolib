// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dlrest

import (
	"encoding/json"

	"pkg.botr.me/discolib/api/dlresource"
	"pkg.botr.me/discolib/api/dltype"
)

type WidgetStyleOption string

const (
	WidgetStyleShield  WidgetStyleOption = "shield"  // shield style widget with Discord icon and guild members online count
	WidgetStyleBanner1 WidgetStyleOption = "banner1" // large image with guild icon, name and online count. "POWERED BY DISCORD" as the footer of the widget
	WidgetStyleBanner2 WidgetStyleOption = "banner2" // smaller widget style with guild icon, name and online count. Split on the right with Discord logo
	WidgetStyleBanner3 WidgetStyleOption = "banner3" // large image with guild icon, name and online count. In the footer, Discord logo on the left and "Chat Now" on the right
	WidgetStyleBanner4 WidgetStyleOption = "banner4" // large Discord logo at the top of the widget. Guild icon, name and online count in the middle portion of the widget and a "JOIN MY SERVER" button at the bottom
)

// GetGuildWidgetSettings returns a guild widget object. Requires the MANAGE_GUILD permission.
type GetGuildWidgetSettings struct {
	GuildID dltype.Snowflake `url:"-" valid:"required"`

	response dlresource.GuildWidget `url:"-" form:"-"`
}

func (req *GetGuildWidgetSettings) Path() string {
	return "/guilds/" + req.GuildID.String() + "/widget"
}

func (req *GetGuildWidgetSettings) Response() *dlresource.GuildWidget { return &req.response }

func (req *GetGuildWidgetSettings) Prepare() Request {
	return Request{Method: mGet, Response: &req.response}
}

// ModifyGuildWidget modify a guild widget object for the guild. All attributes may be passed in with JSON and modified.
// Requires the MANAGE_GUILD permission. Returns the updated guild widget object.
type ModifyGuildWidget struct {
	GuildID dltype.Snowflake `url:"-" json:"-" valid:"required"`

	Enabled   bool              `url:"-" json:"enabled"`
	ChannelID *dltype.Snowflake `url:"-" json:"channel_id"`

	response dlresource.GuildWidget `url:"-" form:"-"`
}

func (req *ModifyGuildWidget) Path() string { return "/guilds/" + req.GuildID.String() + "/widget" }

func (req *ModifyGuildWidget) Response() *dlresource.GuildWidget { return &req.response }

func (req *ModifyGuildWidget) Prepare() Request {
	return Request{Method: mPatch, Response: &req.response}
}

// GetGuildWidget returns the widget for the guild.
type GetGuildWidget struct {
	GuildID dltype.Snowflake `url:"-" valid:"required"`

	response json.RawMessage `url:"-" form:"-"`
}

func (req *GetGuildWidget) Path() string              { return "/guilds/" + req.GuildID.String() + "/widget.json" }
func (req *GetGuildWidget) Response() json.RawMessage { return req.response }
func (req *GetGuildWidget) Prepare() Request          { return Request{Method: mGet, Response: &req.response} }

// GetGuildWidgetImage returns a PNG image widget for the guild. Requires no permissions or authentication.
// All parameters to this endpoint are optional.
type GetGuildWidgetImage struct {
	GuildID dltype.Snowflake `url:"-" valid:"required"`

	Style WidgetStyleOption `url:"style,omitempty"` // style of the widget image returned, default: shield

	response string `url:"-" form:"-"`
}

func (req *GetGuildWidgetImage) Path() string {
	return "/guilds/" + req.GuildID.String() + "/widget.png"
}

// Response contains PNG image widged for guild
func (req *GetGuildWidgetImage) Response() string { return req.response }

func (req *GetGuildWidgetImage) Prepare() Request {
	return Request{Method: mGet, NoAuth: true, Response: &req.response}
}
