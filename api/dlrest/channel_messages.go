// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dlrest

import (
	"io/fs"

	"pkg.botr.me/discolib/api/dlresource"
	"pkg.botr.me/discolib/api/dltype"
)

// GetChannelMessages returns the messages for a channel.
// If operating on a guild channel, this endpoint requires the VIEW_CHANNEL permission to be present on the current user.
// If the current user is missing the 'READ_MESSAGE_HISTORY' permission in the channel then this will return no messages (since they cannot read the message history).
// Returns an array of message objects on success.
//
// The before, after, and around keys are mutually exclusive, only one may be passed at a time.
type GetChannelMessages struct {
	ChannelID dltype.Snowflake `url:"-" valid:"required"`

	Around dltype.Snowflake `url:"around,omitempty"`                                // get messages around this message ID (optional)
	Before dltype.Snowflake `url:"before,omitempty"`                                // get messages before this message ID (optional)
	After  dltype.Snowflake `url:"after,omitempty"`                                 // get messages after this message ID (optional)
	Limit  int              `url:"limit,omitempty" valid:"omitempty,min=1,max=100"` // max number of messages to return (1-100) (optional, default: 50)

	response []dlresource.Message `url:"-" form:"-"`
}

func (req *GetChannelMessages) Path() string {
	return "/channels/" + req.ChannelID.String() + "/messages"
}

func (req *GetChannelMessages) Response() []dlresource.Message { return req.response }

func (req *GetChannelMessages) Prepare() Request {
	return Request{Method: mGet, Response: &req.response}
}

// GetChannelMessage returns a specific message in the channel. If operating on a guild channel,
// this endpoint requires the 'READ_MESSAGE_HISTORY' permission to be present on the current user.
// Returns a message object on success.
type GetChannelMessage struct {
	ChannelID dltype.Snowflake `url:"-" valid:"required"`
	MessageID dltype.Snowflake `url:"-" valid:"required"`

	response dlresource.Message `url:"-" form:"-"`
}

func (req *GetChannelMessage) Path() string {
	return "/channels/" + req.ChannelID.String() + "/messages/" + req.MessageID.String()
}

func (req *GetChannelMessage) Response() *dlresource.Message { return &req.response }

func (req *GetChannelMessage) Prepare() Request {
	return Request{Method: mGet, Response: &req.response}
}

// CreateMessage posts a message to a guild text or DM channel. Returns a message object. Fires a Message Create Gateway event.
//
// Limitations:
//  - When operating on a guild channel, the current user must have the SEND_MESSAGES permission.
//  - When sending a message with tts (text-to-speech) set to true, the current user must have the SEND_TTS_MESSAGES permission.
//  - When creating a message as a reply to another message, the current user must have the READ_MESSAGE_HISTORY permission.
//  - The referenced message must exist and cannot be a system message.
//  - The maximum request size when sending a message is 8MB
//  - For the embed object, you can set every field except type (it will be rich regardless of if you try to set it), provider, video, and any height, width, or proxy_url values for images.
//  - Files can only be uploaded when using the multipart/form-data content type.
//
// You may create a message as a reply to another message. To do so, include a message_reference with a message_id.
// The channel_id and guild_id in the message_reference are optional, but will be validated if provided.
//
// This endpoint supports requests with Content-Types of both application/json and multipart/form-data.
// Note that when sending application/json you must send at least one of content or embed,
// and when sending multipart/form-data, you must send at least one of content, embed or file.
// For a file attachment, the Content-Disposition subpart header MUST contain a filename parameter.
//
// NOTE: Before using this endpoint, you must connect to and identify with a gateway at least once.
//
// NOTE: Discord may strip certain characters from message content, like invalid unicode characters or characters which cause unexpected message formatting.
// If you are passing user-generated strings into message content, consider sanitizing the data to prevent unexpected behavior and utilizing allowed_mentions to prevent unexpected mentions.
type CreateMessage struct {
	ChannelID dltype.Snowflake `json:"-" url:"-" valid:"required"`

	Content          string                       `url:"-" json:"content,omitempty" valid:"max=2000"` // the message contents (up to 2000 characters)
	Nonce            string                       `url:"-" json:"nonce,omitempty"`                    // a nonce that can be used for optimistic message sending
	TTS              bool                         `url:"-" json:"tts"`                                // true if this is a TTS message
	Embeds           []dlresource.Embed           `url:"-" json:"embeds,omitempty"`                   // embedded rich content
	AllowedMentions  *dlresource.AllowedMentions  `url:"-" json:"allowed_mentions,omitempty"`         // allowed mentions for a message
	MessageReference *dlresource.MessageReference `url:"-" json:"message_reference,omitempty"`        // include to make your message a reply
	Components       []dlresource.Component       `url:"-" json:"components,omitempty"`

	File fs.File `json:"-" url:"-"` // file the contents of the file being sent

	response dlresource.Message `url:"-" form:"-"`
}

func (req *CreateMessage) Path() string { return "/channels/" + req.ChannelID.String() + "/messages" }

func (req *CreateMessage) Response() *dlresource.Message { return &req.response }

func (req *CreateMessage) Prepare() Request {
	return Request{Method: mPost, File: req.File, Response: &req.response}
}

// CrosspostMessage crosspost a message in a News Channel to following channels.
// This endpoint requires the 'SEND_MESSAGES' permission, if the current user sent the message,
// or additionally the 'MANAGE_MESSAGES' permission, for all other messages, to be present for the current user.Returns a message object.
type CrosspostMessage struct {
	ChannelID dltype.Snowflake `json:"-" url:"-" valid:"required"`
	MessageID dltype.Snowflake `json:"-" url:"-" valid:"required"`

	response dlresource.Message `url:"-" form:"-"`
}

func (req *CrosspostMessage) Path() string {
	return "/channels/" + req.ChannelID.String() + "/messages/" + req.MessageID.String() + "/crosspost"
}

func (req *CrosspostMessage) Response() *dlresource.Message { return &req.response }

func (req *CrosspostMessage) Prepare() Request {
	return Request{Method: mPost, Response: &req.response}
}

/*
 * For the following endpoints, emoji takes the form of name:id for custom guild emoji, or Unicode characters.
 * TODO: urlencode
 */

// CreateReaction create a reaction for the message. This endpoint requires the 'READ_MESSAGE_HISTORY' permission to be present on the current user.
// Additionally, if nobody else has reacted to the message using this emoji, this endpoint requires the 'ADD_REACTIONS' permission to be present on the current user.
// Returns a 204 empty response on success. The emoji must be URL Encoded or the request will fail with 10014: Unknown Emoji.
type CreateReaction struct {
	ChannelID dltype.Snowflake `json:"-" url:"-" valid:"required"`
	MessageID dltype.Snowflake `json:"-" url:"-" valid:"required"`
	Emoji     dlresource.Emoji `json:"-" url:"-" valid:"required"` // TODO: validate id/name to be not empty
}

func (req *CreateReaction) Path() string {
	emoji := *req.Emoji.Name + ":" + req.Emoji.ID.String()
	return "/channels/" + req.ChannelID.String() + "/messages/" + req.MessageID.String() + "/reactions/" + emoji + "/@me"
}

func (req *CreateReaction) Prepare() Request { return Request{Method: mPut} }

// DeleteUserReaction deletes another user's reaction. This endpoint requires the 'MANAGE_MESSAGES' permission to be present on the current user.
// Returns a 204 empty response on success. The emoji must be URL Encoded or the request will fail with 10014: Unknown Emoji.
//
// When user ID is empty it uses @me pseudo ID to act like DeleteOwnReaction.
type DeleteUserReaction struct {
	ChannelID dltype.Snowflake `json:"-" url:"-" valid:"required"`
	MessageID dltype.Snowflake `json:"-" url:"-" valid:"required"`
	Emoji     dlresource.Emoji `json:"-" url:"-" valid:"required"` // TODO: validate id/name to be not empty
	UserID    dltype.Snowflake `json:"-" url:"-"`
}

func (req *DeleteUserReaction) Path() string {
	user := "@me"
	if req.UserID != 0 {
		user = req.UserID.String()
	}

	emoji := *req.Emoji.Name + ":" + req.Emoji.ID.String()
	return "/channels/" + req.ChannelID.String() + "/messages/" + req.MessageID.String() + "/reactions/" + emoji + "/" + user
}

func (req *DeleteUserReaction) Prepare() Request { return Request{Method: mDelete} }

// GetReactions get a list of users that reacted with this emoji. Returns an array of user objects on success. The emoji must be URL Encoded or the request will fail with 10014: Unknown Emoji.
type GetReactions struct {
	ChannelID dltype.Snowflake `url:"-" valid:"required"`
	MessageID dltype.Snowflake `url:"-" valid:"required"`
	Emoji     dlresource.Emoji `url:"-" valid:"required"` // TODO: validate id/name to be not empty

	Before dltype.Snowflake `url:"before,omitempty"`                                // get users before this user ID (optional)
	After  dltype.Snowflake `url:"after,omitempty"`                                 // get users after this user ID (optional)
	Limit  int              `url:"limit,omitempty" valid:"omitempty,min=1,max=100"` // max number of users to return (1-100) (optional, default: 25)

	response []dlresource.User `url:"-" form:"-"`
}

func (req *GetReactions) Path() string {
	emoji := *req.Emoji.Name + ":" + req.Emoji.ID.String()
	return "/channels/" + req.ChannelID.String() + "/messages/" + req.MessageID.String() + "/reactions/" + emoji
}

func (req *GetReactions) Response() []dlresource.User { return req.response }

func (req *GetReactions) Prepare() Request { return Request{Method: mGet, Response: &req.response} }

// DeleteAllReactions deletes all reactions on a message. This endpoint requires the 'MANAGE_MESSAGES' permission to be present on the current user.
// Fires a Message Reaction Remove All Gateway event.
type DeleteAllReactions struct {
	ChannelID dltype.Snowflake `json:"-" url:"-" valid:"required"`
	MessageID dltype.Snowflake `json:"-" url:"-" valid:"required"`
}

func (req *DeleteAllReactions) Path() string {
	return "/channels/" + req.ChannelID.String() + "/messages/" + req.MessageID.String() + "/reactions"
}

func (req *DeleteAllReactions) Prepare() Request { return Request{Method: mDelete} }

// DeleteAllReactionsForEmoji deletes all the reactions for a given emoji on a message.
// This endpoint requires the MANAGE_MESSAGES permission to be present on the current user. Fires a Message Reaction Remove Emoji Gateway event.
// The emoji must be URL Encoded or the request will fail with 10014: Unknown Emoji.
type DeleteAllReactionsForEmoji struct {
	ChannelID dltype.Snowflake `json:"-" url:"-" valid:"required"`
	MessageID dltype.Snowflake `json:"-" url:"-" valid:"required"`
	Emoji     dlresource.Emoji `json:"-" url:"-" valid:"required"` // TODO: validate id/name to be not empty
}

func (req *DeleteAllReactionsForEmoji) Path() string {
	emoji := *req.Emoji.Name + ":" + req.Emoji.ID.String()
	return "/channels/" + req.ChannelID.String() + "/messages/" + req.MessageID.String() + "/reactions/" + emoji
}

func (req *DeleteAllReactionsForEmoji) Prepare() Request { return Request{Method: mDelete} }

// EditMessage edit a previously sent message. The fields content, embed, allowed_mentions and flags can be edited by the original message author.
// Other users can only edit flags and only if they have the MANAGE_MESSAGES permission in the corresponding channel.
// When specifying flags, ensure to include all previously set flags/bits in addition to ones that you are modifying.
// Only flags documented in the table below may be modified by users (unsupported flag changes are currently ignored without error).
// Returns a message object. Fires a Message Update Gateway event.
type EditMessage struct {
	ChannelID dltype.Snowflake `json:"-" url:"-" valid:"required"`
	MessageID dltype.Snowflake `json:"-" url:"-" valid:"required"`

	Content         string                      `url:"-" json:"content,omitempty" valid:"max=2000"` // the new message contents (up to 2000 characters)
	Embeds          []dlresource.Embed          `url:"-" json:"embeds,omitempty"`                   // embedded rich content
	Flags           dlresource.MessageFlag      `url:"-" json:"flags,omitempty"`                    // edit the flags of a message (only SUPPRESS_EMBEDS can currently be set/unset)
	AllowedMentions *dlresource.AllowedMentions `url:"-" json:"allowed_mentions,omitempty"`         // allowed mentions for the message
	Components      []dlresource.Component      `url:"-" json:"components,omitempty"`

	response dlresource.Message `url:"-" form:"-"`
}

func (req *EditMessage) Path() string {
	return "/channels/" + req.ChannelID.String() + "/messages/" + req.MessageID.String()
}

func (req *EditMessage) Response() *dlresource.Message { return &req.response }

func (req *EditMessage) Prepare() Request { return Request{Method: mPatch, Response: &req.response} }

// DeleteMessage delete a message. If operating on a guild channel and trying to delete a message that was not sent by the current user, this endpoint requires the MANAGE_MESSAGES permission.
// Returns a 204 empty response on success. Fires a Message Delete Gateway event.
type DeleteMessage struct {
	ChannelID dltype.Snowflake `json:"-" url:"-" valid:"required"`
	MessageID dltype.Snowflake `json:"-" url:"-" valid:"required"`
}

func (req *DeleteMessage) Path() string {
	return "/channels/" + req.ChannelID.String() + "/messages/" + req.MessageID.String()
}

func (req *DeleteMessage) Prepare() Request { return Request{Method: mDelete} }

// BulkDeleteMessages delete multiple messages in a single request. This endpoint can only be used on guild channels and requires the MANAGE_MESSAGES permission.
// Returns a 204 empty response on success. Fires a Message Delete Bulk Gateway event.
// Any message IDs given that do not exist or are invalid will count towards the minimum and maximum message count (currently 2 and 100 respectively).
//
// This endpoint will not delete messages older than 2 weeks, and will fail with a 400 BAD REQUEST if any message provided is older than that or if any duplicate message IDs are provided.
type BulkDeleteMessages struct {
	ChannelID dltype.Snowflake `json:"-" url:"-" valid:"required"`

	Messages []dltype.Snowflake `url:"-" json:"messages" valid:"min=2,max=100"` // an array of message ids to delete [2,100]
}

func (req *BulkDeleteMessages) Path() string {
	return "/channels/" + req.ChannelID.String() + "/messages/bulk-delete"
}

func (req *BulkDeleteMessages) Prepare() Request { return Request{Method: mPost} }

// GetPinnedMessages returns all pinned messages in the channel as an array of message objects.
type GetPinnedMessages struct {
	ChannelID dltype.Snowflake `url:"-" valid:"required"`

	response []dlresource.Message `url:"-" form:"-"`
}

func (req *GetPinnedMessages) Path() string { return "/channels/" + req.ChannelID.String() + "/pins" }

func (req *GetPinnedMessages) Response() []dlresource.Message { return req.response }

func (req *GetPinnedMessages) Prepare() Request {
	return Request{Method: mGet, Response: &req.response}
}

// AddPinnedChannelMessage pin a message in a channel. Requires the MANAGE_MESSAGES permission. Returns a 204 empty response on success.
// The max pinned messages is 50.
type AddPinnedChannelMessage struct {
	ChannelID dltype.Snowflake `json:"-" url:"-" valid:"required"`
	MessageID dltype.Snowflake `json:"-" url:"-" valid:"required"`
}

func (req *AddPinnedChannelMessage) Path() string {
	return "/channels/" + req.ChannelID.String() + "/pins/" + req.MessageID.String()
}

func (req *AddPinnedChannelMessage) Prepare() Request { return Request{Method: mPut} }

// DeletePinnedChannelMessage delete a pinned message in a channel. Requires the MANAGE_MESSAGES permission.
// Returns a 204 empty response on success.
type DeletePinnedChannelMessage struct {
	ChannelID dltype.Snowflake `json:"-" url:"-" valid:"required"`
	MessageID dltype.Snowflake `json:"-" url:"-" valid:"required"`
}

func (req *DeletePinnedChannelMessage) Path() string {
	return "/channels/" + req.ChannelID.String() + "/pins/" + req.MessageID.String()
}

func (req *DeletePinnedChannelMessage) Prepare() Request { return Request{Method: mDelete} }
