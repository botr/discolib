// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dlrest

import (
	"io/fs"
)

const (
	V9 = 9

	Version = V9
)

// TODO: set field values using validator?
// TODO: figure out how to make nullable AND optional (using additional field which controls whether to put null in marshaler or not)

// copied from net/http
const (
	mGet    = "GET"
	mPost   = "POST"
	mPut    = "PUT"
	mPatch  = "PATCH"
	mDelete = "DELETE"
)

type Stringer interface {
	String() string
}

// Request contains values to make request.
// It is in rest package and not in client to avoid importing http packages and other things you don't need
// in case you want to write and use custom HTTP client.
type Request struct {
	Method      string  // one of http.Method
	ContentType string  // if not empty, will be used instead of default one
	File        fs.File // file contents of the file being sent

	NoAuth bool // authorization enabled by default

	Response interface{} // interface to store return value
}
