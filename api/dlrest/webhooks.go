// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dlrest

import (
	"io/fs"

	"pkg.botr.me/discolib/api/dlresource"
	"pkg.botr.me/discolib/api/dltype"
)

// GetWebhook returns the new webhook object for the given id
type GetWebhook struct {
	ID dltype.Snowflake `url:"-" valid:"required"`

	response dlresource.Webhook `url:"-" form:"-"`
}

func (req *GetWebhook) Path() string                  { return "/webhooks/" + req.ID.String() }
func (req *GetWebhook) Response() *dlresource.Webhook { return &req.response }
func (req *GetWebhook) Prepare() Request              { return Request{Method: mGet, Response: &req.response} }

// GetWebhookWithToken returns webhook object without username in it and does not require authentication
type GetWebhookWithToken struct {
	ID    dltype.Snowflake `url:"-" valid:"required"`
	Token string           `url:"-" valid:"required"`

	response dlresource.Webhook `url:"-" form:"-"`
}

func (req *GetWebhookWithToken) Path() string {
	return "/webhooks/" + req.ID.String() + "/" + req.Token
}

func (req *GetWebhookWithToken) Prepare() Request {
	return Request{Method: mGet, NoAuth: true, Response: &req.response}
}

// ModifyWebhook requires the MANAGE_WEBHOOKS permission. Returns the updated webhook object on success.
type ModifyWebhook struct {
	ID dltype.Snowflake `json:"-" url:"-" valid:"required"`

	Name      string            `json:"name,omitempty"       url:"-"` // the default name of the webhook
	Avatar    *dltype.ImageData `json:"avatar,omitempty"     url:"-"` // image for the default webhook avatar
	ChannelID dltype.Snowflake  `json:"channel_id,omitempty" url:"-"` // the new channel id this webhook should be moved to

	response dlresource.Webhook `url:"-" form:"-"`
}

func (req *ModifyWebhook) Path() string                  { return "/webhooks/" + req.ID.String() }
func (req *ModifyWebhook) Response() *dlresource.Webhook { return &req.response }
func (req *ModifyWebhook) Prepare() Request              { return Request{Method: mPatch, Response: &req.response} }

// ModifyWebhookWithToken does not require authentication, does not accept a channel_id parameter in the body, and does not return a user in the webhook object
type ModifyWebhookWithToken struct {
	ID    dltype.Snowflake `json:"-" url:"-" valid:"required"`
	Token string           `json:"-" url:"-" valid:"required"`

	Name   string            `json:"name,omitempty"       url:"-"` // the default name of the webhook
	Avatar *dltype.ImageData `json:"avatar,omitempty"     url:"-"` // image for the default webhook avatar

	response dlresource.Webhook `url:"-" form:"-"`
}

func (req *ModifyWebhookWithToken) Path() string {
	return "/webhooks/" + req.ID.String() + "/" + req.Token
}

func (req *ModifyWebhookWithToken) Response() *dlresource.Webhook { return &req.response }

func (req *ModifyWebhookWithToken) Prepare() Request {
	return Request{Method: mPatch, NoAuth: true, Response: &req.response}
}

// DeleteWebhook deletes a webhook permanently. Requires the MANAGE_WEBHOOKS permission. Returns a 204 NO CONTENT response on success.
type DeleteWebhook struct {
	ID dltype.Snowflake `json:"-" url:"-" valid:"required"`
}

func (req *DeleteWebhook) Path() string     { return "/webhooks/" + req.ID.String() }
func (req *DeleteWebhook) Prepare() Request { return Request{Method: mDelete} }

// DeleteWebhookWithToken does not require authentication
type DeleteWebhookWithToken struct {
	ID    dltype.Snowflake `json:"-" url:"-" valid:"required"`
	Token string           `json:"-" url:"-" valid:"required"`
}

func (req *DeleteWebhookWithToken) Path() string {
	return "/webhooks/" + req.ID.String() + "/" + req.Token
}

func (req *DeleteWebhookWithToken) Prepare() Request { return Request{Method: mDelete, NoAuth: true} }

// ExecuteWebhook runs webhook action on discord server. One of content, file, embeds must be specified.
type ExecuteWebhook struct {
	ID    dltype.Snowflake `json:"-" url:"-" valid:"required"`
	Token string           `json:"-" url:"-" valid:"required"`
	Wait  bool             `json:"-" url:"wait,omitempty"` // waits for server confirmation of message send before response, and returns the created message body (defaults to false; when false a message that is not saved does not return an error)

	AllowedMentions dlresource.AllowedMentions `url:"-" json:"allowed_mentions,omitempty"` // allowed mentions for the message
	Username        string                     `url:"-" json:"username,omitempty"`         // override the default username of the webhook
	AvatarURL       string                     `url:"-" json:"avatar_url,omitempty"`       // override the default avatar of the webhook
	TTS             bool                       `url:"-" json:"tts,omitempty"`              // true if this is a TTS message

	Content string             `url:"-" json:"content,omitempty" valid:"max=2000"` // the message contents (up to 2000 characters)
	Embeds  []dlresource.Embed `url:"-" json:"embeds,omitempty"  valid:"max=10"`   // up to 10 embedded rich content objects
	File    fs.File            `url:"-" json:"-"`                                  // the contents of the file being sent
}

func (req *ExecuteWebhook) Path() string     { return "/webhooks/" + req.ID.String() + "/" + req.Token }
func (req *ExecuteWebhook) Prepare() Request { return Request{Method: mPost, File: req.File} }

// EditWebhookMessage edits a previously-sent webhook message from the same token.
type EditWebhookMessage struct {
	ID        dltype.Snowflake `json:"-" url:"-" valid:"required"`
	MessageID dltype.Snowflake `json:"-" url:"-" valid:"required"`
	Token     string           `json:"-" url:"-" valid:"required"`

	Content         string                      `url:"-" json:"content,omitempty" valid:"max=2000"` // the message contents (up to 2000 characters)
	Embeds          []dlresource.Embed          `url:"-" json:"embeds,omitempty"  valid:"max=10"`   // up to 10 embedded rich content objects
	AllowedMentions *dlresource.AllowedMentions `url:"-" json:"allowed_mentions,omitempty"`         // allowed mentions for the message
}

func (req *EditWebhookMessage) Path() string {
	return "/webhooks/" + req.ID.String() + "/" + req.Token + "/messages/" + req.MessageID.String()
}

func (req *EditWebhookMessage) Prepare() Request { return Request{Method: mPatch} }

// CreateFollowupMessage Create a followup message for an Interaction. Same as ExecuteWebhook.
type CreateFollowupMessage = ExecuteWebhook

// EditFollowupMessage Edits a followup message for an Interaction. Functions the same as EditWebhookMessage.
type EditFollowupMessage = EditWebhookMessage

// DeleteFollowupMessage deletes a followup message for an Interaction. Returns 204 on success.
type DeleteFollowupMessage struct {
	ID        dltype.Snowflake `json:"-" url:"-" valid:"required"`
	MessageID dltype.Snowflake `json:"-" url:"-" valid:"required"`
	Token     string           `json:"-" url:"-" valid:"required"`
}

func (req *DeleteFollowupMessage) Path() string {
	return "/webhooks/" + req.ID.String() + "/" + req.Token + "/messages/" + req.MessageID.String()
}

func (req *DeleteFollowupMessage) Prepare() Request { return Request{Method: mDelete} }

// EditOriginalInteractionResponse edits the initial Interaction response.
// Same as EditFollowupMessage except message ID
type EditOriginalInteractionResponse struct {
	ID    dltype.Snowflake `json:"-" url:"-" valid:"required"`
	Token string           `json:"-" url:"-" valid:"required"`

	Content         string                      `url:"-" json:"content,omitempty" valid:"max=2000"` // the message contents (up to 2000 characters)
	Embeds          []dlresource.Embed          `url:"-" json:"embeds,omitempty"  valid:"max=10"`   // up to 10 embedded rich content objects
	AllowedMentions *dlresource.AllowedMentions `url:"-" json:"allowed_mentions,omitempty"`         // allowed mentions for the message
}

func (req *EditOriginalInteractionResponse) Path() string {
	return "/webhooks/" + req.ID.String() + "/" + req.Token + "/messages/@original"
}

func (req *EditOriginalInteractionResponse) Prepare() Request { return Request{Method: mPatch} }

// DeleteOriginalInteractionResponse deletes the initial Interaction response. Returns 204 on success.
type DeleteOriginalInteractionResponse struct {
	ID    dltype.Snowflake `json:"-" url:"-" valid:"required"`
	Token string           `json:"-" url:"-" valid:"required"`
}

func (req *DeleteOriginalInteractionResponse) Path() string {
	return "/webhooks/" + req.ID.String() + "/" + req.Token + "/messages/@original"
}

func (req *DeleteOriginalInteractionResponse) Prepare() Request { return Request{Method: mDelete} }
