// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dlrest

import (
	"encoding/json"

	"pkg.botr.me/discolib/api/dlresource"
	"pkg.botr.me/discolib/api/dltype"
)

// GetGuildChannels returns a list of guild channel objects
type GetGuildChannels struct {
	GuildID dltype.Snowflake `url:"-" valid:"required"`

	response []dlresource.Channel `url:"-" form:"-"`
}

func (req *GetGuildChannels) Path() string                   { return "/guilds/" + req.GuildID.String() + "/channels" }
func (req *GetGuildChannels) Response() []dlresource.Channel { return req.response }
func (req *GetGuildChannels) Prepare() Request               { return Request{Method: mGet, Response: &req.response} }

// CreateGuildChannel requires the MANAGE_CHANNELS permission.
// If setting permission overwrites, only permissions your bot has in the guild can be allowed/denied.
// Setting MANAGE_ROLES permission in channels is only possible for guild administrators.
// Returns the new channel object on success. Fires a Channel Create Gateway event.
type CreateGuildChannel struct {
	GuildID dltype.Snowflake `json:"-" url:"-" valid:"required"`

	Name                 string                 `url:"-" json:"name"                          valid:"min=2,max=100"`             // the name of the channel (2-100 characters)
	Type                 dlresource.ChannelType `url:"-" json:"type,omitempty"`                                                  // the type of channel
	Topic                string                 `url:"-" json:"topic,omitempty"               valid:"max=1024"`                  // the channel topic (0-1024 characters)
	Bitrate              int                    `url:"-" json:"bitrate,omitempty"`                                               // the bitrate (in bits) of the voice channel
	UserLimit            int                    `url:"-" json:"user_limit,omitempty"`                                            // the user limit of the voice channel
	RateLimitPerUser     int                    `url:"-" json:"rate_limit_per_user,omitempty" valid:"omitempty,min=0,max=21600"` // amount of seconds a user has to wait before sending another message (0-21600); bots, as well as users with the permission `manage_messages` or `manage_channel`, are unaffected
	Position             int                    `url:"-" json:"position,omitempty"`                                              // sorting position of the channel
	PermissionOverwrites []dlresource.Overwrite `url:"-" json:"permission_overwrites,omitempty"`                                 // explicit permission overwrites for members and roles
	ParentID             dltype.Snowflake       `url:"-" json:"parent_id,omitempty"`                                             // id of the parent category for a channel (each parent category can contain up to 50 channels)
	NSFW                 bool                   `url:"-" json:"nsfw,omitempty"`                                                  // whether the channel is nsfw

	response dlresource.Channel `url:"-" form:"-"`
}

func (req *CreateGuildChannel) Path() string { return "/guilds/" + req.GuildID.String() + "/channels" }

func (req *CreateGuildChannel) Response() *dlresource.Channel { return &req.response }

func (req *CreateGuildChannel) Prepare() Request {
	return Request{Method: mPost, Response: &req.response}
}

// ModifyGuildChannelPositions requires MANAGE_CHANNELS permission. Returns a 204 empty response on success. Fires multiple Channel Update Gateway events.
// Only channels to be modified are required, with the minimum being a swap between at least two channels.
// Channels field contains array of channel objects with only channel ID and Position provided.
type ModifyGuildChannelPositions struct {
	GuildID dltype.Snowflake `json:"-" url:"-" valid:"required"`

	Channels []dlresource.Channel // FIXME: channel type is not optional
}

func (req *ModifyGuildChannelPositions) Path() string {
	return "/guilds/" + req.GuildID.String() + "/channels"
}

func (req *ModifyGuildChannelPositions) Prepare() Request { return Request{Method: mPatch} }

func (req *ModifyGuildChannelPositions) MarshalJSON() ([]byte, error) {
	return json.Marshal(req.Channels)
}
