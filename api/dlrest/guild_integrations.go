// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dlrest

import (
	"pkg.botr.me/discolib/api/dlresource"
	"pkg.botr.me/discolib/api/dltype"
)

// GetGuildIntegrations returns a list of integration objects for the guild. Requires the MANAGE_GUILD permission.
type GetGuildIntegrations struct {
	GuildID dltype.Snowflake `url:"-" valid:"required"`

	response dlresource.Integration `url:"-" form:"-"`
}

func (req *GetGuildIntegrations) Path() string {
	return "/guilds/" + req.GuildID.String() + "/integrations"
}

func (req *GetGuildIntegrations) Response() *dlresource.Integration { return &req.response }

func (req *GetGuildIntegrations) Prepare() Request {
	return Request{Method: mGet, Response: &req.response}
}

// CreateGuildIntegration attach an integration object from the current user to the guild. Requires the MANAGE_GUILD permission.
// Returns a 204 empty response on success. Fires a Guild Integrations Update Gateway event.
type CreateGuildIntegration struct {
	GuildID dltype.Snowflake `json:"-" url:"-" valid:"required"`

	Type dlresource.IntegrationType `url:"-" json:"type" valid:"required"` // the integration type
	ID   dltype.Snowflake           `url:"-" json:"id"   valid:"required"` // the integration id
}

func (req *CreateGuildIntegration) Path() string {
	return "/guilds/" + req.GuildID.String() + "/integrations"
}

func (req *CreateGuildIntegration) Prepare() Request { return Request{Method: mPost} }

// ModifyGuildIntegration modify the behavior and settings of an integration object for the guild. Requires the MANAGE_GUILD permission.
// Returns a 204 empty response on success. Fires a Guild Integrations Update Gateway event.
type ModifyGuildIntegration struct {
	GuildID       dltype.Snowflake `json:"-" url:"-" valid:"required"`
	IntegrationID dltype.Snowflake `json:"-" url:"-" valid:"required"`

	ExpireBehavior  *dlresource.IntegrationExpireBehavior `url:"-" json:"expire_behavior,omitempty"`     // the behavior when an integration subscription lapses
	ExpireGraceDays int                                   `url:"-" json:"expire_grace_period,omitempty"` // period (in days) where the integration will ignore lapsed subscriptions
	EnableEmoticons bool                                  `url:"-" json:"enable_emoticons,omitempty"`    // whether emoticons should be synced for this integration (twitch only currently)
}

func (req *ModifyGuildIntegration) Path() string {
	return "/guilds/" + req.GuildID.String() + "/integrations/" + req.IntegrationID.String()
}

func (req *ModifyGuildIntegration) Prepare() Request { return Request{Method: mPatch} }

// DeleteGuildIntegration delete the attached integration object for the guild.
// Deletes any associated webhooks and kicks the associated bot if there is one. Requires the MANAGE_GUILD permission.
// Returns a 204 empty response on success. Fires a Guild Integrations Update Gateway event.
type DeleteGuildIntegration struct {
	GuildID       dltype.Snowflake `json:"-" url:"-" valid:"required"`
	IntegrationID dltype.Snowflake `json:"-" url:"-" valid:"required"`
}

func (req *DeleteGuildIntegration) Path() string {
	return "/guilds/" + req.GuildID.String() + "/integrations/" + req.IntegrationID.String()
}

func (req *DeleteGuildIntegration) Prepare() Request { return Request{Method: mDelete} }

// SyncGuildIntegration sync an integration. Requires the MANAGE_GUILD permission. Returns a 204 empty response on success.
type SyncGuildIntegration struct {
	GuildID       dltype.Snowflake `json:"-" url:"-" valid:"required"`
	IntegrationID dltype.Snowflake `json:"-" url:"-" valid:"required"`
}

func (req *SyncGuildIntegration) Path() string {
	return "/guilds/" + req.GuildID.String() + "/integrations/" + req.IntegrationID.String() + "/sync"
}

func (req *SyncGuildIntegration) Prepare() Request { return Request{Method: mPost} }
