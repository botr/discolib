// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dlrest

import (
	"pkg.botr.me/discolib/api/dlresource"
	"pkg.botr.me/discolib/api/dltype"
)

// CreateGuild fires a Guild Create Gateway event. This endpoint can be used only by bots in less than 10 guilds.
//
// When using the roles parameter, the first member of the array is used to change properties of the guild's @everyone role.
// If you are trying to bootstrap a guild with additional roles, keep this in mind.
//
// When using the roles parameter, the required id field within each role object is an integer placeholder, and will be replaced by the API upon consumption.
// Its purpose is to allow you to overwrite a role's permissions in a channel when also passing in channels with the channels array.
//
// When using the channels parameter, the position field is ignored, and none of the default channels are created.
//
// When using the channels parameter, the id field within each channel object may be set to an integer placeholder, and will be replaced by the API upon consumption.
// Its purpose is to allow you to create GUILD_CATEGORY channels by setting the parent_id field on any children to the category's id field.
// Category channels must be listed before any children.
type CreateGuild struct {
	Name                        string                                `url:"-" json:"name" valid:"min=2,max=100"`              // name of the guild (2-100 characters)
	Region                      dltype.Region                         `url:"-" json:"region,omitempty"`                        // voice region id
	Icon                        *dltype.ImageData                     `url:"-" json:"icon,omitempty"`                          // base64 128x128 image for the guild icon
	VerificationLevel           dlresource.VerificationLevel          `url:"-" json:"verification_level,omitempty"`            // verification level
	DefaultMessageNotifications dlresource.MessageNotificationLevel   `url:"-" json:"default_message_notifications,omitempty"` // default message notification level
	ExplicitContentFilter       dlresource.ExplicitContentFilterLevel `url:"-" json:"explicit_content_filter,omitempty"`       // explicit content filter level
	Roles                       []dlresource.Role                     `url:"-" json:"roles,omitempty"`                         // new guild roles
	Channels                    []dlresource.Channel                  `url:"-" json:"channels,omitempty"`                      // new guild's channels
	AFKChannelID                dltype.Snowflake                      `url:"-" json:"afk_channel_id,omitempty"`                // id for afk channel
	AFKSeconds                  int                                   `url:"-" json:"afk_timeout,omitempty"`                   // afk timeout in seconds
	SystemChannelID             dltype.Snowflake                      `url:"-" json:"system_channel_id,omitempty"`             // the id of the channel where guild notices such as welcome messages and boost events are posted

	response dlresource.Guild `url:"-" form:"-"`
}

func (req *CreateGuild) Path() string                { return "/guilds" }
func (req *CreateGuild) Response() *dlresource.Guild { return &req.response }
func (req *CreateGuild) Prepare() Request            { return Request{Method: mPost, Response: &req.response} }

// GetGuild returns the guild object for the given id
type GetGuild struct {
	GuildID    dltype.Snowflake `url:"-" valid:"required"`
	WithCounts bool             `url:"with_counts,omitempty"` // when true, will return approximate member and presence counts for the guild; optional

	response dlresource.Guild `url:"-" form:"-"`
}

func (req *GetGuild) Path() string                { return "/guilds/" + req.GuildID.String() }
func (req *GetGuild) Response() *dlresource.Guild { return &req.response }
func (req *GetGuild) Prepare() Request            { return Request{Method: mGet, Response: &req.response} }

// GetGuildPreview returns the guild preview object for the given id. If the user is not in the guild, then the guild must be Discoverable.
type GetGuildPreview struct {
	GuildID dltype.Snowflake `url:"-" valid:"required"`

	response dlresource.GuildPreview `url:"-" form:"-"`
}

func (req *GetGuildPreview) Path() string                       { return "/guilds/" + req.GuildID.String() + "/preview" }
func (req *GetGuildPreview) Response() *dlresource.GuildPreview { return &req.response }
func (req *GetGuildPreview) Prepare() Request                   { return Request{Method: mGet, Response: &req.response} }

// ModifyGuild requires the MANAGE_GUILD permission. Returns the updated guild object on success. Fires a Guild Update Gateway event.
type ModifyGuild struct {
	GuildID dltype.Snowflake `json:"-" url:"-" valid:"required"`

	Name                        string                                `url:"-" json:"name,omitempty" valid:"omitempty,min=2,max=100"` // guild name
	Region                      dltype.Region                         `url:"-" json:"region,omitempty"`                               // guild voice region id
	VerificationLevel           dlresource.VerificationLevel          `url:"-" json:"verification_level,omitempty"`                   // verification level
	DefaultMessageNotifications dlresource.MessageNotificationLevel   `url:"-" json:"default_message_notifications,omitempty"`        // default message notification level
	ExplicitContentFilter       dlresource.ExplicitContentFilterLevel `url:"-" json:"explicit_content_filter,omitempty"`              // explicit content filter level
	AFKChannelID                dltype.Snowflake                      `url:"-" json:"afk_channel_id,omitempty"`                       // id for afk channel
	AFKSeconds                  int                                   `url:"-" json:"afk_timeout,omitempty"`                          // afk timeout in seconds
	Icon                        *dltype.ImageData                     `url:"-" json:"icon,omitempty"`                                 // base64 1024x1024 png/jpeg/gif image for the guild icon (can be animated gif when the server has ANIMATED_ICON feature)
	OwnerID                     dltype.Snowflake                      `url:"-" json:"owner_id,omitempty"`                             // user id to transfer guild ownership to (must be owner)
	Splash                      *dltype.ImageData                     `url:"-" json:"splash,omitempty"`                               // base64 16:9 png/jpeg image for the guild splash (when the server has INVITE_SPLASH feature)
	Banner                      *dltype.ImageData                     `url:"-" json:"banner,omitempty"`                               // base64 16:9 png/jpeg image for the guild banner (when the server has BANNER feature)
	SystemChannelID             dltype.Snowflake                      `url:"-" json:"system_channel_id,omitempty"`                    // the id of the channel where guild notices such as welcome messages and boost events are posted
	RulesChannelID              dltype.Snowflake                      `url:"-" json:"rules_channel_id,omitempty"`                     // the id of the channel where Community guilds display rules and/or guidelines
	PublicUpdatesChannelID      dltype.Snowflake                      `url:"-" json:"public_updates_channel_id,omitempty"`            // the id of the channel where admins and moderators of Community guilds receive notices from Discord
	PreferredLocale             string                                `url:"-" json:"preferred_locale,omitempty"`                     // the preferred locale of a Community guild used in server discovery and notices from Discord; defaults to "en-US"

	response dlresource.Guild `url:"-" form:"-"`
}

func (req *ModifyGuild) Path() string                { return "/guilds/" + req.GuildID.String() }
func (req *ModifyGuild) Response() *dlresource.Guild { return &req.response }
func (req *ModifyGuild) Prepare() Request            { return Request{Method: mPatch, Response: &req.response} }

// DeleteGuild deletes a guild permanently. User must be owner. Fires a Guild Delete Gateway event.
// Returns 204 No Content on success.
type DeleteGuild struct {
	GuildID dltype.Snowflake `json:"-" url:"-" valid:"required"`
}

func (req *DeleteGuild) Path() string     { return "/guilds/" + req.GuildID.String() }
func (req *DeleteGuild) Prepare() Request { return Request{Method: mDelete} }

// GetGuildAuditLog returns an audit log object for the guild. Requires the VIEW_AUDIT_LOG permission.
type GetGuildAuditLog struct {
	GuildID dltype.Snowflake `url:"-" valid:"required"`

	UserID     dltype.Snowflake         `url:"user_id,omitempty"`                               // filter the log for actions made by a user
	ActionType dlresource.AuditLogEvent `url:"action_type,omitempty"`                           // the type of audit log event
	Before     dltype.Snowflake         `url:"before,omitempty"`                                // filter the log before a certain entry id
	Limit      int                      `url:"limit,omitempty" valid:"omitempty,min=1,max=100"` // how many entries are returned: [1,100] (default: 50)

	response dlresource.AuditLog `url:"-" form:"-"`
}

func (req *GetGuildAuditLog) Path() string                   { return "/guilds/" + req.GuildID.String() + "/audit-logs" }
func (req *GetGuildAuditLog) Response() *dlresource.AuditLog { return &req.response }
func (req *GetGuildAuditLog) Prepare() Request               { return Request{Method: mGet, Response: &req.response} }

// GetGuildWebhooks returns a list of guild webhook objects. Requires the MANAGE_WEBHOOKS permission.
type GetGuildWebhooks struct {
	GuildID dltype.Snowflake `url:"-" valid:"required"`

	response []dlresource.Webhook `url:"-" form:"-"`
}

func (req *GetGuildWebhooks) Path() string                   { return "/guilds/" + req.GuildID.String() + "/webhooks" }
func (req *GetGuildWebhooks) Response() []dlresource.Webhook { return req.response }
func (req *GetGuildWebhooks) Prepare() Request               { return Request{Method: mGet, Response: &req.response} }

// GetGuildPruneCount returns the number of members that would be removed in a prune operation. Requires the KICK_MEMBERS permission.
// By default, prune will not remove users with roles. You can optionally include specific roles in your prune by providing the include_roles parameter.
// Any inactive user that has a subset of the provided role(s) will be counted in the prune and users with additional roles will not.
type GetGuildPruneCount struct {
	GuildID dltype.Snowflake `url:"-" valid:"required"`

	Days         int                `url:"days,omitempty" valid:"omitempty,min=1,max=30"` // number of days to count prune for, default: 7
	IncludeRoles []dltype.Snowflake `url:"include_roles,omitempty"`                       // role(s) to include

	response struct {
		Pruned int `json:"pruned"`
	} `url:"-" form:"-"`
}

func (req *GetGuildPruneCount) Path() string { return "/guilds/" + req.GuildID.String() + "/prune" }

// Response returns number of members to be removed
func (req *GetGuildPruneCount) Response() int { return req.response.Pruned }

func (req *GetGuildPruneCount) Prepare() Request {
	return Request{Method: mGet, Response: &req.response}
}

// BeginGuildPrune begin a prune operation. Requires the KICK_MEMBERS permission.
// Returns an object with one 'pruned' key indicating the number of members that were removed in the prune operation.
// For large guilds it's recommended to set the compute_prune_count option to false, forcing 'pruned' to null.
// Fires multiple Guild Member Remove Gateway events.
//
// By default, prune will not remove users with roles. You can optionally include specific roles in your prune by providing the include_roles parameter.
// Any inactive user that has a subset of the provided role(s) will be included in the prune and users with additional roles will not.
type BeginGuildPrune struct {
	GuildID dltype.Snowflake `json:"-" url:"-" valid:"required"`

	Days              int                `url:"-" json:"days" valid:"omitempty,min=1,max=30"` // number of days to count prune for, default: 7
	IncludeRoles      []dltype.Snowflake `url:"-" json:"include_roles,omitempty"`             // role(s) to include
	ComputePruneCount bool               `url:"-" json:"compute_prune_count,omitempty"`       // whether 'pruned' is returned, discouraged for large guilds, default: true

	response struct {
		Pruned int `json:"pruned"`
	} `url:"-" form:"-"`
}

func (req *BeginGuildPrune) Path() string     { return "/guilds/" + req.GuildID.String() + "/prune" }
func (req *BeginGuildPrune) Response() int    { return req.response.Pruned }
func (req *BeginGuildPrune) Prepare() Request { return Request{Method: mPost, Response: &req.response} }

// GetGuildVoiceRegions returns a list of voice region objects for the guild.
// Unlike the similar /voice route, this returns VIP servers when the guild is VIP-enabled.
type GetGuildVoiceRegions struct {
	GuildID dltype.Snowflake `url:"-" valid:"required"`

	response []dlresource.VoiceRegion `url:"-" form:"-"`
}

func (req *GetGuildVoiceRegions) Path() string { return "/guilds/" + req.GuildID.String() + "/regions" }

func (req *GetGuildVoiceRegions) Response() []dlresource.VoiceRegion { return req.response }

func (req *GetGuildVoiceRegions) Prepare() Request {
	return Request{Method: mGet, Response: &req.response}
}

// GetGuildInvites returns a list of invite objects (with invite metadata) for the guild. Requires the MANAGE_GUILD permission.
type GetGuildInvites struct {
	GuildID dltype.Snowflake `url:"-" valid:"required"`

	response []dlresource.Invite `url:"-" form:"-"`
}

func (req *GetGuildInvites) Path() string                  { return "/guilds/" + req.GuildID.String() + "/invites" }
func (req *GetGuildInvites) Response() []dlresource.Invite { return req.response }
func (req *GetGuildInvites) Prepare() Request              { return Request{Method: mGet, Response: &req.response} }

// GetGuildVanityURL returns a partial invite object for guilds with that feature enabled. Requires the MANAGE_GUILD permission.
// code will be null if a vanity url for the guild is not set.
type GetGuildVanityURL struct {
	GuildID dltype.Snowflake `url:"-" valid:"required"`

	response dlresource.Invite `url:"-" form:"-"`
}

func (req *GetGuildVanityURL) Path() string { return "/guilds/" + req.GuildID.String() + "/vanity-url" }

func (req *GetGuildVanityURL) Response() *dlresource.Invite { return &req.response }

func (req *GetGuildVanityURL) Prepare() Request {
	return Request{Method: mGet, Response: &req.response}
}
