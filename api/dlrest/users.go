// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dlrest

import (
	"pkg.botr.me/discolib/api/dlresource"
	"pkg.botr.me/discolib/api/dltype"
)

// GetUser returns a user object for a given user ID.
// When user ID is empty it uses @me pseudo ID to act like GetCurrentUser.
//
// For OAuth2, this requires the identify scope, which will return the object without an email,
// and optionally the email scope, which returns the object with an email.
type GetUser struct {
	UserID dltype.Snowflake `url:"-"`

	response dlresource.User `url:"-" form:"-"`
}

func (req *GetUser) Path() string {
	if req.UserID == 0 {
		return "/users/@me"
	}
	return "/users/" + req.UserID.String()
}

func (req *GetUser) Response() *dlresource.User { return &req.response }

func (req *GetUser) Prepare() Request { return Request{Method: mGet, Response: &req.response} }

// ModifyCurrentUser ...
type ModifyCurrentUser struct {
	Username string            `json:"username,omitempty" url:"-"` // user's username, if changed may cause the user's discriminator to be randomized.
	Avatar   *dltype.ImageData `json:"avatar,omitempty"   url:"-"` // if passed, modifies the user's avatar

	response dlresource.User `url:"-" form:"-"`
}

func (req *ModifyCurrentUser) Path() string { return "/users/@me" }

func (req *ModifyCurrentUser) Response() *dlresource.User { return &req.response }

func (req *ModifyCurrentUser) Prepare() Request {
	return Request{Method: mPatch, Response: &req.response}
}

// GetCurrentUserGuilds returns a list of partial guild objects containing id, name, icon, owner, permissions, features.
// Requires the guilds OAuth2 scope. All query parameters are optional
type GetCurrentUserGuilds struct {
	Before dltype.Snowflake `url:"before,omitempty"`                                // get guilds before this guild ID
	After  dltype.Snowflake `url:"after,omitempty"`                                 // get guilds after this guild ID
	Limit  int              `url:"limit,omitempty" valid:"omitempty,min=1,max=100"` // max number of guilds to return: [1,100]

	response []dlresource.Guild `url:"-" form:"-"`
}

func (req *GetCurrentUserGuilds) Path() string { return "/users/@me/guilds" }

func (req *GetCurrentUserGuilds) Response() []dlresource.Guild { return req.response }

func (req *GetCurrentUserGuilds) Prepare() Request {
	return Request{Method: mGet, Response: &req.response}
}

// LeaveGuild returns a 204 empty response on success
type LeaveGuild struct {
	GuildID dltype.Snowflake `json:"-" url:"-" valid:"required"`
}

func (req *LeaveGuild) Path() string     { return "/users/@me/guilds/" + req.GuildID.String() }
func (req *LeaveGuild) Prepare() Request { return Request{Method: mDelete} }

// GetUserConnections returns a list of connection objects. Requires the connections OAuth2 scope.
type GetUserConnections struct {
	response []dlresource.Connection `url:"-" form:"-"`
}

func (req *GetUserConnections) Path() string { return "/users/@me/connections" }

func (req *GetUserConnections) Response() []dlresource.Connection { return req.response }

func (req *GetUserConnections) Prepare() Request {
	return Request{Method: mGet, Response: &req.response}
}
