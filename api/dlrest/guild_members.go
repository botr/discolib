// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dlrest

import (
	"pkg.botr.me/discolib/api/dlresource"
	"pkg.botr.me/discolib/api/dltype"
)

// GetGuildMember returns a guild member object for the specified user.
type GetGuildMember struct {
	GuildID dltype.Snowflake `url:"-" valid:"required"`
	UserID  dltype.Snowflake `url:"-" valid:"required"`

	response dlresource.GuildMember `url:"-" form:"-"`
}

func (req *GetGuildMember) Path() string {
	return "/guilds/" + req.GuildID.String() + "/members/" + req.UserID.String()
}

func (req *GetGuildMember) Response() *dlresource.GuildMember { return &req.response }

func (req *GetGuildMember) Prepare() Request { return Request{Method: mGet, Response: &req.response} }

// ListGuildMembers returns a list of guild member objects that are members of the guild.
// In the future, this endpoint will be restricted in line with our Privileged Intents
// All parameters to this endpoint are optional
type ListGuildMembers struct {
	GuildID dltype.Snowflake `url:"-" valid:"required"`

	Limit int              `url:"limit,omitempty" valid:"omitempty,max=1000"` // max number of members to return (1-1000) (optional, default: 1)
	After dltype.Snowflake `url:"after,omitempty"`                            // the highest user id in the previous page (optional)

	response []dlresource.GuildMember `url:"-" form:"-"`
}

func (req *ListGuildMembers) Path() string                       { return "/guilds/" + req.GuildID.String() + "/members" }
func (req *ListGuildMembers) Response() []dlresource.GuildMember { return req.response }
func (req *ListGuildMembers) Prepare() Request                   { return Request{Method: mGet, Response: &req.response} }

// AddGuildMember adds a user to the guild, provided you have a valid oauth2 access token for the user with the guilds.join scope.
// Returns a 201 Created with the guild member as the body, or 204 No Content if the user is already a member of the guild.
// Fires a Guild Member Add Gateway event.
//
// The Authorization header must be a Bot token (belonging to the same application used for authorization),
// and the bot must be a member of the guild with CREATE_INSTANT_INVITE permission.
type AddGuildMember struct {
	GuildID dltype.Snowflake `json:"-" url:"-" valid:"required"`
	UserID  dltype.Snowflake `json:"-" url:"-" valid:"required"`

	AccessToken string             `url:"-" json:"access_token" valid:"required"` // an oauth2 access token granted with the guilds.join to the bot's application for the user you want to add to the guild
	Nick        string             `url:"-" json:"nick,omitempty"`                // value to set users nickname to (permission: MANAGE_NICKNAMES)
	Roles       []dltype.Snowflake `url:"-" json:"roles,omitempty"`               // array of role ids the member is assigned (permission: MANAGE_ROLES)
	Mute        bool               `url:"-" json:"mute,omitempty"`                // whether the user is muted in voice channels (permission: MUTE_MEMBERS)
	Deaf        bool               `url:"-" json:"deaf,omitempty"`                // whether the user is deafened in voice channels (permission: DEAFEN_MEMBERS)

	response dlresource.GuildMember `url:"-" form:"-"`
}

func (req *AddGuildMember) Path() string {
	return "/guilds/" + req.GuildID.String() + "/members/" + req.UserID.String()
}

func (req *AddGuildMember) Response() *dlresource.GuildMember { return &req.response }

func (req *AddGuildMember) Prepare() Request { return Request{Method: mPut, Response: &req.response} }

// ModifyGuildMember modify attributes of a guild member. Returns a 200 OK with the guild member as the body.
// Fires a Guild Member Update Gateway event. If the channel_id is set to null, this will force the target user to be disconnected from voice.
// When moving members to channels, the API user must have permissions to both connect to the channel and have the MOVE_MEMBERS permission.
type ModifyGuildMember struct {
	GuildID dltype.Snowflake `json:"-" url:"-" valid:"required"`
	UserID  dltype.Snowflake `json:"-" url:"-" valid:"required"`

	Nick      string             `url:"-" json:"nick,omitempty"`       // value to set users nickname to (permission: MANAGE_NICKNAMES)
	Roles     []dltype.Snowflake `url:"-" json:"roles,omitempty"`      // array of role ids the member is assigned (permission: MANAGE_ROLES)
	Mute      bool               `url:"-" json:"mute,omitempty"`       // whether the user is muted in voice channels. Will throw a 400 if the user is not in a voice channel (permission: MUTE_MEMBERS)
	Deaf      bool               `url:"-" json:"deaf,omitempty"`       // whether the user is deafened in voice channels. Will throw a 400 if the user is not in a voice channel (permission: DEAFEN_MEMBERS)
	ChannelID dltype.Snowflake   `url:"-" json:"channel_id,omitempty"` // id of channel to move user to (if they are connected to voice) (permission: MOVE_MEMBERS)

	response dlresource.GuildMember `url:"-" form:"-"`
}

func (req *ModifyGuildMember) Path() string {
	return "/guilds/" + req.GuildID.String() + "/members/" + req.UserID.String()
}

func (req *ModifyGuildMember) Response() *dlresource.GuildMember { return &req.response }

func (req *ModifyGuildMember) Prepare() Request {
	return Request{Method: mPatch, Response: &req.response}
}

// ModifyCurrentUserNick modifies the nickname of the current user in a guild.
// Returns a 200 with the nickname on success. Fires a Guild Member Update Gateway event.
type ModifyCurrentUserNick struct {
	GuildID dltype.Snowflake `json:"-" url:"-" valid:"required"`

	Nick string `url:"-" json:"nick,omitempty"` // value to set users nickname to (permission: CHANGE_NICKNAME)

	response string `url:"-" form:"-"`
}

func (req *ModifyCurrentUserNick) Path() string {
	return "/guilds/" + req.GuildID.String() + "/members/@me/nick"
}

// Response contains new nickname of current user
func (req *ModifyCurrentUserNick) Response() string { return req.response }

func (req *ModifyCurrentUserNick) Prepare() Request {
	return Request{Method: mPatch, Response: &req.response}
}

// AddGuildMemberRole adds a role to a guild member. Requires the MANAGE_ROLES permission.
// Returns a 204 empty response on success. Fires a Guild Member Update Gateway event.
type AddGuildMemberRole struct {
	GuildID dltype.Snowflake `json:"-" url:"-" valid:"required"`
	UserID  dltype.Snowflake `json:"-" url:"-" valid:"required"`
	RoleID  dltype.Snowflake `json:"-" url:"-" valid:"required"`
}

func (req *AddGuildMemberRole) Path() string {
	return "/guilds/" + req.GuildID.String() + "/members/" + req.UserID.String() + "/roles/" + req.RoleID.String()
}

func (req *AddGuildMemberRole) Prepare() Request { return Request{Method: mPut} }

// RemoveGuildMemberRole removes a role from a guild member. Requires the MANAGE_ROLES permission.
// Returns a 204 empty response on success. Fires a Guild Member Update Gateway event.
type RemoveGuildMemberRole struct {
	GuildID dltype.Snowflake `json:"-" url:"-" valid:"required"`
	UserID  dltype.Snowflake `json:"-" url:"-" valid:"required"`
	RoleID  dltype.Snowflake `json:"-" url:"-" valid:"required"`
}

func (req *RemoveGuildMemberRole) Path() string {
	return "/guilds/" + req.GuildID.String() + "/members/" + req.UserID.String() + "/roles/" + req.RoleID.String()
}

func (req *RemoveGuildMemberRole) Prepare() Request { return Request{Method: mDelete} }

// RemoveGuildMember removes a member from a guild. Requires KICK_MEMBERS permission.
// Returns a 204 empty response on success. Fires a Guild Member Remove Gateway event.
type RemoveGuildMember struct {
	GuildID dltype.Snowflake `json:"-" url:"-" valid:"required"`
	UserID  dltype.Snowflake `json:"-" url:"-" valid:"required"`
}

func (req *RemoveGuildMember) Path() string {
	return "/guilds/" + req.GuildID.String() + "/members/" + req.UserID.String()
}

func (req *RemoveGuildMember) Prepare() Request { return Request{Method: mDelete} }
