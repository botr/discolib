// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dlrest

import (
	"encoding/json"

	"pkg.botr.me/discolib/api/dlresource"
	"pkg.botr.me/discolib/api/dltype"
)

// GetGlobalApplicationCommands fetches all of the global commands for your application.
// Returns an array of ApplicationCommand objects.
type GetGlobalApplicationCommands struct {
	ApplicationID dltype.Snowflake `url:"-" valid:"required"`

	response []dlresource.ApplicationCommand `url:"-" form:"-"`
}

func (req *GetGlobalApplicationCommands) Path() string {
	return "/applications/" + req.ApplicationID.String() + "/commands"
}

func (req *GetGlobalApplicationCommands) Response() []dlresource.ApplicationCommand {
	return req.response
}

func (req *GetGlobalApplicationCommands) Prepare() Request {
	return Request{Method: mGet, Response: &req.response}
}

// CreateGlobalApplicationCommand returns an ApplicationCommand object.
// New global commands will be available in all guilds after 1 hour.
//
// Creating a command with the same name as an existing command for your application will overwrite the old command.
type CreateGlobalApplicationCommand struct {
	ApplicationID dltype.Snowflake `json:"-" url:"-" valid:"required"`

	Name              string                                `url:"-" json:"name"               valid:"min=1,max=32"`  // 1-32 character command name
	Description       string                                `url:"-" json:"description"        valid:"min=1,max=100"` // 1-100 character description
	Options           []dlresource.ApplicationCommandOption `url:"-" json:"options,omitempty"  valid:"max=25"`        // the parameters for the command
	DefaultPermission *bool                                 `url:"-" json:"default_permission,omitempty"`             // whether the command is enabled by default when the app is added to a guild
	Type              dlresource.ApplicationCommandType     `url:"-" json:"type,omitempty"`                           // the type of command, defaults 1 if not set

	response dlresource.ApplicationCommand `url:"-" form:"-"`
}

func (req *CreateGlobalApplicationCommand) Path() string {
	return "/applications/" + req.ApplicationID.String() + "/commands"
}

func (req *CreateGlobalApplicationCommand) Response() *dlresource.ApplicationCommand {
	return &req.response
}

func (req *CreateGlobalApplicationCommand) Prepare() Request {
	return Request{Method: mPost, Response: &req.response}
}

// GetGlobalApplicationCommand fetches a global command for your application. Returns an ApplicationCommand object.
type GetGlobalApplicationCommand struct {
	ApplicationID dltype.Snowflake `url:"-" valid:"required"`
	CommandID     dltype.Snowflake `url:"-" valid:"required"`

	response dlresource.ApplicationCommand `url:"-" form:"-"`
}

func (req *GetGlobalApplicationCommand) Path() string {
	return "/applications/" + req.ApplicationID.String() + "/commands/" + req.CommandID.String()
}

func (req *GetGlobalApplicationCommand) Response() *dlresource.ApplicationCommand {
	return &req.response
}

func (req *GetGlobalApplicationCommand) Prepare() Request {
	return Request{Method: mGet, Response: &req.response}
}

// EditGlobalApplicationCommand returns an ApplicationCommand object. Updates will be available in all guilds after 1 hour.
type EditGlobalApplicationCommand struct {
	ApplicationID dltype.Snowflake `json:"-" url:"-" valid:"required"`
	CommandID     dltype.Snowflake `json:"-" url:"-" valid:"required"`

	Name              string                                `url:"-" json:"name,omitempty"        valid:"max=32"`  // 1-32 character command name
	Description       string                                `url:"-" json:"description,omitempty" valid:"max=100"` // 1-100 character description
	Options           []dlresource.ApplicationCommandOption `url:"-" json:"options,omitempty"`                     // the parameters for the command
	DefaultPermission *bool                                 `url:"-" json:"default_permission,omitempty"`          // whether the command is enabled by default when the app is added to a guild

	response dlresource.ApplicationCommand `url:"-" form:"-"`
}

func (req *EditGlobalApplicationCommand) Path() string {
	return "/applications/" + req.ApplicationID.String() + "/commands/" + req.CommandID.String()
}

func (req *EditGlobalApplicationCommand) Response() *dlresource.ApplicationCommand {
	return &req.response
}

func (req *EditGlobalApplicationCommand) Prepare() Request {
	return Request{Method: mPatch, Response: &req.response}
}

// DeleteGlobalApplicationCommand returns 204
type DeleteGlobalApplicationCommand struct {
	ApplicationID dltype.Snowflake `json:"-" url:"-" valid:"required"`
	CommandID     dltype.Snowflake `json:"-" url:"-" valid:"required"`
}

func (req *DeleteGlobalApplicationCommand) Path() string {
	return "/applications/" + req.ApplicationID.String() + "/commands/" + req.CommandID.String()
}

func (req *DeleteGlobalApplicationCommand) Prepare() Request { return Request{Method: mDelete} }

// BulkOverwriteGlobalApplicationCommands takes a list of application commands, overwriting the existing global command list for this application.
// Updates will be available in all guilds after 1 hour. Returns 200 and a list of application command objects.
// Commands that do not already exist will count toward daily application command create limits.
//
// NOTE: This will overwrite all types of application commands: slash commands, user commands, and message commands.
type BulkOverwriteGlobalApplicationCommands struct {
	ApplicationID dltype.Snowflake `json:"-" url:"-" valid:"required"`

	response []dlresource.ApplicationCommand `url:"-" form:"-"`
}

func (req *BulkOverwriteGlobalApplicationCommands) Path() string {
	return "/applications/" + req.ApplicationID.String() + "/commands"
}

func (req *BulkOverwriteGlobalApplicationCommands) Response() []dlresource.ApplicationCommand {
	return req.response
}

func (req *BulkOverwriteGlobalApplicationCommands) Prepare() Request {
	return Request{Method: mPut, Response: &req.response}
}

// GetGuildApplicationCommands returns an array of ApplicationCommand objects.
type GetGuildApplicationCommands struct {
	ApplicationID dltype.Snowflake `url:"-" valid:"required"`
	GuildID       dltype.Snowflake `url:"-" valid:"required"`

	response []dlresource.ApplicationCommand `url:"-" form:"-"`
}

func (req *GetGuildApplicationCommands) Path() string {
	return "/applications/" + req.ApplicationID.String() + "/guilds/" + req.GuildID.String() + "/commands"
}

func (req *GetGuildApplicationCommands) Response() []dlresource.ApplicationCommand {
	return req.response
}

func (req *GetGuildApplicationCommands) Prepare() Request {
	return Request{Method: mGet, Response: &req.response}
}

// CreateGuildApplicationCommand returns an ApplicationCommand object. New guild commands will be available in the guild immediately.
// Creating a command with the same name as an existing command for your application will overwrite the old command.
type CreateGuildApplicationCommand struct {
	ApplicationID dltype.Snowflake `json:"-" url:"-" valid:"required"`
	GuildID       dltype.Snowflake `json:"-" url:"-" valid:"required"`

	Name              string                                `url:"-" json:"name"              valid:"min=1,max=32"`  // 1-32 character command name
	Description       string                                `url:"-" json:"description"       valid:"min=1,max=100"` // 1-100 character description
	Options           []dlresource.ApplicationCommandOption `url:"-" json:"options,omitempty" valid:"max=25"`        // the parameters for the command
	DefaultPermission *bool                                 `url:"-" json:"default_permission,omitempty"`            // whether the command is enabled by default when the app is added to a guild
	Type              dlresource.ApplicationCommandType     `url:"-" json:"type,omitempty"`                          // the type of command, defaults 1 if not set

	response dlresource.ApplicationCommand `url:"-" form:"-"`
}

func (req *CreateGuildApplicationCommand) Path() string {
	return "/applications/" + req.ApplicationID.String() + "/guilds/" + req.GuildID.String() + "/commands"
}

func (req *CreateGuildApplicationCommand) Response() *dlresource.ApplicationCommand {
	return &req.response
}

func (req *CreateGuildApplicationCommand) Prepare() Request {
	return Request{Method: mPost, Response: &req.response}
}

// GetGuildApplicationCommand returns an array of ApplicationCommand objects.
type GetGuildApplicationCommand struct {
	ApplicationID dltype.Snowflake `url:"-" valid:"required"`
	GuildID       dltype.Snowflake `url:"-" valid:"required"`
	CommandID     dltype.Snowflake `url:"-" valid:"required"`

	response dlresource.ApplicationCommand `url:"-" form:"-"`
}

func (req *GetGuildApplicationCommand) Path() string {
	return "/applications/" + req.ApplicationID.String() + "/guilds/" + req.GuildID.String() + "/commands/" + req.CommandID.String()
}

func (req *GetGuildApplicationCommand) Response() *dlresource.ApplicationCommand {
	return &req.response
}

func (req *GetGuildApplicationCommand) Prepare() Request {
	return Request{Method: mGet, Response: &req.response}
}

// EditGuildApplicationCommand returns an ApplicationCommand object. Updates for guild commands will be available immediately.
type EditGuildApplicationCommand struct {
	ApplicationID dltype.Snowflake `json:"-" url:"-" valid:"required"`
	GuildID       dltype.Snowflake `json:"-" url:"-" valid:"required"`
	CommandID     dltype.Snowflake `json:"-" url:"-" valid:"required"`

	Name              string                                `url:"-" json:"name,omitempty"        valid:"max=32"`  // 1-32 character command name
	Description       string                                `url:"-" json:"description,omitempty" valid:"max=100"` // 1-100 character description
	Options           []dlresource.ApplicationCommandOption `url:"-" json:"options,omitempty"     valid:"max=25"`  // the parameters for the command
	DefaultPermission *bool                                 `url:"-" json:"default_permission,omitempty"`          // whether the command is enabled by default when the app is added to a guild

	response dlresource.ApplicationCommand `url:"-" form:"-"`
}

func (req *EditGuildApplicationCommand) Path() string {
	return "/applications/" + req.ApplicationID.String() + "/guilds/" + req.GuildID.String() + "/commands/" + req.CommandID.String()
}

func (req *EditGuildApplicationCommand) Response() *dlresource.ApplicationCommand {
	return &req.response
}

func (req *EditGuildApplicationCommand) Prepare() Request {
	return Request{Method: mPatch, Response: &req.response}
}

// DeleteGuildApplicationCommand returns 204
type DeleteGuildApplicationCommand struct {
	ApplicationID dltype.Snowflake `json:"-" url:"-" valid:"required"`
	GuildID       dltype.Snowflake `json:"-" url:"-" valid:"required"`
	CommandID     dltype.Snowflake `json:"-" url:"-" valid:"required"`
}

func (req *DeleteGuildApplicationCommand) Path() string {
	return "/applications/" + req.ApplicationID.String() + "/guilds/" + req.GuildID.String() + "/commands/" + req.CommandID.String()
}

func (req *DeleteGuildApplicationCommand) Prepare() Request { return Request{Method: mDelete} }

// GetGuildApplicationCommandPermissions fetches command permissions for all commands for your application in a guild. Returns an array of ApplicationCommandPermissions.
type GetGuildApplicationCommandPermissions struct {
	ApplicationID dltype.Snowflake `json:"-" url:"-" valid:"required"`
	GuildID       dltype.Snowflake `json:"-" url:"-" valid:"required"`

	response []dlresource.ApplicationCommandPermissions `url:"-" form:"-"`
}

func (req *GetGuildApplicationCommandPermissions) Path() string {
	return "/applications/" + req.ApplicationID.String() + "/guilds/" + req.GuildID.String() + "/commands/permissions"
}

func (req *GetGuildApplicationCommandPermissions) Response() []dlresource.ApplicationCommandPermissions {
	return req.response
}

func (req *GetGuildApplicationCommandPermissions) Prepare() Request {
	return Request{Method: mGet, Response: &req.response}
}

// GetApplicationCommandPermissions fetches command permissions for a specific command for your application in a guild. Returns an array of ApplicationCommandPermissions.
type GetApplicationCommandPermissions struct {
	ApplicationID dltype.Snowflake `json:"-" url:"-" valid:"required"`
	GuildID       dltype.Snowflake `json:"-" url:"-" valid:"required"`
	CommandID     dltype.Snowflake `json:"-" url:"-" valid:"required"`

	response []dlresource.ApplicationCommandPermissions `url:"-" form:"-"`
}

func (req *GetApplicationCommandPermissions) Path() string {
	return "/applications/" + req.ApplicationID.String() + "/guilds/" + req.GuildID.String() + "/commands/" + req.CommandID.String() + "/permissions"
}

func (req *GetApplicationCommandPermissions) Response() []dlresource.ApplicationCommandPermissions {
	return req.response
}

func (req *GetApplicationCommandPermissions) Prepare() Request {
	return Request{Method: mGet, Response: &req.response}
}

// EditApplicationCommandPermissions edits command permissions for a specific command for your application in a guild.
//
// NOTE: Deleting or renaming a command will permanently delete all permissions for that command
//
// NOTE: This endpoint will overwrite existing permissions for the command in that guild
type EditApplicationCommandPermissions struct {
	ApplicationID dltype.Snowflake `json:"-" url:"-" valid:"required"`
	GuildID       dltype.Snowflake `json:"-" url:"-" valid:"required"`
	CommandID     dltype.Snowflake `json:"-" url:"-" valid:"required"`

	Permissions []dlresource.ApplicationCommandPermissions `json:"permissions"`
}

func (req *EditApplicationCommandPermissions) Path() string {
	return "/applications/" + req.ApplicationID.String() + "/guilds/" + req.GuildID.String() + "/commands/" + req.CommandID.String() + "/permissions"
}

func (req *EditApplicationCommandPermissions) Prepare() Request { return Request{Method: mPut} }

// BatchEditApplicationCommandPermissions edits permissions for all commands in a guild.
//
// NOTE: This endpoint will overwrite all existing permissions for all commands in a guild
type BatchEditApplicationCommandPermissions struct {
	ApplicationID dltype.Snowflake `json:"-" url:"-" valid:"required"`
	GuildID       dltype.Snowflake `json:"-" url:"-" valid:"required"`

	Permissions []dlresource.GuildApplicationCommandPermissions
}

func (req *BatchEditApplicationCommandPermissions) Path() string {
	return "/applications/" + req.ApplicationID.String() + "/guilds/" + req.GuildID.String() + "/commands/permissions"
}

func (req *BatchEditApplicationCommandPermissions) Prepare() Request { return Request{Method: mPut} }

func (req *BatchEditApplicationCommandPermissions) MarshalJSON() ([]byte, error) {
	return json.Marshal(req.Permissions)
}
