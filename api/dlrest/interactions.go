// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dlrest

import (
	"pkg.botr.me/discolib/api/dlresource"
	"pkg.botr.me/discolib/api/dltype"
)

// CreateInteractionResponse creates a response to an Interaction from the dlgateway. Takes an Interaction Response.
type CreateInteractionResponse struct {
	InteractionID dltype.Snowflake `json:"-" url:"-" valid:"required"`
	Token         string           `json:"-" url:"-" valid:"required"`

	Type dlresource.InteractionCallbackType  `url:"-" json:"type" valid:"required"` // the type of response
	Data *dlresource.InteractionCallbackData `url:"-" json:"data,omitempty"`        // an optional response message
}

func (req *CreateInteractionResponse) Path() string {
	return "/interactions/" + req.InteractionID.String() + "/" + req.Token + "/callback"
}

func (req *CreateInteractionResponse) Prepare() Request { return Request{Method: mPost} }
