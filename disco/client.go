// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package disco

import (
	"context"
	"runtime"
	"sync"
	"time"

	"pkg.botr.me/discolib/api/dlauth"
	"pkg.botr.me/discolib/api/dlgateway"
	"pkg.botr.me/discolib/api/dlrest"
	"pkg.botr.me/discolib/api/dltype"
	"pkg.botr.me/discolib/disco/dlclient"
	"pkg.botr.me/discolib/disco/dltransport"
	"pkg.botr.me/discolib/internal"
	"pkg.botr.me/discolib/internal/errstr"
	"pkg.botr.me/discolib/internal/log"
)

const (
	concurrencyWindow = 5 * time.Second
)

const (
	ErrInvalidInput errstr.Error = "invalid input"
)

// Config contains parameters for Client to operate.
type Config struct {
	Token          string                `valid:"required"`                 // authentication token (from developer settings)
	LargeThreshold int                   `valid:"omitempty,min=20,max=250"` // number of offline members
	Shards         int                   `valid:"omitempty,min=1"`          // used for Guild Sharding. Do not set to use recommended value from API
	Intents        dlgateway.Intent      // limits incoming events
	Encoding       dlgateway.Encoding    // gateway packet encoding
	Compress       dlgateway.Compression // gateway transport compression

	SkipValidation bool // skips command values validation

	dltype.UserAgent

	// NOTE: validation tags should be kept in sync with dlgateway.IdentifyPayload
}

type Client struct {
	htoken   string // token for HTTP requests
	encoding dlgateway.Encoding
	compress dlgateway.Compression

	valid internal.Validate
	log   log.StructuredLogger

	http       *dlclient.Client
	dispatcher dlgateway.EventDispatcher
	shards     []shard

	state gatewayState
}

func NewClient(log log.StructuredLogger, dispatcher dlgateway.EventDispatcher, config Config) (*Client, error) {
	err := internal.DefaultValidator.Struct(&config)
	if err != nil {
		return nil, err
	}

	c := Client{
		htoken:     dlauth.Token{Type: dlauth.TokenBot, Value: config.Token}.HeaderValue(),
		valid:      internal.DefaultValidator,
		log:        log,
		dispatcher: dispatcher,
	}

	c.http, err = dlclient.New(dlclient.Config{UA: config.UserAgent})
	if err != nil {
		err = errstr.Error("can't create http client").Wrap(err)
		return nil, err
	}

	if config.SkipValidation {
		c.valid = nil
	}

	c.encoding = config.Encoding
	if config.Encoding == "" {
		c.encoding = dlgateway.EncodingJSON
	}

	c.compress = config.Compress

	c.state.identify = dlgateway.IdentifyPayload{
		Properties: dlgateway.ConnectionProperties{
			OS:      runtime.GOOS,
			Browser: internal.LibName,
			Device:  internal.LibName,
		},
		Token:          config.Token,
		LargeThreshold: config.LargeThreshold,
		Shard:          []int{-1, config.Shards},
		Intents:        config.Intents,
	}

	return &c, nil
}

// Send writes gateway command to client
func (c *Client) Send(ctx context.Context, cmd dltransport.Command) error {
	if c.valid != nil {
		if err := c.valid.Struct(cmd); err != nil {
			return ErrInvalidInput.Wrap(err)
		}
	}

	switch cmd := cmd.(type) {
	case dlgateway.VoiceStateUpdatePayload:
		n := cmd.GuildID.Shard(len(c.shards))
		return c.shards[n].transport.Send(cmd)

	case dlgateway.RequestGuildMembersPayload:
		n := cmd.GuildID.Shard(len(c.shards))
		return c.shards[n].transport.Send(cmd)

	case dlgateway.PresenceUpdatePayload:
		// if there is shard information in context then it's probably comes from event handler
		if shardn, ok := contextShard(ctx); ok {
			err := c.shards[shardn].transport.Send(cmd)
			if err == nil {
				c.state.StorePresence(dlgateway.StatusUpdate(cmd))
			}
			return err
		}

		// otherwise broadcast across every shard
		for i := range c.shards {
			err := c.shards[i].transport.Send(cmd)
			if err != nil {
				return err
			}
		}
		c.state.StorePresence(dlgateway.StatusUpdate(cmd))
		return nil
	}

	// TODO: return error if connection is down and reconnecting that can be checked by receiver to retry later
	return errstr.Error("command not allowed").Describe(cmd.OpCode())
}

// Request passes request to HTTP client and sets Authorization header if necessary.
// Do not run OAuth2 requests using this method, since some of them require Basic HTTP Authorization.
func (c *Client) Request(ctx context.Context, req dlclient.Preparer) error {
	var err error
	prep := req.Prepare()
	if prep.NoAuth {
		err = c.http.Request(ctx, req)
	} else {
		err = c.http.AuthRequest(ctx, c.htoken, req)
	}

	c.log.Debugw("request", "method", prep.Method, "path", req.Path(), "req", req, "response", req.Prepare().Response, "err", err)
	return err
}

// Query passes requests to HTTP client
func (c *Client) Query(req dlclient.Query) (string, error) {
	url, err := c.http.Query(req)

	c.log.Debugw("query", "path", req.Path(), "req", req, "url", url, "err", err)
	return url, err
}

// Close drops current Listen. Doesn't block.
func (c *Client) Close() {
	for i := range c.shards {
		c.shards[i].quit <- struct{}{}
	}
}

// Listen blocks until Close called. If connection needs to be resumed, client will try its best to reconnect.
// Gateway URL retrieved using GetGateway REST endpoint.
func (c *Client) Listen() error {
	var err error
	defer func() {
		c.log.Debugw("client stop", "err", err)
	}()

	var r dlrest.GetGatewayBot
	if err := c.Request(context.Background(), &r); err != nil {
		err = errstr.Error("gateway request").Wrap(err)
		return err
	}

	params := r.Response()
	c.state.SetURL(params.URL)

	// FIXME: do not cache this url
	// TODO: enable resharding without server stop

	c.log.Debugw("bot gateway", "params", params)

	nshards := c.state.identify.Shard[1]
	if nshards == 0 {
		nshards = params.Shards
		c.state.identify.Shard[1] = nshards
	}

	c.log.Debugw("starting gateway with shards", "shards", nshards, "recommended", params.Shards)

	// prepare shards before listening
	for id := 0; id < nshards; id++ {
		log := log.WithStructuredLogger(c.log, "shard", id)
		t, err := dltransport.New(log, dltransport.Config{
			Version:  dlgateway.Version,
			Encoding: c.encoding,
			Compress: c.compress,
		})
		if err != nil {
			return errstr.Error("can't create gateway transport").Wrap(err)
		}

		c.shards = append(c.shards, shard{
			id:              id,
			log:             log,
			state:           &c.state,
			transport:       t,
			EventDispatcher: c.dispatcher,
		})

		c.shards[id].dispatch = dlgateway.NewEventHandler(&c.shards[id], t.Unmarshaler())
		c.shards[id].sess.resume.Token = c.state.identify.Token
	}

	// start listeners
	cc := params.SessionStartLimit.MaxConcurrency

	var wg sync.WaitGroup
	for i := range c.shards {
		c.log.Debugw("launch shard", "shard", i)

		if cc == 0 {
			c.log.Debugw("limited concurrency", "shard", i, "limit", params.SessionStartLimit.MaxConcurrency, "wait", concurrencyWindow)

			time.Sleep(concurrencyWindow)
			cc = params.SessionStartLimit.MaxConcurrency
		}

		cc -= 1
		wg.Add(1)

		go func(s *shard) {
			s.listen()
			wg.Done()
		}(&c.shards[i])
	}

	wg.Wait()
	return nil
}
