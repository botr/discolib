// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dlclient

import (
	"encoding/json"
	"strconv"
)

// Error is an error returned by rest API
type Error struct {
	Code    int    `json:"code"`
	Message string `json:"message"`

	ErrorString      string `json:"error"`             // oauth2 error
	ErrorDescription string `json:"error_description"` // oauth2 error

	Errors json.RawMessage `json:"errors"`
}

func (e *Error) Error() string {
	if e.Message == "" {
		return e.ErrorString + " (" + e.ErrorDescription + ")"
	}
	return e.Message + " (code: " + strconv.Itoa(e.Code) + ")"
}
