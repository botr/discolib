// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dlclient

import (
	"bytes"
	"context"
	"encoding/json"
	"io"
	"io/fs"
	"mime/multipart"
	"net/http"
	"strconv"
	"strings"

	"pkg.botr.me/discolib/api/dlrest"
	"pkg.botr.me/discolib/api/dltype"
	"pkg.botr.me/discolib/internal"
	"pkg.botr.me/discolib/internal/errstr"
	"pkg.botr.me/discolib/internal/urlencode"
)

const (
	baseURL = "https://discord.com/api"
)

const (
	ErrInvalidInput errstr.Error = "invalid input"
)

// Query can be used to construct URL and marshal body of request within REST API.
type Query interface {
	Path() string
}

// Preparer can be used to construct URL and marshal body of request within REST API.
type Preparer interface {
	Query

	Prepare() dlrest.Request
}

type Config struct {
	UA dltype.UserAgent `valid:"required"`

	RootURL        string // API root, including version part (override for testing purposes)
	SkipValidation bool   // skips request values validation
}

type Client struct {
	apiRoot   string
	userAgent string

	http  http.Client
	valid internal.Validate

	enc struct {
		url  urlencode.URLEncoder
		form urlencode.FormEncoder
	}

	dec struct {
		url urlencode.URLDecoder
	}
}

// New configures helper for making requests to Discord HTTP REST API.
// TLS min version is set to 1.2
func New(config Config) (*Client, error) {
	if err := internal.DefaultValidator.Struct(&config); err != nil {
		return nil, err
	}

	client := Client{
		apiRoot:   config.RootURL,
		userAgent: config.UA.String(),
	}

	if client.apiRoot == "" {
		client.apiRoot = baseURL + "/v" + strconv.Itoa(dlrest.Version)
	}

	if !config.SkipValidation {
		client.valid = internal.DefaultValidator
	}

	client.enc.url = urlencode.NewURLEncoder()
	client.enc.form = urlencode.NewFormEncoder()
	client.dec.url = urlencode.NewURLDecoder()

	client.http.Transport = defaultTransport

	return &client, nil
}

// Query returns url prepared and checked by supplied Queries
func (c *Client) Query(prep Query) (string, error) {
	if c.valid != nil {
		if err := c.valid.Struct(prep); err != nil {
			return "", ErrInvalidInput.Wrap(err)
		}
	}

	u, err := c.enc.url.Encode(c.apiRoot+prep.Path(), prep)
	if err != nil {
		return "", errstr.Error("url encode").Wrap(err)
	}

	return u.String(), nil
}

// DecodeRequestURI parses rawurl string into receiver
func (c *Client) DecodeRequestURI(rawurl string, dst interface{}) error {
	return c.dec.url.DecodeQuery(rawurl, dst)
}

// Request takes data from dlrest.Request and sends requests to Discord API server over HTTP
//
// When Request has File multipart/form-data content type is used.
// Otherwise, if ContentType field of Request is set, it can modify output format.
//
// Server might respond with following responses:
// OK                  The request completed successfully
// Created             The entity was created successfully.
// NoContent           The request completed successfully but returned no content.
// NotModified         The entity was not modified (no action was taken).
// BadRequest          The request was improperly formatted, or the server couldn't understand it.
// Unauthorized        The `Authorization` header was missing or invalid.
// Forbidden           The `Authorization` token you passed did not have permission to the resource.
// NotFound            The resource at the location specified doesn't exist.
// MethodNotAllowed    The HTTP method used is not valid for the location specified.
// TooManyRequests     You are being rate limited, see Rate Limits.
// InternalServerError (any 500 error) The server had an error processing your request (these are rare).
// BadGateway          There was not a gateway available to process your request. Wait a bit and retry.
func (c *Client) Request(ctx context.Context, prep Preparer) error {
	return c.request(prep, makeRequest(ctx))
}

// AuthRequest is same as Request but appends Authorization header with header value
func (c *Client) AuthRequest(ctx context.Context, header string, prep Preparer) error {
	request := makeRequest(ctx)
	request.Header.Set(HeaderAuthorization, header)

	return c.request(prep, request)
}

func (c *Client) request(prep Preparer, request *http.Request) error {
	if c.valid != nil {
		if err := c.valid.Struct(prep); err != nil {
			return ErrInvalidInput.Wrap(err)
		}
	}

	source := prep.Prepare()

	// setup standard headers
	request.Method = source.Method
	request.Header.Set(HeaderUserAgent, c.userAgent)

	// construct URL string
	var err error
	request.URL, err = c.enc.url.Encode(c.apiRoot+prep.Path(), prep)
	if err != nil {
		return err
	}
	request.Host = request.URL.Host

	switch source.Method {
	// do not marshal body for GET requests
	case http.MethodPost, http.MethodPatch, http.MethodPut, http.MethodDelete:
		var marshaler func(interface{}, *http.Request) error
		if source.File == nil {
			switch source.ContentType {
			case ApplicationXWWWFormUrlencoded:
				marshaler = c.marshalForm
			case "":
				marshaler = marshalJSON
			default:
				panic("unsupported content type: " + source.ContentType)
			}
		} else {
			marshaler = marshalMultipart(source.File)
		}

		if err = marshaler(prep, request); err != nil {
			return errstr.Error("marshal request body").Wrap(err)
		}
	}

	response, err := c.http.Do(request)
	if err != nil {
		return errstr.Error("make request").Wrap(err)
	}

	if response.StatusCode == http.StatusNoContent {
		return nil
	}

	if response.StatusCode >= http.StatusBadRequest {
		var result Error
		err = json.NewDecoder(response.Body).Decode(&result)
		if err != nil {
			return errstr.Error("unmarshal error response body").Wrap(err)
		}

		return errstr.Error("unsuccessful request").Wrap(&result)
	}

	if source.Response != nil {
		err = json.NewDecoder(response.Body).Decode(source.Response)
		if err != nil {
			return errstr.Error("unmarshal response body").Wrap(err)
		}
	}

	return nil
}

// marshalForm tries to call EncodeForm method on src and falls back to generic tag-based encoder
func (c *Client) marshalForm(src interface{}, request *http.Request) error {
	body, err := c.enc.form.Encode(src)
	if err != nil {
		return err
	}

	request.Header.Set(HeaderContentType, ApplicationXWWWFormUrlencoded)
	request.Body = io.NopCloser(strings.NewReader(body))
	return nil
}

// marshalJSON writes encoded src into request body
func marshalJSON(src interface{}, request *http.Request) error {
	data, err := json.Marshal(src)
	if err != nil {
		return err
	}

	request.Header.Set(HeaderContentType, ApplicationJSON)
	request.Body = io.NopCloser(bytes.NewReader(data))
	return nil
}

// marshalMultipart only supports sending payload using payload_json field so everything that isn't file will be there
func marshalMultipart(f fs.File) func(interface{}, *http.Request) error {
	return func(src interface{}, request *http.Request) error {
		fstat, err := f.Stat()
		if err != nil {
			return errstr.Error("getting file stat").Wrap(err)
		}

		// buffer for multipart form output
		buf := bytes.NewBuffer(nil)
		mpart := multipart.NewWriter(buf)

		// write payload first
		w, err := mpart.CreateFormField("payload_json")
		if err == nil {
			err = json.NewEncoder(w).Encode(src)
		}

		if err != nil {
			return errstr.Error("create payload").Wrap(err)
		}

		// copy file contents
		w, err = mpart.CreateFormFile("file", fstat.Name())
		if err == nil {
			_, err = io.Copy(w, f)
		}

		if err != nil {
			return errstr.Error("create file part").Wrap(err)
		}

		if err = mpart.Close(); err != nil {
			return errstr.Error("close multi-part form").Wrap(err)
		}

		// append data to request
		request.Header.Set(HeaderContentType, mpart.FormDataContentType())
		request.Body = io.NopCloser(buf)
		return nil
	}
}

// makeRequest creates minimal http.Request
func makeRequest(ctx context.Context) *http.Request {
	request := &http.Request{
		Header: make(http.Header),
	}

	return request.WithContext(ctx)
}
