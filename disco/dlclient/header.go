package dlclient

const (
	HeaderContentType   = "Content-Type"
	HeaderUserAgent     = "User-Agent"
	HeaderAuthorization = "Authorization"
)
