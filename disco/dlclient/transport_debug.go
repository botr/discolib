//go:build http_debug

package dlclient

import (
	"fmt"
	"net/http"
	"net/http/httputil"
)

var defaultTransport http.RoundTripper = new(debugHTTPTransport)

type debugHTTPTransport struct{}

func (d *debugHTTPTransport) RoundTrip(req *http.Request) (*http.Response, error) {
	dump, _ := httputil.DumpRequestOut(req, true)
	fmt.Printf("--- BEGIN REQUEST ---\n%s\n---- END REQUEST ----\n", dump)

	resp, err := http.DefaultTransport.RoundTrip(req)
	if err == nil {
		dump, _ = httputil.DumpResponse(resp, true)
		fmt.Printf("--- BEGIN RESPONSE ---\n%s\n---- END RESPONSE ----\n", dump)
	}

	return resp, err
}
