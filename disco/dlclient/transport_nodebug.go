//go:build !http_debug

package dlclient

import "net/http"

var defaultTransport http.RoundTripper = http.DefaultTransport
