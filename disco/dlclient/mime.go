package dlclient

const (
	ApplicationXWWWFormUrlencoded = "application/x-www-form-urlencoded"
	ApplicationJSON               = "application/json"
)
