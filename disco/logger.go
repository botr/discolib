// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package disco

// StubLog implementes StructuredLogger and Logger interfaces used across discolib but doesn't log anything
var StubLog stubLogger

type stubLogger struct{}

func (stubLogger) Debug(...interface{})          {}
func (stubLogger) Debugf(string, ...interface{}) {}
func (stubLogger) Debugw(string, ...interface{}) {}
func (stubLogger) Error(...interface{})          {}
func (stubLogger) Errorf(string, ...interface{}) {}
func (stubLogger) Errorw(string, ...interface{}) {}
func (stubLogger) Info(...interface{})           {}
func (stubLogger) Infof(string, ...interface{})  {}
func (stubLogger) Infow(string, ...interface{})  {}

func (stubLogger) With(...interface{}) stubLogger { return stubLogger{} }
