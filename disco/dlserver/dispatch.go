// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dlserver

import (
	"context"

	"pkg.botr.me/discolib/api/dlinteraction"
	"pkg.botr.me/discolib/api/dlresource"
)

// InteractionDispatcher implements simple Ping method and should be embedded by dispatcher but it is safe to override
type InteractionDispatcher struct {
	dlinteraction.UnimplementedDispatcher
}

// Ping just returns no errors which is sufficient enough
func (*InteractionDispatcher) Ping(context.Context, *dlresource.Interaction) error { return nil }

func (*InteractionDispatcher) mustEmbed() {}
