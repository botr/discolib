// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dlserver

import (
	"crypto/ed25519"
	"encoding/hex"
	"encoding/json"
	"io"
	"net"
	"net/http"
	"strconv"
	"strings"
	"time"

	"pkg.botr.me/discolib/api/dlinteraction"
	"pkg.botr.me/discolib/api/dlresource"
	"pkg.botr.me/discolib/api/dltype"
	"pkg.botr.me/discolib/internal"
	"pkg.botr.me/discolib/internal/errstr"
	"pkg.botr.me/discolib/internal/log"
)

// TODO: optional TLS support
// FIXME: response must have deadline since documentation says so

const (
	hdrSignatureEd25519   = "X-Signature-Ed25519"
	hdrSignatureTimestamp = "X-Signature-Timestamp"
)

const (
	responseDeadline = 3 * time.Second // If the 3 second deadline is exceeded, the token will be invalidated.
	// followupDeadline = 15 * time.Minute // Interaction tokens are valid for 15 minutes and can be used to send followup messages.
)

// DispatchHandler ensures forward compatibility for implementations
type DispatchHandler interface {
	dlinteraction.Dispatcher
	mustEmbed()
}

type Config struct {
	UA dltype.UserAgent `valid:"required"`

	Path      string // prefix for server query path (default: "/")
	PublicKey string `valid:"len=64"` // 32-bit hex-encoded string of ed25519 public key
}

type Server struct {
	name string

	http    http.Server
	handler dlinteraction.Handler
	log     log.StructuredLogger

	pubKey ed25519.PublicKey
}

// New creates Server for outgoing webhook dlinteraction.
func New(log log.StructuredLogger, config Config, dispatcher DispatchHandler) (*Server, error) {
	if err := internal.DefaultValidator.Struct(&config); err != nil {
		return nil, err
	}

	server := Server{
		handler: dlinteraction.NewHandler(dispatcher),
		log:     log,
	}

	var err error
	if server.pubKey, err = hex.DecodeString(config.PublicKey); err != nil {
		return nil, errstr.Error("public key decode").Wrap(err)
	}

	server.name = config.UA.String()

	mux := http.NewServeMux()
	mux.HandleFunc(
		"/"+strings.Trim(config.Path, "/"),
		server.postHandler,
	)

	server.http.Handler = mux

	return &server, nil
}

// ListenAndServe starts http.Serve using protocol and address from addr in form of [tcp://][1.2.3.4]:port|unix:///path/to/socket.sock
//
// TCP used by default if protocol is omitted.
func (s *Server) ListenAndServe(addr string) error {
	if addr == "" {
		return errstr.Error("empty listen address")
	}

	l, err := net.Listen(addr2pair(addr))
	if err != nil {
		return errstr.Error("listen").Wrap(err)
	}

	return s.http.Serve(l)
}

func (s *Server) postHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Server", s.name)

	if status, err := s.validateRequest(r); err != nil {
		w.WriteHeader(status)
		s.log.Errorf("invalid request: %+w", err)
		return
	}

	var it dlresource.Interaction
	err := json.NewDecoder(r.Body).Decode(&it)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		s.log.Errorf("malformed json: %+w", err)
		return
	}

	var ir *dlresource.InteractionResponse
	if ir, err = s.handler.Handle(r.Context(), &it); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		s.log.Errorf("event cannot be handled: %+w", err)
		return
	}

	err = json.NewEncoder(w).Encode(ir)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		s.log.Errorf("event cannot be handled: %+w", err)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
}

func (s *Server) validateRequest(request *http.Request) (int, error) {
	// check timestamp header
	ts := request.Header.Get(hdrSignatureTimestamp)
	sec, err := strconv.ParseInt(ts, 10, 0)
	if err != nil {
		return http.StatusBadRequest, err
	}

	// check signature header
	sig, err := hex.DecodeString(request.Header.Get(hdrSignatureEd25519))
	if len(sig) != ed25519.SignatureSize || err != nil {
		return http.StatusBadRequest, internal.Describe("wrong signature", len(sig))
	}

	// check if timestamp too old
	if d := time.Since(time.Unix(sec, 0)); d > responseDeadline {
		return http.StatusUnauthorized, internal.Describe("deadline exceed", d)
	}

	// verify signature
	buf := make([]byte, len(ts)+int(request.ContentLength))
	copy(buf, []byte(ts))

	_, err = io.ReadFull(request.Body, buf[len(ts):])
	if err != nil {
		return http.StatusInternalServerError, errstr.Error("read request body").Wrap(err)
	}

	if ok := ed25519.Verify(s.pubKey, buf, sig); !ok {
		return http.StatusUnauthorized, errstr.Error("verify signature")
	}

	return http.StatusOK, nil
}

// address2pair parses URI-like schema:
// [tcp://][1.1.1.1]:port|unix:///path/to/socket.sock
func addr2pair(addr string) (scheme, path string) {
	ss := strings.SplitN(addr, "://", 2)

	if len(ss) > 1 {
		return ss[0], ss[1]
	}
	return "tcp", ss[0]
}
