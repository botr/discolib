// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package disco

import (
	"sync"
	"sync/atomic"

	"pkg.botr.me/discolib/api/dlgateway"
)

// session contains client information for single gateway connection
type shardSession struct {
	mu     sync.Mutex
	resume dlgateway.ResumePayload
}

// Resume returns resume payload and second parameter indicates if session id was set
func (s *shardSession) Resume() (dlgateway.ResumePayload, bool) {
	s.mu.Lock()
	defer s.mu.Unlock()

	return s.resume, s.resume.SessionID != ""
}

func (s *shardSession) Seq() uint64 {
	s.mu.Lock()
	defer s.mu.Unlock()

	return s.resume.Seq
}

func (s *shardSession) StoreID(id string) {
	s.mu.Lock()
	defer s.mu.Unlock()

	s.resume.SessionID = id
}

func (s *shardSession) StoreSeq(seq uint64) {
	s.mu.Lock()
	defer s.mu.Unlock()

	s.resume.Seq = seq
}

// gatewayState contains global client state
type gatewayState struct {
	url atomic.Value

	mu       sync.Mutex
	identify dlgateway.IdentifyPayload // information used to connect
	status   dlgateway.StatusUpdate    // last user presence
}

func (s *gatewayState) URL() string       { return s.url.Load().(string) }
func (s *gatewayState) SetURL(url string) { s.url.Store(url) }

func (s *gatewayState) Identify(shard int) dlgateway.IdentifyPayload {
	s.mu.Lock()
	defer s.mu.Unlock()

	payload, status := s.identify, s.status

	// we copy presence and shards slice so we can be sure that status wasn't modified after we copied identify payload
	if s.identify.Shard[1] > 0 {
		payload.Shard = []int{shard, s.identify.Shard[1]}
	}

	payload.Presence = &status
	return payload
}

func (s *gatewayState) StorePresence(status dlgateway.StatusUpdate) {
	s.mu.Lock()
	defer s.mu.Unlock()

	s.status = status
}
