// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package dltransport

import (
	"context"
	"crypto/tls"
	"errors"
	"io"
	"net"
	"sync"

	"github.com/gobwas/ws"

	"pkg.botr.me/discolib/api/dlgateway"
	"pkg.botr.me/discolib/internal"
	"pkg.botr.me/discolib/internal/errstr"
	"pkg.botr.me/discolib/internal/log"
	"pkg.botr.me/discolib/internal/urlencode"
)

// TODO: support payload compression

// Command defines interface for comands that may be sent over gateway
type Command interface {
	OpCode() dlgateway.OpCode
}

type Config struct {
	Version  int                   `url:"v,required" valid:"required"`
	Encoding dlgateway.Encoding    `url:"encoding"   valid:"required"`
	Compress dlgateway.Compression `url:"compress,omitempty"`
}

// Transport handles connection to websocket gateway and all of the encoding/decoding.
// It is stateless and does not store any information about client except open connection.
type Transport struct {
	config Config

	dialer ws.Dialer
	log    log.StructuredLogger

	encoder wsEncoder

	mu     sync.Mutex
	writer io.Writer
	quit   chan<- error
}

func New(log log.StructuredLogger, config Config) (*Transport, error) {
	if err := internal.DefaultValidator.Struct(&config); err != nil {
		return nil, err
	}

	t := Transport{
		log:    log,
		config: config,
		dialer: ws.Dialer{TLSConfig: &tls.Config{MinVersion: tls.VersionTLS12}},
	}

	switch config.Encoding {
	case dlgateway.EncodingJSON:
		t.encoder = textEncoder{jsonMarshaler{}}
	default:
		return nil, errstr.Error("unknown encoder").Describe(config.Encoding)
	}

	switch config.Compress {
	case dlgateway.CompressNone:
	case dlgateway.CompressZlibStream:
		t.encoder = &zlibEncoder{wsEncoder: t.encoder}
	default:
		return nil, errstr.Error("unknown transport compression method").Describe(config.Compress)
	}

	return &t, nil
}

func (t *Transport) Unmarshaler() dlgateway.Unmarshaler {
	return t.encoder
}

// Send marshals cmd body and writes to dlgateway. This method should only be called after DialAndHandle successfully established connection
func (t *Transport) Send(cmd Command) error {
	var err error
	defer func() {
		t.log.Debugw("send gateway payload", "op", cmd.OpCode(), "data", cmd, "err", err)
	}()

	t.mu.Lock()
	w := t.writer
	t.mu.Unlock()

	if w == nil {
		err = errstr.Error("writer not initialized")
		return err
	}

	payload := dlgateway.Payload{OpCode: cmd.OpCode()}
	if payload.Data, err = t.encoder.Marshal(cmd); err != nil {
		err = errstr.Error("payload data marshal").Wrap(err)
		return err
	}

	err = t.encoder.Encode(w, &payload)
	if err != nil {
		err = errstr.Error("websocket encode").Wrap(err)
		return err
	}

	return nil
}

// Close drops current Listen and closes connection with given code. Blocks until connection is closed.
func (t *Transport) Close(code dlgateway.CloseEventCode) {
	t.mu.Lock()
	t.quit <- code
	t.writer, t.quit = nil, nil
	t.mu.Unlock()
}

// Listen connects to websocket gateway and handles incoming events.
// This method blocks until either connection or handler returns an error.
// When connection successfuly establisher, ready channel will be closed.
func (t *Transport) Listen(endpoint string, dispatcher dlgateway.PayloadDispatcher) error {
	var err error
	defer func() {
		t.log.Debugw("exit listener", "err", err)
	}()

	t.mu.Lock()
	defer t.mu.Unlock()

	if t.writer != nil {
		err = errstr.Error("transport already listening")
		return err
	}

	// construct connection URL using address and client options
	u, err := urlencode.NewURLEncoder().Encode(endpoint, &t.config)
	if err != nil {
		return errstr.Error("encode endpoint url").Wrap(err)
	}

	endpoint = u.String()
	t.log.Debugw("dial gateway", "endpoint", endpoint)

	// create WebSocket connection
	// TODO: check br and read from it if not nil
	var conn net.Conn
	if conn, _, _, err = t.dialer.Dial(context.Background(), endpoint); err != nil {
		err = errstr.Error("dial failed").Wrap(err)
		return err
	}
	defer conn.Close()

	// store writer to send messages
	quit := make(chan error)
	t.writer, t.quit = conn, quit
	t.mu.Unlock()

	go func() {
		handler := dlgateway.NewPayloadHandler(dispatcher, t.encoder.(gatewayMarshaler))
		if he := t.handle(conn, handler); he != nil {
			quit <- he
		}
		close(quit) // closeWriter will make next read fail and close will be called only once
	}()

	// loop allowing to catch multiple events before finish
	for err = range quit {
		if q := closeWriter(conn, err); q != nil && !isEOF(q) {
			t.log.Errorw("close writer", "err", q)
		}
	}

	t.mu.Lock() // unlock deferred at the top
	return err
}

func (t *Transport) handle(rw io.ReadWriter, handler dlgateway.PayloadHandler) error {
	var err error

	for {
		var payload dlgateway.Payload
		if err = t.encoder.Decode(rw, &payload); err != nil {
			err = errstr.Error("decode websocket message").Wrap(err)
			break
		}

		t.log.Debugw("recv gateway payload", "op", payload.OpCode, "type", payload.Type, "seq", payload.Seq, "data", payload.Data)

		if err = handler.Handle(context.Background(), payload); err != nil {
			err = errstr.Error("payload handler").Wrap(err)
			break
		}
	}

	if !isEOF(err) {
		return err
	}
	return nil
}

func isEOF(err error) bool {
	return errors.Is(err, io.EOF) || errors.Is(err, net.ErrClosed)
}

func closeWriter(w io.WriteCloser, err error) error {
	var code dlgateway.CloseEventCode
	if !errors.As(err, &code) {
		code = dlgateway.CloseUnknownError
	}

	status := ws.StatusCode(code)
	if !status.In(ws.StatusRangePrivate) {
		status = ws.StatusAbnormalClosure
	}

	f := ws.NewCloseFrame(ws.NewCloseFrameBody(status, ""))
	if err := ws.WriteFrame(w, f); err != nil {
		return err
	}
	return w.Close()
}
