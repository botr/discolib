package dltransport

import (
	"bytes"
	"compress/flate"
	"encoding/json"
	"errors"
	"io"

	"github.com/gobwas/ws/wsutil"

	"pkg.botr.me/discolib/api/dlgateway"
	"pkg.botr.me/discolib/internal/errstr"
)

// gatewayMarshaler describes interface for working with gateway messages
type gatewayMarshaler interface {
	dlgateway.Unmarshaler

	Marshal(interface{}) ([]byte, error)
}

type jsonMarshaler struct{}

func (jsonMarshaler) Marshal(v interface{}) ([]byte, error) { return json.Marshal(v) }

func (jsonMarshaler) Unmarshal(data []byte, v interface{}) error {
	return json.Unmarshal(stripTraceJSON(data), v)
}

// wsEncoder describes interface for interacting with websocket protocol
type wsEncoder interface {
	gatewayMarshaler

	Decode(io.ReadWriter, interface{}) error
	Encode(io.Writer, interface{}) error
}

// textEncoder writes and reads text-encoded frames
type textEncoder struct {
	gatewayMarshaler
}

func (e textEncoder) Decode(rw io.ReadWriter, v interface{}) error {
	data, err := wsutil.ReadServerText(rw)
	if err != nil {
		return errstr.Error("can't read websocket data").Wrap(err)
	}

	return e.Unmarshal(data, v)
}

func (e textEncoder) Encode(w io.Writer, v interface{}) error {
	data, err := e.Marshal(v)
	if err != nil {
		return errstr.Error("can't marshal data").Wrap(err)
	}

	return wsutil.WriteClientText(w, data)
}

// binaryEncoder writes and reads binary-encoded frames
type binaryEncoder struct {
	gatewayMarshaler
}

func (e binaryEncoder) Decode(rw io.ReadWriter, v interface{}) error {
	data, err := wsutil.ReadServerBinary(rw)
	if err != nil {
		return errstr.Error("can't read websocket data").Wrap(err)
	}

	return e.Unmarshal(data, v)
}

func (e binaryEncoder) Encode(w io.Writer, v interface{}) error {
	data, err := e.Marshal(v)
	if err != nil {
		return errstr.Error("can't marshal data").Wrap(err)
	}

	return wsutil.WriteClientBinary(w, data)
}

// zlibEncoder wraps decoder in compression context. It does not compress messages on encoding
type zlibEncoder struct {
	wsEncoder

	reader struct {
		dict []byte
		pos  int
	}
}

func (e *zlibEncoder) Decode(rw io.ReadWriter, v interface{}) error {
	data, err := wsutil.ReadServerBinary(rw)
	if err != nil {
		return errstr.Error("read websocket data").Wrap(err)
	}

	const zlibSuffix = "\x00\x00\xff\xff"
	if len(data) < len(zlibSuffix) || !bytes.Equal(data[len(data)-4:], []byte(zlibSuffix)) {
		return errstr.Error("malformed zlib suffix")
	}

	var fr io.ReadCloser

	buf := bytes.NewBuffer(data)
	if e.reader.dict == nil {
		e.reader.dict = make([]byte, 32*1024) // deflate sliding window is 32KiB
		e.reader.pos = len(e.reader.dict)

		// check zlib header
		hdr := make([]byte, 2)
		if _, err = io.ReadFull(buf, hdr); err != nil {
			return errstr.Error("read zlib header").Wrap(err)
		}

		const zlibDeflate = 8 // compress/zlib
		if hdr[0]&0x0F != zlibDeflate || (uint(hdr[0])<<8|uint(hdr[1]))%31 != 0 {
			return errstr.Error("malformed zlib header")
		}

		fr = flate.NewReader(buf)
	} else {
		fr = flate.NewReaderDict(buf, e.reader.dict[e.reader.pos:]) // makes copy of provided dict so it's safe to overwrite later
	}
	defer fr.Close()

	pkt, err := io.ReadAll(fr)
	if err != nil && !errors.Is(err, io.ErrUnexpectedEOF) {
		// FIXME: io.ErrUnexpectedEOF shouldn't be used to detect packet end
		return errstr.Error("read deflate stream").Wrap(err)
	}

	if dicln, pktln := len(e.reader.dict), len(pkt); pktln >= dicln {
		copy(e.reader.dict, pkt[pktln-dicln:])
		e.reader.pos = 0
	} else if pktln >= e.reader.pos {
		copy(e.reader.dict, e.reader.dict[pktln:])
		copy(e.reader.dict[dicln-pktln:], pkt[pktln-e.reader.pos:])
		e.reader.pos = 0
	} else /* pktln < e.reader.pos < dicln */ {
		copy(e.reader.dict[e.reader.pos-pktln:], e.reader.dict[e.reader.pos:])
		copy(e.reader.dict[dicln-pktln:], pkt)
		e.reader.pos = e.reader.pos - pktln
	}

	return e.wsEncoder.(gatewayMarshaler).Unmarshal(pkt, v)
}

// stripTraceJSON cuts off discord internal metadata to reduce noise in logs
func stripTraceJSON(data []byte) []byte {
	const hdr = `"_trace":[`
	start := bytes.Index(data, []byte(hdr))
	if start < 1 {
		return data
	}

	end := start + len(hdr)
	for c := 1; end < len(data) && c != 0; end++ {
		switch data[end] {
		case '[':
			c++
		case ']':
			c--
		}
	}

	postfix := len(data) - end
	if start > 0 && data[start-1] == ',' {
		start--
	}

	copy(data[start:], data[end:])
	return data[:start+postfix]
}
