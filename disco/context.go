// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package disco

import (
	"context"
)

type ctxKey int

const (
	_ ctxKey = iota

	ctxShard
)

func shardContext(ctx context.Context, n int) context.Context {
	return context.WithValue(ctx, ctxShard, n)
}

func contextShard(ctx context.Context) (int, bool) {
	n, ok := ctx.Value(ctxShard).(int)
	return n, ok
}
