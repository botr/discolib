// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package disco

import (
	"context"
	"errors"
	"math/rand"
	"sync/atomic"
	"time"

	"pkg.botr.me/discolib/api/dlgateway"
	"pkg.botr.me/discolib/disco/dltransport"
	"pkg.botr.me/discolib/internal/errstr"
	"pkg.botr.me/discolib/internal/log"
)

const (
	minWaitReconnect = 1000
	maxWaitReconnect = 5000
)

const (
	errInvalidSession errstr.Error = "invalid session"
)

var (
	errInvalidResume   = errInvalidSession.Wrap(dlgateway.CloseSessionTimedOut)
	errInvalidIdentify = errInvalidSession.Wrap(dlgateway.CloseGoingAway)
)

type shard struct {
	id int

	log   log.StructuredLogger
	state *gatewayState

	dispatch  dlgateway.EventHandler
	transport *dltransport.Transport

	sess      shardSession
	quit      chan<- struct{}
	reconnect chan<- error
	hello     chan<- dlgateway.HelloPayload

	pending int32

	// embedded handlers
	dlgateway.EventDispatcher
	dlgateway.UnimplementedPayloadDispatcher
}

// Ready overrides Ready event handler so when dispatched it is possible to get session id from there
func (s *shard) Ready(ctx context.Context, payload dlgateway.ReadyEvent) {
	s.sess.StoreID(payload.SessionID)
	s.EventDispatcher.Ready(ctx, payload)

	// TODO: sync ready status to omit sending status update manually to every shard
	// - collect disconnects/not connected shards
	// - wait for every shard to be ready
	// - issue ready event
}

func (s *shard) Resumed(ctx context.Context, payload dlgateway.ResumedEvent) {}

// Dispatch passes gateway payload to event dispatcher and stores current event sequence number
func (s *shard) Dispatch(ctx context.Context, payload dlgateway.DispatchPayload) {
	s.sess.StoreSeq(payload.Seq)

	err := s.dispatch.Handle(shardContext(ctx, s.id), payload.Type, payload.Data)
	if err != nil {
		s.log.Error(err)
	}
}

// Heartbeat indicates that client must issue heartbeat to server as it normally would
func (s *shard) Heartbeat(ctx context.Context, payload dlgateway.HeartbeatPayload) {
	if err := s.sendHeartbeat(); err != nil {
		s.reconnect <- err
		return
	}
}

func (s *shard) InvalidSession(ctx context.Context, payload dlgateway.InvalidSessionPayload) {
	if payload.Resumable {
		s.reconnect <- errInvalidResume
		return
	}

	// if client cannot reconnect in time to resume, it is expected to wait and resend identify
	ms := minWaitReconnect + rand.Int63n(maxWaitReconnect)
	s.log.Debugw("invalid session, waiting for restart", "delay", ms)

	time.Sleep(time.Duration(ms) * time.Millisecond)
	s.reconnect <- errInvalidIdentify
}

func (s *shard) Hello(ctx context.Context, payload dlgateway.HelloPayload) {
	s.hello <- payload
}

func (s *shard) Reconnect(ctx context.Context, payload dlgateway.ReconnectPayload) {
	s.reconnect <- nil
}

func (s *shard) HeartbeatACK(ctx context.Context, payload dlgateway.HeartbeatACKPayload) {
	atomic.AddInt32(&s.pending, -1)
}

func (s *shard) listen() {
	// channels to control execution flow
	hello := make(chan dlgateway.HelloPayload, 1)
	quit := make(chan struct{}, 1)
	reconnect := make(chan error, 1)

	s.hello, s.quit, s.reconnect = hello, quit, reconnect

	// transport errors must be sent to this channel
	go func() { s.reconnect <- s.transport.Listen(s.state.URL(), s) }()

	var heartbeat chan struct{} // hearbeat control channel
	var resume bool             // whether to send resume on Hello or Identify
	var connecting bool         // reconnect was issued and pending

	shutdown := func() {
		if heartbeat != nil {
			close(heartbeat)
			heartbeat = nil
		}
		s.transport.Close(dlgateway.CloseNormalClosure)
	}

	for {
		select {
		case <-quit:
			shutdown()
			return

		case err := <-reconnect:
			s.log.Debugw("reconnect", "connecting", connecting, "reason", err)
			if connecting {
				continue // avoid reconnection while reconnecting
			}
			connecting = true

			// TODO: reconnect with exponentially increasing timeout

			var code dlgateway.CloseEventCode
			if !errors.As(err, &code) {
				code = dlgateway.CloseUnknownError
			}

			resume = code.Resumable()

			shutdown()
			go func() { s.reconnect <- s.transport.Listen(s.state.URL(), s) }()

		case payload := <-hello:
			var cmd dltransport.Command
			cmd, ok := s.sess.Resume()
			if !resume || !ok {
				cmd = s.state.Identify(s.id) // TODO: there is a limit on identify calls per 24h
			}

			resume, connecting = false, false
			atomic.StoreInt32(&s.pending, 0)

			heartbeat = make(chan struct{}, 1)
			go s.heartbeat(payload.HeartbeatInterval, heartbeat)

			if err := s.transport.Send(cmd); err != nil {
				s.reconnect <- err
			}
		}
	}
}

func (s *shard) sendHeartbeat() error {
	pending := atomic.AddInt32(&s.pending, 1)
	if pending > 1 {
		return errstr.Error("stale connection").Wrap(dlgateway.CloseSessionTimedOut)
	}

	err := s.transport.Send(dlgateway.HeartbeatPayload{Seq: s.sess.Seq()})
	if err != nil {
		return errstr.Error("heartbeat send").Wrap(err)
	}

	return nil
}

func (s *shard) heartbeat(interval time.Duration, quit <-chan struct{}) {
	s.log.Debugw("start heatbeat", "interval", interval)
	defer s.log.Debugw("stop heartbeat")

	t := time.NewTimer(interval)
	for {
		select {
		case <-quit:
			if !t.Stop() {
				<-t.C // drain channel if stop wasn't called from this goroutine
			}
			return
		case <-t.C:
			if err := s.sendHeartbeat(); err != nil {
				s.reconnect <- err
				return
			}
			t.Reset(interval)
		}
	}
}
