// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package urlencode

import (
	"net/url"

	"github.com/gorilla/schema"
)

type formEncoder interface {
	EncodeForm(SchemaEncoder, url.Values) error
}

// FormEncoder extends schema.Encoder to conveniently encode x-www-form-urlencoded forms from structs, maps, etc
type FormEncoder struct{ schema schema.Encoder }

func NewFormEncoder() FormEncoder {
	enc := FormEncoder{*schema.NewEncoder()} // internal fields are pointers so struct copy is fine
	enc.schema.SetAliasTag("form")
	return enc
}

// Encode encodes src into url.Values and returns query string.
// If src contains `EncodeForm(SchemaEncoder, url.Values) error` method it will be used instead of just tag-based encoding.
func (e FormEncoder) Encode(src interface{}) (string, error) {
	q := make(url.Values)

	// check if src implements custom encoder for URL queries
	if v, ok := src.(formEncoder); ok {
		if err := v.EncodeForm(&e.schema, q); err != nil {
			return "", err
		}

		return q.Encode(), nil
	}

	if err := e.schema.Encode(src, q); err != nil {
		return "", err
	}

	return q.Encode(), nil
}
