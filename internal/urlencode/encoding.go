// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package urlencode

type SchemaEncoder interface {
	Encode(interface{}, map[string][]string) error
}

type SchemaDecoder interface {
	Decode(interface{}, map[string][]string) error
}
