// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package urlencode

import (
	"net/url"

	"github.com/gorilla/schema"
)

type urlEncoder interface {
	EncodeURL(SchemaEncoder, url.Values) error
}

type urlDecoder interface {
	DecodeURL(SchemaDecoder, url.Values) error
}

// URLEncoder extends schema.Encoder to conveniently encode url query fields from structs, maps, etc
type URLEncoder struct{ schema schema.Encoder }

func NewURLEncoder() URLEncoder {
	enc := URLEncoder{*schema.NewEncoder()} // internal fields are pointers so struct copy is fine
	enc.schema.SetAliasTag("url")
	return enc
}

// Encode encodes src into url.Values and appends to rawurl string returning full URL.
// If src contains `EncodeURL(SchemaEncoder, url.Values) error` method it will be used instead of just tag-based encoding.
func (e URLEncoder) Encode(rawurl string, src interface{}) (*url.URL, error) {
	u, err := url.ParseRequestURI(rawurl)
	if err != nil {
		return nil, err
	}

	q := make(url.Values)

	// check if src implements custom encoder for URL queries
	if v, ok := src.(urlEncoder); ok {
		err = v.EncodeURL(&e.schema, q)
		if err != nil {
			return nil, err
		}

		if len(q) > 0 {
			u.RawQuery = q.Encode()
		}
		return u, nil
	}

	if err = e.schema.Encode(src, q); err != nil {
		return nil, err
	}

	if len(q) > 0 {
		u.RawQuery = q.Encode()
	}

	return u, nil
}

// URLDecoder extends schema.Decoder to conveniently decode url query fields to structs
type URLDecoder struct{ schema schema.Decoder }

func NewURLDecoder() URLDecoder {
	dec := URLDecoder{*schema.NewDecoder()} // internal fields are pointers so struct copy is fine
	dec.schema.SetAliasTag("url")
	return dec
}

// DecodeQuery parses rawurl and tries to decode ?query string into dst
func (d URLDecoder) DecodeQuery(rawurl string, dst interface{}) error {
	u, err := url.ParseRequestURI(rawurl)
	if err != nil {
		return err
	}

	return d.decode(dst, u.Query())
}

// DecodeFragment parses rawurl and tries to decode #fragment string into dst
func (d URLDecoder) DecodeFragment(rawurl string, dst interface{}) error {
	u, err := url.ParseRequestURI(rawurl)
	if err != nil {
		return err
	}

	f, err := url.ParseQuery(u.Fragment)
	if err != nil {
		return err
	}

	return d.decode(dst, f)
}

func (d URLDecoder) decode(dst interface{}, src url.Values) error {
	// check if dst implements custom decoder for URL queries
	if v, ok := dst.(urlDecoder); ok {
		return v.DecodeURL(&d.schema, src)
	}

	return d.schema.Decode(dst, src)
}
