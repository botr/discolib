// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package internal

import "github.com/go-playground/validator/v10"

const ValidatorTag = "valid"

// DefaultValidator may be used to check configuration structs
var DefaultValidator Validate = validator.New()

type Validate interface {
	Struct(interface{}) error
}

func init() {
	DefaultValidator.(*validator.Validate).SetTagName(ValidatorTag)
}
