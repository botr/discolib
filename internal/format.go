// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package internal

import (
	"fmt"

	"pkg.botr.me/discolib/internal/errstr"
)

// Describe returns new error with additional info attached.
// Old Error information is still accessible using errors.Is/errors.As
func Describe(e errstr.Error, args ...interface{}) error {
	return e.Describe(fmt.Sprint(args...))
}
