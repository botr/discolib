// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package log

import "reflect"

// Logger interface used across discolib and implemented by zap.SugaredLogger
type Logger interface {
	Error(args ...interface{})
	Errorf(fmt string, args ...interface{})

	Debug(args ...interface{})
	Debugf(fmt string, args ...interface{})

	Info(args ...interface{})
	Infof(fmt string, args ...interface{})
}

// StructuredLogger interface implemented by zap.SugaredLogger
type StructuredLogger interface {
	Logger

	Errorw(msg string, pairs ...interface{})
	Debugw(msg string, pairs ...interface{})
	Infow(msg string, pairs ...interface{})
}

// WithLogger can add some context to parent logger.
// Custom logger must have method With(...interface) which returns type that implements Logger.
func WithLogger(parent Logger, args ...interface{}) Logger {
	rv := reflect.ValueOf(parent)

	with := rv.MethodByName("With")
	if with.IsZero() {
		panic("logger has no method With")
	}

	rt := with.Type()
	switch {
	case rt.NumIn() != 1:
		panic("wrong number of input parameters")
	case rt.NumOut() != 1:
		panic("wrong number of out parameters")
	case !rt.IsVariadic():
		panic("function has no variadic parameters")
	case rt.In(0).Kind() != reflect.Slice || rt.In(0).Elem().Kind() != reflect.Interface:
		panic("function parameters aren't interface{}")
	case !rt.Out(0).Implements(loggerType):
		panic("return value doesn't implement Logger")
	}

	argv := make([]reflect.Value, 0, len(args))
	for _, arg := range args {
		argv = append(argv, reflect.ValueOf(arg))
	}

	rval := with.Call(argv)
	return rval[0].Interface().(Logger)
}

// WithStructuredLogger returns StructuredLogger with some additional context
func WithStructuredLogger(parent StructuredLogger, args ...interface{}) StructuredLogger {
	return WithLogger(parent, args...).(StructuredLogger)
}

var loggerType = reflect.TypeOf((*Logger)(nil)).Elem()
