// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package internal

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"pkg.botr.me/discolib/internal/errstr"
)

func TestDescriber(t *testing.T) {
	assert := assert.New(t)

	const err errstr.Error = "unwrapped error string"
	const descriptor = "error description string"
	errd := Describe(err, descriptor)

	assert.Equal(errd.Error(), err.Error()+": "+descriptor)

	assert.ErrorIs(errd, err)

	var v errstr.Error
	assert.ErrorAs(errd, &v)
}
