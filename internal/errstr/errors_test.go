// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package errstr

import (
	"errors"
	"io"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestError(t *testing.T) {
	assert := assert.New(t)

	const str = "test error string"
	const err Error = str

	assert.Equal(err.Error(), str)

	assert.False(err.Is(errors.New(str)))

	var err2 Error = str
	assert.True(err.Is(err2))

	var err3 = io.EOF
	assert.False(err.As(&err3))
	var err4 Error
	assert.True(err.As(&err4))
	assert.Equal(err4.Error(), str)
}

func TestWrapper(t *testing.T) {
	assert := assert.New(t)

	const err Error = "unwrapped error string"
	errw := err.Wrap(io.EOF)

	assert.Equal(errors.Unwrap(errw), io.EOF)

	assert.Equal(errw.Error(), err.Error()+": "+io.EOF.Error())

	assert.ErrorIs(errw, io.EOF)
	assert.ErrorIs(errw, err)

	var v Error
	assert.ErrorAs(errw, &v)
}

func TestDescriber(t *testing.T) {
	assert := assert.New(t)

	const err Error = "unwrapped error string"
	const descriptor = "error description string"
	errd := err.Describe(descriptor)

	assert.Equal(errd.Error(), err.Error()+": "+descriptor)

	assert.ErrorIs(errd, err)

	var v Error
	assert.ErrorAs(errd, &v)
}
