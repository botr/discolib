// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

// Package errstr contains wrapper for simple const string errors
package errstr

// Error implements errors.Error interface while being immutable (and may be const)
type Error string

func (e Error) Error() string { return string(e) }

// Wrap returns new error with chained error.
// Nested error still accessible from errors.Is and errors.As.
func (e Error) Wrap(err error) error {
	return &wrapper{e, err}
}

// Describe returns new error with added description.
// Original error still accessible from errors.Is and errors.As.
// This method panic if v is not string or implements fmt.Stringer
func (e Error) Describe(v interface{}) error {
	switch s := v.(type) {
	case string:
		return &Describer{e, s}
	case interface{ String() string }:
		return &Describer{e, s.String()}
	}
	panic("unstringable type")
}

func (e Error) Is(target error) bool {
	t, ok := target.(Error)
	return ok && e == t
}

func (e Error) As(target interface{}) (ok bool) {
	t, ok := target.(*Error)
	if ok {
		*t = e
	}
	return
}

// wrapper contains arbitrary error with attached Error
type wrapper struct {
	e Error
	w error
}

func (w *wrapper) Unwrap() error { return w.w }

func (w *wrapper) Error() string              { return w.e.Error() + ": " + w.w.Error() }
func (w *wrapper) Is(target error) bool       { return w.e.Is(target) }
func (w *wrapper) As(target interface{}) bool { return w.e.As(target) }

// Describer contains error and its description
type Describer struct {
	e Error
	d string
}

func (w *Describer) Error() string              { return w.e.Error() + ": " + w.d }
func (w *Describer) Is(target error) bool       { return w.e.Is(target) }
func (w *Describer) As(target interface{}) bool { return w.e.As(target) }
